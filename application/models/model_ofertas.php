<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class model_ofertas extends CI_Model 
{
  /*-------------------------------------
  FUNCION PARA MOSTRAR TODAS LAS OFERTAS*/  
  public function mostrarOferta($id){
  	$this->db->where('id',$id);
  	$query = $this->db->get('ofertas');
  	if ($query->num_rows>0) {
  		return $query->result();
  	}else{
  		return false;
  	}
  }
  public function Datos_oferta($id){
        $query= $this->db->query("SELECT usuario.email,ofertas.titulo FROM ofertas,usuario  WHERE ofertas.id=".$id." AND ofertas.idUsuario=usuario.id");
        return $query->result();
  }
  /*---------------------------------------
  FUNCION PARA TRAER LAS OFERTAS LIMITE 10*/  
  public function ofertas(){
    $this->db->where('estado',1);
    $this->db->order_by("id", "desc");
    $query=$this->db->get('ofertas'/*,10,0*/);
      if ($query->num_rows>0) {
        return $query->result();
      }else
      {
        return false;
      }
  }

  
  /*-------------------------------------
  FUNCION PARA MOSTRAR TODAS LAS OFERTAS*/
  public function insertarOferta($datos){
    $data=array(
      'idUsuario'     => $datos['id'],
      'titulo'        => $datos['titulo'],
      'logo'          => $datos['logo'],
      'perfil'        => $datos['perfil'],
      'salario'       => $datos['salario'],
      'tipo_contrato' => $datos['contrato'],
      'pais'          => $datos['pais'],
      'ciudad'        => $datos['ciudad'],
      'fecha'         => $datos['fecha'],
    
    );
    return $this->db->insert('ofertas',$data);
  }
  /*-------------------------------------------
  FUNCION PARA APLICAR EL USUARIO A UNA OFERTA*/  
  public function aplicarOferta($datos){
    $data=array(
      'idUsuario'     => $datos['idUsuario'],
      'idOfertas'        => $datos['idOferta'],
      'Razones'          => $datos['razones'],
      'ruta_hoja_vida'   => $datos['hoja_vida'],
    );
    return $this->db->insert('Aplicar_Ofertas',$data);
  }

  /*FUNCION PARA CONTAR CUANTAS PERRSONAS APLICARON A UNA OFERTA*/
  public function contarAplicadas($id){
    $this->db->where('idOfertas', $id);
    $this->db->from('Aplicar_Ofertas');
    $cnt = $this->db->count_all_results();
    return $cnt;
  }
  public function Datos_oferta2($id){
        $this->db->where('id',$id);
        $query=$this->db->get('usuario'/*,10,0*/);
        return $query->result();
  }  
  /*FUNCIONES QUE ENVIAN LA RUTA DE LA HOJA DE VIDA AL CORREO
  public function listarAplicadas($id){
    $query=$this->db->query("SELECT * FROM Aplicar_Ofertas WHERE idOfertas=".$id);
    if ($query->num_rows>0) {
        return $query->result();
      }else
      {
        return false;
      }
  }

  public function listarOfertas(){
    $query=$this->db->query("SELECT * FROM ofertas WHERE estado=1;");
    return $query->result();
  }*/

  public function buscar($palabra){
      $query=$this->db->query("SELECT * FROM ofertas where titulo LIKE '%".$palabra."%' OR perfil LIKE '%".$palabra."%';");
      if ($query->num_rows>0) {
        return $query->result();
      }else
      {
        return false;
      }

  }
  /*--------------------------------------------------
  FUNCION PARA REFERIR UNA HOJA DE VIDA DE UN USUARIO*/  
  public function referirOferta($datos){
    $data=array(
      'idUsuario'     => $datos['idUsuario'],
      'Razones'          => $datos['razones'],
      'hv_url'   => $datos['hoja_vida'],
      'fecha'   => Date("Y-m-d"),
    );
    return $this->db->insert('referir_hv',$data);
  }  
  /*---------------------------------------------------------------
  FUNCION PARA TRAER TODAS LAS HOJA DE VIDA REFERIDA CON ESTADO 0*/  
  public function hoja_vida_referida(){
    $query=$this->db->query("SELECT * FROM usuario,referir_hv WHERE usuario.id=referir_hv.idUsuario AND referir_hv.estado=0 ORDER BY referir_hv.fecha DESC");
      if ($query->num_rows>0) {
        return $query->result();
      }else{return false;}
  } 
  /*--------------------------------------
  FUNCION PARA MOSTAR LAS HOJAS REFERIDAS*/
  public function hojas_referidas(){
    $query=$this->db->query("SELECT * FROM referir_hv");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false; }
  }
  /*---------------------------------------------
  FUNCION PARA MOSTAR LAS HOJAS REFERIDAS POR ID*/
  public function hojas_referidas_id($id){
    $query=$this->db->query("SELECT * FROM referir_hv WHERE id=".$id);
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false; }
  }  
  /*-------------------------------------
  FUNCION PARA ELIMINAR UNA HV REFERIDA*/          
  public function delete_hv_referida($id){
    $this->db->where('id',$id);
    return $this->db->delete('referir_hv');
  }

  /*-------------------------------------
  FUNCION PARA cambiar el estado de una oferta*/          
  public function change_estado_oferta($id){
    $query=$this->db->query("UPDATE ofertas SET estado= 0 WHERE id =".$id.";");
    //$this->db->where('id',$id);
    //return $this->db->delete('referir_hv');
  }



}	