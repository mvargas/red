<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class model_index extends CI_Model 
{
  /*---------------------------------------
  FUNCION PARA TRAER LAS NOTICIAS LIMITE 3*/	
  public function noticia(){
    $this->db->order_by("id", "desc");
  	$query=$this->db->get('noticia',10,0);
	  	if ($query->num_rows>0) {
	  		return $query->result();
	  	}else
	  	{
	  		return false;
	  	}
  }
  public function noticiaIndex(){
    $this->db->order_by("id", "desc");
    $query=$this->db->get('noticia',3,0);
      if ($query->num_rows>0) {
        return $query->result();
      }else
      {
        return false;
      }
  }
  /*---------------------------------------
  FUNCION PARA TRAER LAS OFERTAS LIMITE 10*/  
  public function ofertas(){
    $this->db->order_by("id", "desc");
    $this->db->where("estado",1);
    $query=$this->db->get('ofertas',4,0);
      if ($query->num_rows>0) {
        return $query->result();
      }else
      {
        return false;
      }
  }  
  /*---------------------------------------
  FUNCION PARA MOTRAR UNA NOTICIA POR MEDIO
  DE SU ID*/  
  public function mostrarNoticia($id){
    $this->db->where('id',$id);
    $query=$this->db->get('noticia');
    return $query->result();
  }
  public function mostrarpregunta(){
    $this->db->where('estado',1);
    $query=$this->db->get('preguntas');
    return $query->result();
  }
  public function mostrarpreguntatodas(){
    $query=$this->db->get('preguntas');
    return $query->result();
  }
  public function mostrarrespuestas(){
    $query=$this->db->get('respuestas');
    return $query->result();
  }
  public function mostrarrespuestasid($id){
    $this->db->where('idPreguntas',$id);
    $query=$this->db->get('respuestas');
    return $query->result();
  }
  public function encuesta($data){
      $data=array(
      'idPreguntas'    => $data['idpregunta'], 
      'idRespuesta'   => $data['idrespuesta'],
      );  
        return $this->db->insert('votos',$data); 
  }
  public function inserIp($ip){
      $data=array(
      'ip_encuesta'    => $ip);  
      return $this->db->insert('ip',$data); 
  }
  public function validaIp($ip){
   $this->db->where('ip_encuesta',$ip);
   $query=$this->db->get('ip');
     if ($query->num_rows>0) {
      return true;
     }else{
      return false;
     }
  } 
  /*------------------------------
  FUNCION MOSTRAR IMAGEN DEL HOME*/
  public function imagen_home(){
    $query=$this->db->query("SELECT * FROM imagen_web WHERE (tipo=1 OR tipo=2) AND estado=1");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  }  
  /*------------------------------------------------
  FUNCION MOSTRAR IMAGEN DE LAS PAUTAR PARA EL HOME*/
  public function imagen_pauta(){
    $query=$this->db->query("SELECT * FROM imagen_web WHERE tipo=3 OR tipo=4 order by rand()");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  } 
  /*-------------------------------------
  FUNCION MOSTRAR IMAGEN DE LAS PAGINAS*/
  public function imagen_pagina(){
    $query=$this->db->query("SELECT * FROM imagen_web WHERE tipo=5 order by rand()");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  }
 /*Funcion Extra para imagen directorio*/
  public function imagen_pagina2(){
    $query=$this->db->query("SELECT * FROM imagen_web WHERE tipo=6 order by rand()");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  }
  /*------------------------------
  FUNCION MOSTRAR LOS MINI-BANNER
  la variable $order sirve para organizar el orden de los minibanner esta variable 
  fue creada ya   que esta funcion se esta utilizando en el administrador pero en otro order*/
  public function miniBanner($orden){
    $query=$this->db->query("SELECT * FROM miniBanner ".$orden." limit 12");
    if ($query->num_rows>0) {
      return $query->result_array();
    }else{return false;}
  } 
  /*--------------------------
  FUNCION MOSTRAR LOS REVISTAS
  la variable $order sirve para organizar el orden de los minibanner esta variable 
  fue creada ya   que esta funcion se esta utilizando en el administrador pero en otro order*/
  public function revistas($consulta){
    $query=$this->db->query("SELECT * FROM revista ".$consulta);
    if ($query->num_rows>0) {
      return $query->result_array();
    }else{return false;}
  }  
  public function revistasdefinida(){
    $query=$this->db->query("SELECT * FROM revista order by id DESC LIMIT 0, 1");
    if ($query->num_rows>0) {
      return $query->result_array();
    }else{return false;}
  }              
}	//termina clase