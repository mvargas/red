<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class model_registro extends CI_Model 
{
  /*-------------------------------
  FUNCION PARA INSERTAR EL USUARIO*/	
  public function registro($datos){
  	$data=array(
	'nombre'     => $datos['nombre'], 
	'apellido'   => $datos['apellido'],
	'cedula'     => $datos['cedula'],
	'email'      => $datos['correo'],
	'contrasena' => $datos['contrasena'],
	'telefono'   => $datos['telefono'],
	'pais'       => $datos['pais'],
	'ciudad'     => $datos['ciudad'],
	'profesion'  => $datos['profesion'],
	'empresa'    => $datos['empresa'],
	'sector'     => $datos['sector'],
	'cargo'      => $datos['cargo'],
	'idtipoUsuario'=>1,
    );  
    return $this->db->insert('usuario',$data); 
  }	
  /*-----------------------------------------
  FUNCION PARA VERIFICAR SI EXISTE EL CORREO*/  
  public function verificacion_correo($correo){
   $this->db->where('email',$correo);
   $query=$this->db->get('usuario');
	   if ($query->num_rows>0) {
	   	return false;
	   }
  }

    /*-----------------------------------------
  FUNCION PARA VERIFICAR SI EXISTE EL CORREO*/  
  public function verificacion_cedula($cedula){
   $this->db->where('cedula',$cedula);
   $query=$this->db->get('usuario');
     if ($query->num_rows>0) {
      return false;
     }
  }
  /*-----------------------------------------
  FUNCION UTILIZADA PARA EL LOGIN DEL USUARIO*/ 
  public function verificacion_login($data){
   $this->db->where('email',$data['correo']);
   $this->db->where('contrasena',$data['contrasena']);
   $query=$this->db->get('usuario');
	   if ($query->num_rows>0) {
	   	return $query->result();
	   }else{
	   	return false;
	   }
  }
  /*---------------------------------------------------------
  FUNCION PARA ACTUALIZAR LA CONTRASEÑA PARA RECUPERAR LOGIN*/	
  public function Actualizar_contrasena($datos){
  $data=array('contrasena' => $datos['nueva_contrasena']);  
	$this->db->where('email',$datos['correo_user']);
	return $this->db->update('usuario',$data); 
  }	
  /*---------------------------------------
  FUNCION PARA MOSTRAR TODOS LOS SECTORES*/  
  public function Mostrar_sectores(){
  $query= $this->db->query("SELECT * FROM sectores");
  return $query->result(); 
  }  
  /*---------------------------------------
  FUNCION PARA MOSTRAR TODOS LOS PAISES*/  
  public function Mostrar_paises(){
  $query= $this->db->query("SELECT * FROM paises");
  return $query->result(); 
  }     
}