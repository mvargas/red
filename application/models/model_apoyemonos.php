<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class model_apoyemonos extends CI_Model {
  /*----------------------------------
  FUNCION PARA INSERTAR UNA SOLICITUD*/	
  public function insert_solicitud($datos){
  	$data=array(
	'idUsuario'     => $datos['id_users'], 
	'titulo'   => $datos['titulo'],
	'razon_solicitud'     => $datos['solicitud'],
	'fecha_solicitud' => date('Y-m-d'),
    );  
    return $this->db->insert('solicitud',$data); 
  }	
  /*---------------------------------------------------
  FUNCION PARA INSERTAR UNA RESPUESTA DE UNA SOLICITUD*/	
  public function insert_respuesta_solicitud($datos){
  	$data=array(
	'idUsuario'     => $datos['id_users'], 
	'idSolicitud'   => $datos['id_solicitud'],
	'respuesta_solicitudd' => $datos['respuesta_solicitud'],
	'fecha_respuesta' => date('Y-m-d'),
    );  
    return $this->db->insert('respuesta_solicitud',$data); 
  }	  
  /*---------------------------
  FUNCION PARA TRAER SOLICITUD */	
  public function mostrar_solicitudes(){
	  $this->db->where('estado',1); 
    $this->db->order_by("fecha_solicitud", "desc"); 
	  $query= $this->db->get('solicitud');
	  if ($query->num_rows>0) {
	  	return $query->result();
	  }else{
	  	return false;
	  }
  } 
  /*---------------------------
  FUNCION PARA TRAER SOLICITUD */	
  public function mostrar_respuesta_solicitud($id){
    $query= $this->db->query("SELECT usuario.nombre,usuario.apellido,respuesta_solicitud.fecha_respuesta,respuesta_solicitud.respuesta_solicitudd FROM respuesta_solicitud,usuario  WHERE respuesta_solicitud.idusuario=usuario.id AND respuesta_solicitud.idSolicitud=".$id);
    if ($query->num_rows>0) {
    	return $query->result();
    }else { return false; }    
  }    	
}