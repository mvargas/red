<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class model_servicios extends CI_Model 
{
  /*--------------------------------------
  FUNCION PARA INSERTAR LOS CLASIFICADOS*/	
  public function registro($datos){
  $data=array(
	'idUsuario'     => $datos['id'], 
	'titulo'   => $datos['titulo'],
	'descripcion'      => $datos['perfil'],
	'telefono' => $datos['telefono'],
	'correo'   => $datos['correo'],
	// 'costo'       => $datos['costo'],
	// 'forma_pago'     => $datos['forma_pago'],
	'ruta_img'  => $datos['ruta_img'],
  'ruta_img1'  => $datos['ruta_img1'],
  'ruta_img2'  => $datos['ruta_img2'],
  'ruta_img3'  => $datos['ruta_img3'],
  'ruta_img4'  => $datos['ruta_img4'],
	'fecha_publicado'    => $datos['fecha'],
  'tiempo'    => $datos['tiempo'],
   );  
    return $this->db->insert('clasificados',$data); 
  }
  /*--------------------------------------
  FUNCION PARA INSERTAR LOS CLASIFICADOS*/  
  public function registro_pequeno($datos){
    $data=array(
  'idUsuario'=> $datos['id'], 
  'titulo'   => $datos['titulo'],
  'descripcion'=> $datos['perfil'],
  'correo'   => $datos['correo'],
  'ruta_img'  => $datos['ruta_img'],
  'fecha_publicado'=> $datos['fecha'],
  );  
    return $this->db->insert('clasificados1',$data); 
  }  
  /*--------------------------------------------
  FUNCION PARA MOSTAR LOS CLASIFICADOS PAGADOS*/
  public function clasificados(){
    $query=$this->db->query("SELECT * FROM clasificados WHERE estado_clas=1 ORDER BY rand()");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}  	
  }
  /*--------------------------------------------
  FUNCION PARA MOSTAR LOS CLASIFICADOS PAGADOS*/
  public function clasificados1(){
    $query=$this->db->query("SELECT * FROM clasificados1 WHERE  estado_clas=1 ORDER BY rand()");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}  	
  }  
  /*---------------------------------------
  FUNCION PARA BUSCAR CLASIFICADOS GRANDES*/          
  public function buscador($palabra){
    $query=$this->db->query("SELECT * FROM clasificados WHERE  (titulo LIKE '%".$palabra."%' OR descripcion LIKE '%".$palabra."%') ORDER BY fecha_publicado ASC;");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  }
  /*---------------------------------------
  FUNCION PARA BUSCAR CLASIFICADOS PEQUEÑOS*/          
  public function buscador1($palabra){
    $query=$this->db->query("SELECT * FROM clasificados1 WHERE  (titulo LIKE '%".$palabra."%' OR descripcion LIKE '%".$palabra."%') ORDER BY fecha_publicado ASC;");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  }
  /*----------------------------------
  FUNCION PARA ELIMINAR UNA SOLICITUD*/          
  public function delete_clasificado($id){
    $this->db->where('id',$id);
    return $this->db->delete('clasificados');
  } 
  /*----------------------------------
  FUNCION PARA ELIMINAR UNA SOLICITUD*/          
  public function delete_clasificado1($id){
    $this->db->where('id',$id);
    return $this->db->delete('clasificados1');
  }  
  /*----------------------------------------------
  FUNCION TRAER DATOS DE UN CLASIFICADO POR SU ID*/  
  public function clasificado_id($id){
      $query=$this->db->query("SELECT * FROM clasificados WHERE id=".$id);
      if ($query->num_rows>0) {
        return $query->result_array();
      }else{return false;}
  }
  /*---------------------------------
  FUNCION PARA APROVAR EL CLASIFICADO*/          
  public function aprovar($id){
    $data=array('aprovado'  => 1);
    $this->db->where('id',$id);
    return $this->db->update('clasificados',$data);
  }

 /*--------------------------------------
  FUNCION PARA CONSULTAR EN EL DIRECTORIO*/  
  public function directorio($clas){
    $query=$this->db->query("SELECT DISTINCT(c.id),d.id, d.nombre_empresa, d.descripcion, d.persona_contacto, d.telefono, d.pais, d.ciudad, d.pagina_web, d.email, d.logotipo, d.banner, c.nombre, f.id_clasificacion, d.estado   
from directorio d, clasificacion c, directorio_hash_clasificacion f
where  c.id=".$clas." and c.id=f.id_clasificacion and d.id=f.id_directorio and d.estado=1;");
      if ($query->num_rows>0) {
        return $query->result();
      }else
      {
        return false;
      }
  }

  public function insert_clasificacion($idC){
    $query=$this->db->query("INSERT INTO directorio_hash_clasificacion (id_directorio,id_clasificacion) VALUES ((SELECT MAX(id) FROM directorio),".$idC.");");
  }

  /*--------------------------------------
  FUNCION PARA INSERTAR EN EL DIRECTORIO*/  
  public function ingresa_directorio($datos){
  $data=array(
  'nombre_empresa'   => $datos['nombre_empresa'],
  'descripcion'=> $datos['descripcion'],
  'persona_contacto'   => $datos['persona_contacto'],
  'telefono'   => $datos['telefono'],
  'email'   => $datos['email'],
  'pais'   => $datos['pais'],
  'ciudad'   => $datos['ciudad'],
  'pagina_web'   => $datos['pagina_web'],
  'estado'   => 0,
  'logotipo'  => $datos['logotipo'],
  //'clasificacion'  => $datos['clasificacion'],
  'banner'  => $datos['banner'],
  );  
    return $this->db->insert('directorio',$data);

  }

  /*---------------------------------------
  FUNCION PARA BUSCAR wen el directorio*/          
  public function buscador_directorio($palabra){
    $query=$this->db->query("SELECT * FROM directorio WHERE  (nombre_empresa LIKE '%".$palabra."%' OR descripcion LIKE '%".$palabra."%') ORDER BY nombre_empresa ASC;");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  }

   public function select_directorio($id){
    $query=$this->db->query("SELECT * FROM directorio WHERE  id=".$id.";");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  }

    
}//termina clase	