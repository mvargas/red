<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class model_miperfil extends CI_Model {
  /*---------------------------------------
  FUNCION consultar todos los usuarios*/  
  public function eliminar($id){
   return $query=$this->db->query("DELETE FROM usuario where id=".$id."");
  }
  /*-------------------------------------------------------------
  FUNCION VERIFICAR CONTRASEÑA DE USUARIO PARA EL CAMBIO DE ELLA*/  
  public function contrasena_verificacion($id,$contrasena){
   $this->db->where('id',$id);
   $this->db->where('contrasena',$contrasena);
   $query=$this->db->get('usuario');
	   if ($query->num_rows>0) {
	   	return true;
	   }else{
	   	return false;
	   }
  }
  /*-----------------------------------------------------------------
  FUNCION PARA ACTUALIZAR LA CONTRASEÑA DESPUES DE AVERLA RECUPERADO*/	
  public function actualizar_contrasena($datos){
  	$data=array('contrasena' => $datos['nueva_contrasena']);  
	$this->db->where('id',$datos['id']);
	return $this->db->update('usuario',$data); 
  }

  public function usuarios($id){
    $this->db->where('id',$id);
    $query = $this->db->get('usuario');
    if ($query->num_rows>0) {
      return $query->result();
    }else{
      return false;
    }
  }

  public function editarContrasena($datos){
    $data=array(
  'email'      => $datos['correo'],
  'contrasena' => $datos['contrasena'],
  'telefono'   => $datos['telefono'],
  'pais'       => $datos['pais'],
  'ciudad'     => $datos['ciudad'],
  'profesion'  => $datos['profesion'],
  'empresa'    => $datos['empresa'],
  'sector'     => $datos['sector'],
  'cargo'      => $datos['cargo'],
    ); 
    $this->db->where('id',$datos['id']); 
    return $this->db->update('usuario',$data); 
  }

  public function editar($datos){
    $data=array(
  'email'      => $datos['correo'],
  'telefono'   => $datos['telefono'],
  'pais'       => $datos['pais'],
  'ciudad'     => $datos['ciudad'],
  'profesion'  => $datos['profesion'],
  'empresa'    => $datos['empresa'],
  'sector'     => $datos['sector'],
  'cargo'      => $datos['cargo'],
    ); 
    $this->db->where('id',$datos['id']); 
    return $this->db->update('usuario',$data); 
  } 
  /*---------------------------------------
  FUNCION PARA MOSTRAR TODOS LOS SECTORES*/  
  public function Mostrar_sectores(){
  $query= $this->db->query("SELECT * FROM sectores");
  return $query->result(); 
  }  
  /*---------------------------------------
  FUNCION PARA MOSTRAR TODOS LOS PAISES*/  
  public function Mostrar_paises(){
  $query= $this->db->query("SELECT * FROM paises");
  return $query->result(); 
  }
  /*--------------------------------------------
  FUNCION PARA MOSTAR LOS CLASIFICADOS PAGADOS*/
  public function clasificados($idusuario){
    $query=$this->db->query("SELECT * FROM clasificados WHERE tipo_clasificado='pagado' AND idUsuario=".$idusuario." ORDER BY fecha_publicado,estado_clas ASC");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}    
  }
  /*--------------------------------------------
  FUNCION PARA MOSTAR LOS CLASIFICADOS PAGADOS*/
  public function clasificados1($idusuario){
    $query=$this->db->query("SELECT * FROM clasificados WHERE tipo_clasificado='gratis' AND idUsuario=".$idusuario." ORDER BY fecha_publicado,estado_clas ASC");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}    
  }     
}