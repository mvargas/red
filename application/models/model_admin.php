<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class model_admin extends CI_Model {
  /*---------------------------------------
  FUNCION consultar todos los usuarios*/  
  public function usuarios(){
      $query=$this->db->query("SELECT * FROM usuario where id !=1 order by id desc");
      if ($query->num_rows>0) {
        return $query->result_array();
      }else
      {
        return false;
      }
  }
   /*** cambia estado de los users*/  
  public function cambiar_estado($id,$nuevo_tipo){
      $data= array('idtipoUsuario'=>$nuevo_tipo);
      $this->db->where('id',$id);
      return $this->db->update('usuario',$data);
  }

     /*** muestra tipo de ususario a consultar*/  
  public function usuarios_id($id){
    $this->db->where('id',$id);
    $query=$this->db->get('usuario');
     if ($query->num_rows>0){
      return $query->result();
     }
     else {
      return false;
     }
  }
    /*-------------------------------------
  insertar noticias*/
  public function insertarnoticia($datos){
    $data=array(
      'titulo'     => $datos['titulo'],
      'noticia'        => $datos['noticia'],
      'img'          => $datos['img']);
    return $this->db->insert('noticia',$data);
  }
  public function updatenoticiaImg($datos){
    $data=array(
      'titulo'     => $datos['titulo'],
      'noticia'        => $datos['noticia'],
      'img'          => $datos['img']);
    $this->db->where('id',$datos['id']);
    return $this->db->update('noticia',$data);
  }
  public function updatenoticia($datos){
    $data=array(
      'titulo'     => $datos['titulo'],
      'noticia'        => $datos['noticia']);
    $this->db->where('id',$datos['id']);
    return $this->db->update('noticia',$data);
  }
  

  public function cambio_password($id,$contrasena){
      $data= array('contrasena'  =>$contrasena);
      $this->db->where('id',$id);
      return $this->db->update('usuario',$data); 
    } 

  public function ofertas_sinAutorizar(){
        $this->db->order_by("id", "desc");
        $query = $this->db->get('ofertas');
        if ($query->num_rows>0) {
          return $query->result();
        }else{
          return false;
        }
  }
  public function publicar($id){
      $data= array('estado'  =>1);
      $this->db->where('id',$id);
      return $this->db->update('ofertas',$data); 
  } 
  public function cancelar_oferta($id){
      $data= array('estado'  =>0);
      $this->db->where('id',$id);
      return $this->db->update('ofertas',$data); 
  }
  //* insertar nosotros*/
  public function insertarNosotros($datos){
    $data=array(
      'titulo'     => $datos['titulo'],
      'texto'        => $datos['contenido'],
      'foto'          => $datos['img']);
    return $this->db->insert('nosotros',$data);
  } 
  //  FUNCION PARA actualizar ofertas con imagen*/
  public function updateOfertasimg($datos){
    $data=array(
      'titulo'        => $datos['titulo'],
      'logo'          => $datos['logo'],
      'perfil'        => $datos['perfil'],
      'salario'       => $datos['salario'],
      'tipo_contrato' => $datos['contrato'],
      'pais'          => $datos['pais'],
      'ciudad'        => $datos['ciudad'],
      'fecha'         => $datos['fecha'],
    
    );
    $this->db->where('id',$datos['id']);
    return $this->db->update('ofertas',$data);
  } 
  //  FUNCION PARA actualizar ofertas sin imagen*/
  public function updateOfertas($datos){
    $data=array(
      'titulo'        => $datos['titulo'],
      'perfil'        => $datos['perfil'],
      'salario'       => $datos['salario'],
      'tipo_contrato' => $datos['contrato'],
      'pais'          => $datos['pais'],
      'ciudad'        => $datos['ciudad'],
      'fecha'         => $datos['fecha'],
    
    );
    $this->db->where('id',$datos['id']);
    return $this->db->update('ofertas',$data);
  } 

  public function eliminarOferta($id){
    $this->db->where('id',$id);
   return $this->db->delete('ofertas'); 
  } 

  public function eliminarNosotros($id){
    $this->db->where('id',$id);
   return $this->db->delete('nosotros'); 
  } 
  public function eliminarNoticia($id){
    $this->db->where('id',$id);
   return $this->db->delete('noticia'); 
  }
  public function nosotros($id){
    $this->db->where('id',$id);
    $query=$this->db->get('nosotros');
      if ($query->num_rows>0) {
        return $query->result();
      }else
      {
        return false;
      }
  }

  public function updateNosotrosimg($datos){
    $data=array(
      'titulo'     => $datos['titulo'],
      'texto'        => $datos['contenido'],
      'foto'          => $datos['img']);
    $this->db->where('id',$datos['id']);
    return $this->db->update('nosotros',$data);
  }
  public function updateNosotros($datos){
    $data=array(
      'titulo'     => $datos['titulo'],
      'texto'        => $datos['contenido']);
    $this->db->where('id',$datos['id']);
    return $this->db->update('nosotros',$data);
  }

   public function buscar($palabra){
      $query=$this->db->query("SELECT* FROM usuario where id !=1 and (nombre like '%".$palabra."%' or apellido like '%".$palabra."%' or email like '%".$palabra."%' or pais like '%".$palabra."%' or ciudad like '%".$palabra."%' or profesion like '%".$palabra."%' or empresa like '%".$palabra."%' or sector like '%".$palabra."%' or cargo like '%".$palabra."%' or telefono like '%".$palabra."%');");
      if ($query->num_rows>0) {
        return $query->result_array();
      }else
      {
        return false;
      }

  }

  function getAllPaginated($per_page,$segment) {
      $st='id!=1';
      $this->db->where($st, NULL, FALSE); 
      $this->db->order_by('id desc');
      $q = $this->db->get('usuario',$per_page,$segment);
      $data["usuario"]=array();
      if ($q->num_rows() > 0)
      {
        foreach ($q->result() as $row)
        {
          $data["usuario"][$row->id]["id"] = $row->id;
          $data["usuario"][$row->id]["nombre"] = $row->nombre;
          $data["usuario"][$row->id]["apellido"] = $row->apellido;
          $data["usuario"][$row->id]["cedula"] = $row->cedula;
          $data["usuario"][$row->id]["email"] = $row->email;
          $data["usuario"][$row->id]["telefono"] = $row->telefono;
          $data["usuario"][$row->id]["pais"] = $row->pais;
          $data["usuario"][$row->id]["ciudad"] = $row->ciudad;
          $data["usuario"][$row->id]["profesion"] = $row->profesion;
          $data["usuario"][$row->id]["empresa"] = $row->empresa; 
          $data["usuario"][$row->id]["sector"] = $row->sector;
          $data["usuario"][$row->id]["cargo"] = $row->cargo;
          $data["usuario"][$row->id]["idtipoUsuario"] = $row->idtipoUsuario;
        }
      }
      return $data["usuario"];
    }
 
    function totalCustomers()
    {
       $q = $this->db->get('usuario');
      return  $q->num_rows() ;
    }

    public function NombreUsuarios($idoferta){
      $query=$this->db->query("SELECT o.titulo, u.nombre,u.email FROM ofertas o, usuario u where o.id=".$idoferta." and o.idUsuario=u.id;");
      if ($query->num_rows>0) {
        return $query->result();
      }else
      {
        return false;
      }

  }

  public function ofertas_filtro($estado){
        $this->db->where('estado',$estado);
        $this->db->order_by("id", "desc");
        $query = $this->db->get('ofertas');
        if ($query->num_rows>0) {
          return $query->result();
        }else{
          return false;
        }
  }

  public function votos($idpregunta,$idrespuesta){
      $query=$this->db->query("SELECT COUNT(*) cont FROM votos WHERE idPreguntas=".$idpregunta." and idRespuesta=".$idrespuesta.";");
      if ($query->num_rows>0) {
        return $query->result();
      }else
      {
        return false;
      }

  }

  public function insert_pregunta($datos){
    $data=array(
      'pregunta'  => $datos['pregunta']);
    return $this->db->insert('preguntas',$data);
  } 
  public function insert_respuesta($datos){
    $data=array(
      'idPreguntas'  => $datos['idpregunta'],
      'respuesta'  => $datos['respuesta']);
    return $this->db->insert('respuestas',$data);
  }  

  public function desactivar_encuesta(){
    $data=array(
      'estado'     => 0);
    return $this->db->update('preguntas',$data);
  } 
  public function activar_encuesta($id){
    $data=array(
      'estado'     => 1);
    $this->db->where('id',$id);
    return $this->db->update('preguntas',$data);
  }
  public function borrar_ip(){
    $query=$this->db->query("DELETE FROM ip");
        return true;
  }  
  /*-----------------------------------
  FUNCION INSERTAR IMAGEN PARA LA WEB*/
  public function inserta_imagen($datos){
    $data=array(
      'ruta' => $datos['ruta'],
      'estado'  => $datos['estado'],
      'tipo'    => $datos['tipo'],
      'url'    => $datos['url'],
      );
    return $this->db->insert('imagen_web',$data);
  }
  /*------------------------------
  FUNCION MOSTRAR IMAGEN DEL HOME*/
  public function imagen_home(){
    $query=$this->db->query("SELECT * FROM imagen_web WHERE tipo=1 OR tipo=2");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  }
  /*------------------------------
  FUNCION MOSTRAR IMAGEN DE PAUTAS*/
  public function imagen_putas(){
    $query=$this->db->query("SELECT * FROM imagen_web WHERE tipo=3 OR tipo=4");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  } 
  /*------------------------------
  FUNCION MOSTRAR IMAGEN DE PAUTAS*/
  public function imagen_pagina(){
    $query=$this->db->query("SELECT * FROM imagen_web WHERE tipo=5");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  }   
  /*-----------------------------------
  FUNCION MOSTRAR IMAGEN CON ESTADO 1*/
  public function img_home_estado(){
    $query=$this->db->query("SELECT * FROM imagen_web WHERE estado=1");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  }
  /*------------------------------------------------
  FUNCION ACTUALIZAR EL ESTADO DE LA IMAGEN DE HOME*/          
  public function update_estado_img($id){
    $data=array('estado'  => 0);
    $this->db->where('id',$id);
    return $this->db->update('imagen_web',$data);
  }
  /*------------------------------------------------
  FUNCION ACTUALIZAR EL ESTADO DE UNA IMAGEN DE HOME*/          
  public function img_home_estado_new($id){
    $data=array('estado'  => 1);
    $this->db->where('id',$id);
    return $this->db->update('imagen_web',$data);
  }  
  /*---------------------------------------
  FUNCION PARA ELIMINAR UNA IMAGEN DE HOME*/          
  public function delete_img_home($id){
    $this->db->where('id',$id);
    return $this->db->delete('imagen_web');
  } 
  /*----------------------------------------
  FUNCION MOSTRAR IMAGEN DEL HOME CON SU ID*/
  public function imagen_home_id($id){
    $query=$this->db->query("SELECT * FROM imagen_web WHERE id=".$id);
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  }  
  /*----------------------------------------
  FUNCION MOSTRAR IMAGEN DEL HOME CON SU ID*/
  public function mostrar_solicitudes(){
    $query=$this->db->query("SELECT titulo,nombre,email,razon_solicitud,solicitud.estado,solicitud.id,fecha_solicitud FROM usuario,solicitud WHERE solicitud.idUsuario=usuario.id ORDER BY solicitud.estado,fecha_solicitud DESC;");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  }     
  /*-------------------------------------
  FUNCION PARA SACAR ESTADO DE SOLICITUD*/          
  public function solicitud_id($id){
  $this->db->where('id',$id);
  $query = $this->db->get('solicitud');
  if ($query->num_rows>0) {
    return $query->result();
  }else{return false;}
  }
  /*-------------------------------------
  FUNCION PARA CAMBIAR ESTADO DE SOLICITUD*/          
  public function update_solicitud($datos){
    $data=array('estado'  => $datos['estado']);
    $this->db->where('id',$datos['id']);
    return $this->db->update('solicitud',$data);
  }   
  /*----------------------------------
  FUNCION PARA ELIMINAR UNA SOLICITUD*/          
  public function delete_solicitud($id){
    $this->db->where('id',$id);
    return $this->db->delete('solicitud');
  } 
  /*--------------------------------------------
  FUNCION PARA BUSCAR UNA SOLICITUD POR PALABRAS*/          
  public function buscar_solicitud($palabra){
    $query=$this->db->query("SELECT titulo,nombre,razon_solicitud,solicitud.estado,solicitud.id,fecha_solicitudm, FROM usuario,solicitud WHERE solicitud.idUsuario=usuario.id AND (titulo LIKE '%".$palabra."%' OR razon_solicitud LIKE '%".$palabra."%') ORDER BY solicitud.estado,fecha_solicitud ASC;");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  }
  /*----------------------------------------------------
  FUNCION PARA BUSCAR POR FILTRO SOLICITUD POR PALABRAS*/          
  public function filtro_solicitud($filtro){
    $query=$this->db->query("SELECT titulo,nombre,razon_solicitud,solicitud.estado,solicitud.id,fecha_solicitud FROM usuario,solicitud WHERE solicitud.idUsuario=usuario.id AND solicitud.estado=".$filtro." ORDER BY solicitud.estado,fecha_solicitud ASC;");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  } 
  /*---------------------------------------
  FUNCION PARA TRAER DATOS SOLICITUD POR ID*/          
  public function select_solicitud($id){
    $query=$this->db->query("SELECT * FROM solicitud WHERE id=".$id);
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  }   
  /*---------------------------------
  FUNCION PARA ACTUALIZAR SOLICITUD*/          
  public function update_solicitud_id($datos){
    $data=array('titulo'  => $datos['titulo'],'razon_solicitud'  => $datos['solicitud']);
    $this->db->where('id',$datos['id']);
    return $this->db->update('solicitud',$data);
  }  
  /*--------------------------------------------
  FUNCION PARA MOSTAR LOS CLASIFICADOS PAGADOS*/
  public function clasificados(){
    $query=$this->db->query("SELECT *,clasificados.id as idC FROM clasificados,usuario WHERE usuario.id=clasificados.idUsuario AND aprovado=1 ORDER BY fecha_publicado,estado_clas ASC");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}    
  }
  /*--------------------------------------------
  FUNCION PARA MOSTAR LOS CLASIFICADOS PAGADOS*/
  public function clasificados1(){
    $query=$this->db->query("SELECT * FROM clasificados1 ORDER BY fecha_publicado,estado_clas ASC");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}    
  }  
  /*---------------------------------------
  FUNCION PARA BUSCAR CLASIFICADOS GRANDES*/          
  public function buscador_clasificados($palabra){
    $query=$this->db->query("SELECT * FROM clasificados WHERE tipo_clasificado='pagado' AND aprovado=1 AND  (titulo LIKE '%".$palabra."%' OR descripcion LIKE '%".$palabra."%') ORDER BY fecha_publicado,estado_clas ASC;");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  }
  /*---------------------------------------
  FUNCION PARA BUSCAR CLASIFICADOS PEQUEÑOS*/          
  public function buscador_clasificados1($palabra){
    $query=$this->db->query("SELECT * FROM clasificados WHERE tipo_clasificado='gratis' AND  (titulo LIKE '%".$palabra."%' OR descripcion LIKE '%".$palabra."%') ORDER BY fecha_publicado,estado_clas ASC;");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  }
  /*---------------------------------
  FUNCION PARA ACTUALIZAR CLASIFICADO*/          
  public function activar_clasificado($id){
    $data=array('estado_clas'  => 1);
    $this->db->where('id',$id);
    return $this->db->update('clasificados',$data);
  }
    /*---------------------------------
  FUNCION PARA ACTUALIZAR CLASIFICADO*/          
  public function activar_clasificadoP($id){
    $data=array('estado_clas'  => 1);
    $this->db->where('id',$id);
    return $this->db->update('clasificados1',$data);
  }
  /*----------------------------------
  FUNCION PARA ELIMINAR UNA SOLICITUD*/          
  public function delete_clasificado($id){
    $this->db->where('id',$id);
    return $this->db->delete('clasificados');
  } 
  /*----------------------------------
  FUNCION PARA ELIMINAR UNA SOLICITUD*/          
  public function delete_clasificadoP($id){
    $this->db->where('id',$id);
    return $this->db->delete('clasificados1');
  }  
  /*----------------------------------------------------
  FUNCION PARA BUSCAR POR FILTRO CLASIFICADOS POR PALABRAS*/          
  public function filtro_clasificados($filtro){
    $query=$this->db->query("SELECT * FROM clasificados WHERE tipo_clasificado='pagado' AND aprovado=1  AND  estado_clas=".$filtro." ORDER BY fecha_publicado,estado_clas ASC;");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  } 
  /*----------------------------------------------------
  FUNCION PARA BUSCAR POR FILTRO CLASIFICADOS POR PALABRAS*/          
  public function filtro_clasificados1($filtro){
    $query=$this->db->query("SELECT * FROM clasificados WHERE tipo_clasificado='gratis' AND  estado_clas=".$filtro." ORDER BY fecha_publicado,estado_clas ASC;");
    if ($query->num_rows>0) {
      return $query->result();
    }else{return false;}
  }  
  /*----------------------------------------------
  FUNCION TRAER DATOS DE UN CLASIFICADO POR SU ID*/  
  public function clasificado_id($id){
      $query=$this->db->query("SELECT * FROM clasificados WHERE id=".$id."");
      if ($query->num_rows>0) {
        return $query->result_array();
      }else{return false;}
  }
  /*----------------------------------------------
  FUNCION TRAER DATOS DE UN CLASIFICADO POR SU ID*/  
  public function clasificado_idP($id){
      $query=$this->db->query("SELECT * FROM clasificados1 WHERE id=".$id."");
      if ($query->num_rows>0) {
        return $query->result_array();
      }else{return false;}
  }    
  /*------------------------------------
  FUNCION PARA ACTUALIZAR CLASIFICADOS*/  
  public function update_clasificado($datos){
    $data=array(
    'titulo'=>$datos['titulo'],
    'descripcion'=>$datos['perfil'],
    'telefono'=>$datos['telefono'],
    'correo'=>$datos['correo'],
    'costo'=>$datos['costo'],
    'forma_pago'=>$datos['forma_pago'],
    'ruta_img'=>$datos['ruta_img'],
    'ruta_img1'=>$datos['ruta_img1'],
    'ruta_img2'=>$datos['ruta_img2'],
    'ruta_img3'=>$datos['ruta_img3'],
    'ruta_img4'=>$datos['ruta_img4'],
    'tiempo'=>$datos['tiempo'],
    );
    $this->db->where('id',$datos['id']);
    return $this->db->update('clasificados',$data);
  }    
  /*------------------------------------
  FUNCION PARA ACTUALIZAR CLASIFICADOS*/  
  public function update_clasificado1($datos){
    $data=array(
    'titulo'=>$datos['titulo'],
    'descripcion'=>$datos['perfil'],
    'telefono'=>$datos['telefono'],
    'correo'=>$datos['correo'],
    'costo'=>$datos['costo'],
    'forma_pago'=>$datos['forma_pago'],
    'tiempo'=>$datos['tiempo'],
    );
    $this->db->where('id',$datos['id']);
    return $this->db->update('clasificados',$data);
  } 
  /*---------------------------------------------------
  FUNCION PARA USUARIOS DEPENDIENDO DE SU TIPO USUARIO*/          
  public function usuario_tipo($tipo){
  $this->db->where('idtipoUsuario',$tipo);
  $query = $this->db->get('usuario');
  if ($query->num_rows>0) {
    return $query->result();
  }else{return false;}
  } 
  /*-------------------------------
  FUNCION PARA ELIMINAR UN USUARIO*/
  public function eliminar_user($id){
    $this->db->where('id',$id);
    $query=$this->db->delete('usuario');
    return $query;
  }

    /*/////Funcion para colsultaslas ofertas con el id/////*/
   public function ofertasUser($id){
      $this->db->where('idUsuario',$id);
      $query=$this->db->get('ofertas');
      if ($query->num_rows>0) {
      return true;
      }else{
      return false;
      }
   }
   /*-----------------------------------
   FUNCION PARA INSERTAR UN MINI-BANNER*/     
   public function insert_miniBanner($datos)
   {
    $data=array(
      'nombre_img'=>$datos['nombre_img'],
      'url_img'=>$datos['url_img'],
    );
    return $this->db->insert('miniBanner',$data);
   }
   /*-----------------------------------
   FUNCION PARA ELIMINAR  UN MINI-BANNER*/
  public function delete_miniBanner($id){
    $this->db->where('id',$id);
    $query=$this->db->delete('miniBanner');
    return $query;
  }
  /*---------------------------------
  FUNCION PARA INSERTAR UNA REVISTA*/ 
  public function insert_revista($datos)
  {
    $data=array(
      'nombre'=>$datos['nombre'],
      'ubicacion'=>$datos['ubicacion'],
    );
    return $this->db->insert('revista',$data);
  }
   /*------------------------------------------
   FUNCION PARA ELIMINAR  UNA REVISTA DE LA BD*/
  public function revista_delete($id){
    $this->db->where('id',$id);
    $query=$this->db->delete('revista');
    return $query;
  }  
   /*-----------------------------------
   FUNCION PARA INSERTAR UN MINI-BANNER*/     
   public function update_revista($datos)
   {
    $data=array(
      'nombre'=>$datos['nombre'],
      'ubicacion'=>$datos['ubicacion'],
    );
    $this->db->where('id',$datos['id']);
    return $this->db->update('revista',$data);
   }  

    /*------------------------------
  FUNCION MOSTRAR LOS MINI-BANNER*/
  public function miniBanner($orden){
    $query=$this->db->query("SELECT * FROM miniBanner ");
    if ($query->num_rows>0) {
      return $query->result_array();
    }else{return false;}
  }

  /*FUNCIONES DE DIRECTORIO*/
  public function directorio(){
    $query=$this->db->query("SELECT DISTINCT(c.id),d.id, d.nombre_empresa, d.descripcion, d.persona_contacto, d.telefono, d.pais, d.ciudad, d.pagina_web, d.email, d.logotipo, d.banner, c.nombre, f.id_clasificacion, d.estado   
from directorio d, clasificacion c, directorio_hash_clasificacion f
where  c.id=f.id_clasificacion and d.id=f.id_directorio group by d.id");
      if ($query->num_rows>0) {
        return $query->result();
      }else
      {
        return false;
      }
  }

  public function directorio_id($id){
      $query=$this->db->query("SELECT * FROM directorio WHERE id=".$id."");
      if ($query->num_rows>0) {
        return $query->result_array();
      }else{return false;}
  }

  public function clasificacion(){
      $query=$this->db->query("SELECT c.nombre, h.id_directorio, h.id_clasificacion FROM directorio_hash_clasificacion h, clasificacion c WHERE c.id = h.id_clasificacion");
      if ($query->num_rows>0) {
        return $query->result_array();
      }else{return false;}
  }

  public function insert_clasificacion($idC, $idD){
    $query=$this->db->query("INSERT INTO directorio_hash_clasificacion (id_directorio,id_clasificacion) VALUES (".$idD.",".$idC.");");
  }

  public function clasificacion_id($id){
      $query=$this->db->query("SELECT c.nombre, h.id_directorio, h.id_clasificacion, d.id FROM directorio_hash_clasificacion h, clasificacion c, directorio d WHERE c.id = h.id_clasificacion and h.id_directorio=".$id);
      if ($query->num_rows>0) {
        return $query->result_array();
      }else{return false;}
  }

  public function borrado_clasificacion($idD){
    $query=$this->db->query("DELETE FROM directorio_hash_clasificacion WHERE id_directorio=".$idD.";");
    return $query;
  }

  public function delete_directorio($id){
    $this->db->where('id',$id);
    return $this->db->delete('directorio');
  } 
  public function delete_directorio_clasificacion($id){
    $this->db->where('id_directorio',$id);
    return $this->db->delete('directorio_hash_clasificacion');
  } 
  
  public function activate_directorio($id){
    $query=$this->db->query("UPDATE directorio SET estado='1' WHERE id=".$id.";");
  }

  public function desactivate_directorio($id){
    $query=$this->db->query("UPDATE directorio SET estado='0' WHERE id=".$id.";");
  }

  public function edit_directorio($datos){
    
    $data=array(
    'nombre_empresa'=>$datos['nombre_empresa'],
    'descripcion'=>$datos['descripcion'],
    'persona_contacto'=>$datos['persona_contacto'],
    'telefono'=>$datos['telefono'],
    'email'=>$datos['email'],
    'pagina_web'=>$datos['pagina_web'],
    'ciudad'=>$datos['ciudad'],
    'pais'=>$datos['pais'],
    //'clasificacion'=>$datos['clasificacion'],
    'logotipo'=>$datos['logotipo'],
    'banner'=>$datos['banner'],
    );
    $this->db->where('id',$datos['id']);
    return $this->db->update('directorio',$data);
  }
}//fin de la clase