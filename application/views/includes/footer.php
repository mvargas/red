  <footer>
    <div class="row">
      <div class="copy">Todos los derechos reservados © 2013. Red de Gestión Humana.</div>
    </div>
  </footer><!-- end footer -->
</div>
  


<<!-- Included JS Files (Compressed) -->                      
<script src="<?php echo base_url('js/jquery.js'); ?>"></script>
<script src="<?php echo base_url('js/foundation.min.js');?>"></script>
<!-- jQuery validate -->
<script src="<?php echo base_url('js/jquery.validate.js'); ?>"></script>
<!-- Fancybox -->
<script type="text/javascript" src="<?php echo base_url('js/fancybox/jquery.mousewheel-3.0.6.pack.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/fancybox/source/jquery.fancybox.js'); ?>"></script>
<!-- carousel.js-->
<script src="<?php echo base_url('js/carousel.js'); ?>"></script>


<!-- App.js-->
<script src="<?php echo base_url('js/app.js'); ?>"></script>
<script src="<?php echo base_url('js/buscadorPais.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function() {

    $("select").searchable();

});
</script>


<?php if ($view == "ofertas/index" OR $view == "ofertas/sinOfertas" OR $view == "apoyemonos/index" OR $view == "servicios/index"): 
 echo $msg; 
endif ?>
</body>
</html>