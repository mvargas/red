<nav id="admin_nav">
    <ul>
        <li><?php echo anchor('admin/estado_users','Listado de Usuarios'); ?>
        <li><?php echo anchor('admin/password','Cambiar Contraseña'); ?></li>
        <li><?php echo anchor('admin/ver_noticias','Noticias'); ?></li>
        <li><?php echo anchor('admin/noticias','Crear Noticia'); ?></li>
        <li><?php echo anchor('admin/autorizar_ofertas','Ofertas'); ?></li>
        <li><?php echo anchor('admin/ver_nosotros','Nosotros'); ?></li>
        <li><?php echo anchor('admin/nosotros','Crear Nosotros'); ?></li>
        <li><?php echo anchor('admin/encuesta','Ver Encuesta'); ?></li>
        <li><?php echo anchor('admin/crear_encuesta','Crear Encuesta'); ?></li>
        <li><?php echo anchor('admin/miniBanner','Mini Banner'); ?></li>
        <li><?php echo anchor('admin/vista/banner','Banner Home'); ?></li>
        <li><?php echo anchor('admin/vista/pautas','Pautas'); ?></li>
        <li><?php echo anchor('admin/solicitudes','Solicitudes'); ?></li>
        <li><?php echo anchor('admin/clasificados','Clasificados'); ?></li>
        <li><?php echo anchor('admin/bakcup','Base de Datos'); ?></li>
        <li><?php echo anchor('admin/referidas','Hojas de Vida'); ?></li>
        <li><?php echo anchor('admin/pagina_img','Imagenes de Pagina'); ?></li>
        <li><?php echo anchor('admin/revistas','Revistas'); ?></li>
        <li><?php echo anchor('admin/directorio','Directorio'); ?></li>
    </ul>
</nav><!-- end admin_nav -->