<div class="columns twelve">
    <nav id="main_nav">
      <ul>
        <li <?php if ($view == "index/index"): ?>class="current"<?php endif ?>><?php echo anchor('index','INICIO'); ?></li>
        <li <?php if ($view == "nosotros/index"): ?>class="current"<?php endif ?>><?php echo anchor('nosotros','NOSOTROS'); ?></li>
        <?php if ($this->session->userdata('id_user_red')==0){?>
        <li <?php if ($view == "registro/index"): ?>class="current"<?php endif ?>><?php echo anchor('registro','REGISTRO'); ?></li>
        <?php }?>
        <li <?php if ($view == "ofertas/index"): ?>class="current"<?php endif ?>><?php echo anchor('ofertas','OFERTAS DE EMPLEO'); ?></li>
        <?php if ($this->session->userdata('tipo_user_red')==2 OR $this->session->userdata('tipo_user_red')==3){?>
        <li <?php if ($view == "apoyemonos/index"): ?>class="current"<?php endif ?>><?php echo anchor('apoyemonos','APOYÁNDONOS'); ?></li>
        <?php }?>
        <li <?php if ($view == "noticias/index"): ?>class="current"<?php endif ?>><?php echo anchor('index/noticias','NOTICIAS'); ?></li>
        <li <?php if ($view == "index/revista"): ?>class="current"<?php endif ?>><?php echo anchor('index/revista','REVISTA'); ?></li>
        <li <?php if ($view == "servicios/index"): ?>class="current"<?php endif ?>><a href="">SERVICIOS</a>
            <ul class="submenu">
                <li><?php echo anchor('servicios','Clasificados'); ?></li>
               
                <li><?php echo anchor('servicios/directorio','Directorio'); ?></li>
            </ul>
                <!--<li><a href="">Convenios</a></li>
                <li><a href="">Convesatorios</a></li>
            </ul>
        </li>-->
        <li <?php if ($view == "index/contactenos"): ?>class="current"<?php endif ?>><?php echo anchor('index/contactenos','CONTÁCTENOS'); ?></li>
      </ul>
    </nav><!-- end main_nav -->
</div>
