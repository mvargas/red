<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/********
template el cual me cargara el header la vista que le envio desde el controlador
y el footer
*********/
$data['view']=$view;
$this->load->view('includes/header',$data);
$this->load->view($view);
$this->load->view('includes/footer');