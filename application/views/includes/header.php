<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-css-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />
  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>Red de Gestión Humana</title>
  
  <!-- Included CSS Files (Compressed) -->
  <link rel="stylesheet" href="<?php echo base_url('css/bootstrap.css'); ?>"> 
  <link rel="stylesheet" href="<?php echo base_url('css/foundation.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('js/fancybox/source/jquery.fancybox.css'); ?>" media="screen" />
  <link rel="stylesheet" href="<?php echo base_url('css/app.css'); ?>">
</head>
<body>
  <div id="wrapper">
    <div id="social_networks">
      <ul>
        <li class="twitter"><a href="https://twitter.com/ReddeGH" target="_blank" title="Twitter">Twitter</a></li>
        <li class="facebook"><a href="http://www.facebook.com/reddegh.gestionhumana" target="_blank" title="Facebook">Facebook</a></li>
        <!--<li class="flickr"><a href="#" target="_blank" title="Flickr">Flickr</a></li>
        <li class="youtube"><a href="#" target="_blank" title="Youtube">Youtube</a></li>-->
        <li class="linkedin"><a href="http://www.linkedin.com/groups/Red-Gestion-Humana-REDdeGH-3391529" target="_blank" title="Linkedin">Linkedin</a></li>
      </ul>
    </div>
    <header>
      <div class="top_bar">
        <div class="row">
          <?php if ($this->session->userdata('id_user_red')) {

           echo '<div id="copy_loged"> <span>Bienvenido....'.$this->session->userdata('nombre_user_red').'</span>'.anchor('miPerfil/index',' Mi Perfil', array("class" => "Perfil")).anchor('registro/loginout',' Cerrar sesión', array("class" => "cerrar_sesion")).'</div>';
          }else{ ?>
            <form action="" id="login_form">
            <label for="user">Correo:</label>
            <input type="text" id="correo_login" name="correo_login">
            <label for="pass">Contraseña:</label>
            <input type="password" id="pass_login" name="pass_login">
            <label class="error"></label>
            <input type="button" class="send" value="Ingresar" id="ingresar_login">
            </form>
            <div id="recuperar_contrasena" class="fancy_skin_redgh" style="width:500px;display: none;">
              <h2>Para recuperar tu contraseña por favor Ingresa tu correo electronico</h2>
              <form action="" id="formulario_recuperar">
              <label for="user">Correo:</label>
              <input type="text" id="correo_recuperar" name="correo_recuperar">
              <label class="error_recuperar"></label>
              <input type="button" class="button required" value="Enviar solicitud" id="ingresar_login" onclick="Cambio_contrasena()">
              </form>
            </div>
          <?php }?>
        </div>
      </div><!-- end top_bar -->
      <div class="main_header">
        <div class="header_top">
          <div class="row">
            <div class="columns four">
              <a href="<?php echo base_url(); ?>" id="logo">
                <img src="<?php echo base_url('images/logo.png'); ?>" alt="">
              </a>
            </div>
          </div>
        </div><!-- end header_top -->
        <div class="bar_nav">
          <div class="row">
            <?php $this->load->view("includes/partials/nav.php"); ?>
          </div>
        </div><!-- end header_nav -->
        <?php if ($view == "index/index"): ?>
        <div class="row">
          <div class="colums twelve">
            <div class="banner_top">
              <div class="inner_banner_top">
                <?php 
                 if ($img_home!= false) {
                   foreach ($img_home as $mostrar){
                      if ($mostrar->tipo==1) {
                        echo '<a href="'.$mostrar->url.'" target="_blank"><img id="img_home"src="'.base_url($mostrar->ruta).'"></a>';  
                      }else{
                        echo '<object  width="940" height="700">';
                        echo '<param name="movie" value="'.base_url($mostrar->ruta).'" />';
                        echo '<param name="quality" value="high" />';
                        echo '<embed src="'.base_url($mostrar->ruta).'" quality="high" type="application/x-shockwave-flash" width="940" height="700" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>';
                        echo '</object>';
                      }
                   }
                 }else{ echo '<img id="img_home"src="'.base_url().'images/pauta_home.jpg">';}
                ?>
              </div>
            </div><!-- end banner_top -->
          </div>
        </div>
        
        <?php endif ?>
      </div><!-- end main_header -->
    </header><!-- end header -->