    <section class="container row">
      <div class="row block">
        <div class="twelve columns">
          <div id="main_magazine">
            <div><object style="width:1000px;height:430px" ><param name="movie" value="http://static.issuu.com/webembed/viewers/style1/v1/IssuuViewer.swf?mode=embed&amp;layout=http%3A%2F%2Fskin.issuu.com%2Fv%2Flight%2Flayout.xml&amp;showFlipBtn=true&amp;documentId=130109042006-cfd09fa5dc7c49fe93665494be0a07b0&amp;docName=revista_conexiones_12va_edicion&amp;username=reddegh&amp;loadingInfoText=12va%20edicion&amp;et=1360715444158&amp;er=86" /><param name="allowfullscreen" value="true"/><param name="menu" value="false"/><param name="allowscriptaccess" value="always"/><embed src="http://static.issuu.com/webembed/viewers/style1/v1/IssuuViewer.swf" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" menu="false" style="width:1000px;height:430px" flashvars="mode=embed&amp;layout=http%3A%2F%2Fskin.issuu.com%2Fv%2Flight%2Flayout.xml&amp;showFlipBtn=true&amp;documentId=130109042006-cfd09fa5dc7c49fe93665494be0a07b0&amp;docName=revista_conexiones_12va_edicion&amp;username=reddegh&amp;loadingInfoText=12va%20edicion&amp;et=1360715444158&amp;er=86" /></object></div>
             <!--<?php /*if ($definida!=false) {foreach ($definida as $value) {
               echo $value['ubicacion']; 
              }} */?>-->
          </div><!-- end main_magazine -->
        </div>
      </div><!-- end block -->
      <div class="row block">
        <div class="columns twelve">
          <h2>Ediciones Anteriores:</h2>
          <p>La Revista CONEXIONES es un medio en el cual los miembros de la REDDEGH pueden escribir artículos, presentar eventos corporativos, realizar propuestas sobre mejores practicas, hacer mención de reconocimientos o premios, recomendar libros  etc.; todas ellas enfocadas a temas de gestión humana. Al mismo tiempo es un espacio que permite conocer los puntos de vista de los miembros de la RED a través de entrevistas personalizadas.  Adicionalmente, en cada edición un proveedor de la REDDEGH puede hablar sobre su compañía y presentar información como: visión, misión, valores, servicios, clientes, información de contacto, etc. Por último pero no menos importante,  la Revista CONEXIONES proporciona un medio para pautar y promocionar eventos, servicios o productos.
          Si usted desea escribir un artículo, publicar eventos corporativos, o pautar y promocionar sus eventos productos y servicios, contáctenos a publicaciones@reddegh.com</p>
        </div>
      </div><!-- end block -->
      <div class="row block latest_magazine">
        <?php if ($revistas!=false) {foreach ($revistas as $mostrar) {?>
          <div class="columns three item">
            <?php echo $mostrar['ubicacion']; ?>
          </div>
        <?php }} ?>
      </div><!-- end block -->
    </section><!-- end container -->