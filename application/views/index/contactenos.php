    <section class="container row">
      <div class="row block">
        <div class="twelve columns">
         <!-- slaider-->
         <?php $this->load->view("includes/partials/slider.php"); ?>
         <!-- End slaider-->
        </div>
      </div><!-- end block -->
      <div class="row block">
        <section class="columns eight" id="content_primary">
          <div class="contact_content">
            <div class="row block">
              <div class="columns twelve">
                <h2>Contáctenos</h2>
                <p>Para nosotros es un placer atenderle y estar en contacto con usted.</p>
                <p>Al equipo de la RED de GH le genera valor conocer sus sugerencias, comentarios y necesidades. Si desea más información sobre nuestros servicios, desea pautar en la página, escribir un artículo en la Revista CONEXIONES, registrarse, o si quiere ponerse en contacto con nuestro equipo de trabajo, no dude en escribirnos a:</p>
              </div>
              <div class="clr"></div>
              <div class="contact_info">
                <table width="100%">
                  <tr>
                    <td>Dirección General: </td>
                    <td><a href="mailto:nelidaforero@reddegh.com ">nelidaforero@reddegh.com</a></td>
                  </tr>
                  <tr>
                    <td>Equipo de atención al usuario:  </td>
                    <td><a href="mailto:atencionalusuario@reddegh.com">atencionalusuario@reddegh.com</a></td>
                  </tr>
                  <tr>
                    <td>Equipo de publicaciones:  </td>
                    <td><a href="mailto:publicaciones@reddegh.com">publicaciones@reddegh.com</a></td>
                  </tr>
                  <tr>
                    <td>Equipo de registro:  </td>
                    <td><a href="mailto:registro@reddegh.com">registro@reddegh.com</a></td>
                  </tr>
                </table>
              </div>
            </div>
            <div class="row block">
                <?php echo form_open('index/contacto_mensaje','id="contact_form"'); ?>
                <div class="columns six">
                  <label for="name_contact">Nombre:</label>
                  <input type="text" name="name_contact" id="name_contact" class="required">
                  <?php echo form_error('name_contact'); ?>
                </div>
                <div class="columns six">
                  <label for="email_contact">Correo electrónico:</label>
                  <input type="text" name="email_contact" id="email_contact" class="required email">
                  <?php echo form_error('email_contact'); ?>
                </div>
                <div class="clr"></div>
                <div class="columns six">
                  <label for="subjet_contact">Asunto:</label>
                  <input type="text" id="subjet_contact" name="asunto_contact">
                  <?php echo form_error('asunto_contact'); ?>
                </div>
                <div class="clr"></div>
                <div class="columns twelve">
                  <label for="message_contact">Mensaje:</label>
                  <textarea name="message_contact"  class="required" id="message_contact" cols="30" rows="10"></textarea>
                  <?php echo form_error('message_contact'); ?>
                </div>
                <div class="clr"></div>   <br>  
                <div class="align_right">
                  <input type="submit" class="button blue" value="Enviar Mensaje">
                </div>
              <?php echo form_close(); ?>
            </div>
          </div><!-- end contact_content -->
        </section><!-- end block -->
        <aside class="columns four" id="side_bar">
          <!-- PAUTAS ACTIVACION -->
          <?php $this->load->view("includes/partials/banner_sidebar.php"); ?>  
          <!-- TERMINA PAUTAS ACTIVACION --> 
        </aside><!-- end block -->
      </div><!-- end block -->
    </section><!-- end container -->