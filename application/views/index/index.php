<section class="container row">
      <div class="row block">
        <div class="twelve columns">
         <!-- slaider-->
         <?php $this->load->view("includes/partials/slider.php"); ?>
         <!-- End slaider-->
        </div>
      </div><!-- end block -->
      <div class="row block">
        <div class="four columns">
          <div class="aboutus_box">
            <h2>Red de Gestión Humana</h2>
            <p>Somos una red social profesional de Recursos Humanos, enfocada en el desarrollo de la gestión
            estratégica de las personas en las organizaciones. Lo hacemos con el apoyo de sus miembros en la
            construcción del conocimiento conjunto frente a los retos cotidianos generados por la labor dentro y
            fuera de las organizaciones. Nos guiamos siempre por el respeto, la ética, la responsabilidad, la profunda
            pasión por lo que hacemos y el desarrollo del talento humano.
            La Red de GH está dirigida a profesionales de Gestión Humana, que impactan los diferentes campos/
            personas en la sociedad a través de su forma de actuar y herramientas como la Red de GH y sus
            beneficios.</p>
          </div>
        </div>
        <div class="four columns">
          <div class="poll_content">
            <h2>Encuesta</h2>
              <?php if ($msg1==null){ ?>
                  <?php //CODIGO PHP PARA CREAR EL FORMULARIO CON CODEIGNAITER
                   $attributes = array('class' => 'custom', 'id' => '');
                   echo form_open('index/encuentas',$attributes); ?>
                   <?php 
                   $btoregistrar = array('value'=>'Enviar','class'=>'buttons bt_continuar'); ?>
                  <div class="question">
                    <p><?php echo $preguntas[0]->pregunta ?></p>
                    <input name="idpregunta" type="hidden" id="idpregunta" value="<?php echo $preguntas[0]->id ?>"> 
                  </div>
                  <div class="answers">
                    <?php $cont=0; ?>
                    <?php foreach ($respuestas as $mostrar) { ?> 
                    <?php $cont=$cont+1;?>
                    <label for="radio<?php echo $cont?>">
                      <input name="radio" value="<?php echo$mostrar->id; ?>"type="radio" id="radio<?php echo $cont;?>" style="display:none;"><span class="custom radio"></span> <?php echo $mostrar->respuesta ?> 
  
                    </label>
                  <?php } ?>

                  </div>
                  <div class="align_center">
                    <?php echo form_submit($btoregistrar); ?>
                  </div>
                 <?php echo form_close(); ?>
                <?php } else{ ?>
                  <div class="question">
                    <p><?php echo $preguntas[0]->pregunta ?></p>
                  </div>
                  <div class="answers">
                    <?php $cont=0; ?>
                    <?php foreach ($respuestas as $mostrar) { ?> 
                        <?php $votos=$this->model_admin->votos($preguntas[0]->id,$mostrar->id); ?>
                        <?php $cont=$cont+1;?>
                        <label for="radio<?php echo $cont?>">
                          <li><input name="radio1" values="<?php echo $mostrar->id ?>" type="text" id="radio<?php echo $cont;?>" style="display:none;"><span class=""></span> <?php echo $mostrar->respuesta ?>
                            <strong>Total votos :</strong><?php echo $votos[0]->cont ?></li>      
                        </label>
                      <?php } ?>
                  </div>
               <?php }?> 
          </div>
        </div>
        <div class="four columns">
          <?php $this->load->view("includes/partials/banner_sidebar.php"); ?>
        </div>
      </div><!-- end block -->
      <!-- NOTICIAS -->
      <div class="row block news_home">
      <!-- Ciclo para cargar las noticias -->
      <?php foreach ($noticia as $mostrar) { ?>
        <div class="four columns item"> 
          <h2 cass="news_title"><?php echo $mostrar->titulo; ?></h2>         
          <div class="news_thumb">
            <img src="<?php echo base_url($mostrar->img); ?>" alt="">
          </div>          
          <div class="news_txt" style="word-wrap: break-word;">
            <p><?php echo Index::corta_palabra($mostrar->noticia,200)."...";?></p>  
          </div>
          <div class="align_right">
            <?php echo anchor('index/noticia/'.$mostrar->id.'','Ver más','class="view_more"'); ?>
          </div>
        </div>
      <?php  }?>
      <!-- fin del ciclo -->
      </div><!-- end block.noticias -->
      <div class="row block">
        <div class="four columns">
          <div class="issu_content">
            <a href="<?php echo base_url(); ?>index/revista"><img src="<?php echo base_url(); ?>images/media/issu.jpg" alt=""></a>
          </div>
        </div>
        <div class="four columns ofertas_home">
          <h2>Ofertas de Empleo</h2>
          <ul>
            <?php foreach ($ofertas as $mostrar1) { ?>
            <li>
              <p><?php echo "<strong>".$mostrar1->titulo.": </strong>".Index::corta_palabra($mostrar1->perfil,110); ?></p>
              <div class="align_right">
              <?php echo anchor('ofertas','Ver más','class="view_more"'); ?>
              <?php //echo anchor('ofertas/oferta/'.$mostrar1->id.'','Ver más','class="view_more"'); ?>
              </div>
            </li>
            <?php }?>
          </ul>
        </div>
        <div class="columns four clients_home">
          <ul>
            <?php if($miniBanner!=false){foreach ($miniBanner as $value) { ?>            
            <li><img src="<?php echo base_url($value['url_img']); ?>" alt=""></li>
            <?php }} ?>
          </ul>
        </div>
      </div><!-- end block -->
    </section><!-- end container -->