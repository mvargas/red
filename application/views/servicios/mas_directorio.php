<section class="container row">
      <div class="row block">
        <section class="columns eight" id="content_primary">
          <div class="noticias_content">
          	<div class="block post_content">

            <h2><?php echo $directorio[0]->nombre_empresa; ?></h2>
          		<div class="img_directorio">
	          		<img src="<?php echo base_url($directorio[0]->banner); ?>" alt="">
	          	</div>
	          	<?php echo $directorio[0]->descripcion; ?>
          	</div>
          </div><!-- end ofertas_content -->
          <div class="align_center submit_content">
            <?php echo anchor('servicios/directorio', 'Regresar', array( "class" => "button blue" )); ?>       
        </div>  
        </section><!-- end block -->
        <aside class="columns four" id="side_bar">
          <!-- PAUTAS ACTIVACION -->
          <?php $this->load->view("includes/partials/banner_sidebar.php"); ?>  
          <!-- TERMINA PAUTAS ACTIVACION --> 
        </aside><!-- end block -->
        
      </div><!-- end block -->
    </section>