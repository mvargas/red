<section class="container row">
      <div class="row block">
        <section class="columns twelve">
          <!--<h2>Vista Previa del Clasificado en Linea</h2>-->
          <div class="clasificados_content">
            <?php if($directorio !=FALSE){?> 
            <?php foreach ($directorio as $mostrar) { ?>

        <!--muestra listado de anuncios directorio-->
        <div class="row block item">
              <div class="columns twelve">
                <h2><?php echo $mostrar->nombre_empresa; ?></h2>
              </div>
              <div class="columns three">
                <div class="_img_noticia">
                  <a href="<?php echo "http://".$mostrar->pagina_web?>" target="_blank"><img width="150" height="150" src="<?php echo base_url($mostrar->logotipo); ?>" alt="" onClick="$mostrar->pagina_web"></a>
                </div>
              </div>
              <div class="columns nine">
                <div class="info_oferta">
                  <div class="perfil_desc">
                  <p><?php echo $mostrar->descripcion; ?></p>
                  </div>
                    <h5>Persona de contacto:</h5>
                    <div class="perfil_desc">
                      <p><?php echo $mostrar->persona_contacto; ?></p>
                    </div>
                    <h5>Teléfono:</h5>
                    <div class="perfil_desc">
                      <p><?php echo $mostrar->telefono; ?></p>
                    </div>
                    <h5>Correo Electrónico</h5>
                    <div class="perfil_desc">
                      <p><?php echo $mostrar->email; ?></p>
                    </div>
                    <h5>Ciudad / País</h5>
                    <div class="perfil_desc">
                      <p><?php echo $mostrar->pais.", ".$mostrar->ciudad; ?></p>
                    </div>
                    <h5>Página Web</h5>
                    <div class="perfil_desc">
                      <p><?php echo $mostrar->pagina_web; ?></p>
                    </div>
                    <div class="align_right">
                      <div class="align_right">
                        <?php echo anchor('servicios/directorio_banner/'.$mostrar->id.'','Ver más','class="view_more button blue"'); ?>
                      </div>
                    </div>
                </div>
              </div>
              </div><!-- Termina anuncios directorio-->
              <?php }}else{ echo '<div class="row block item elite_item">no hay resultados</div>';} ?>
          </div>
        </section><!-- end block -->
      </div><!-- end block -->
      <div> 
      </div>
    </section>