<section class="container row">
      <div class="row block">
        <section class="columns eight" id="content_primary">
          <h1>"Su Clasificado estará en linea en menos de 12 horas, si cumple con los parámetros legales permitidos"</h1>
          <h2>Vista Previa del Clasificado en Linea</h2>
          <div class="clasificados_content">
            <?php if($clasificado_pagado!=false) {?>
            <?php foreach($clasificado_pagado as $mostrar){?>
            <!-- Clasificado Grande -->
            <div class="row block item elite_item">
              <!-- Vista de Botones-->
              <div class="columns twelve" style="margin: 20px 0px 30px 0px;">
                  <?php 
                  echo anchor('servicios/aprovacion/'.$mostrar['id'].'','Aprobar',array("class"=>"success button")); 
                  echo "          ";
                  echo anchor('servicios/editar/'.$mostrar['id'].'','Editar',array("class"=>"red button")); 
                  echo "          ";
                  echo anchor('servicios/cancela/'.$mostrar['id'].'','Cancelar',array("class"=>"alert button")); 
                  ?>                             
              </div>
              <div class="columns twelve">
                <h2><?php echo $mostrar['titulo']; ?></h2>
              </div>
              <div class="columns six">
                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                         <div class="active item"><a href="#"><img src="<?php echo base_url($mostrar['ruta_img']); ?>"></a></div>
                         <?php  if($mostrar['ruta_img1']!=""){?>
                         <div class="item"><a href="#"><img src="<?php echo base_url($mostrar['ruta_img1']); ?>"></a></div>
                         <?php } ?>
                         <?php  if($mostrar['ruta_img2']!=""){?>
                         <div class="item"><a href="#"><img src="<?php echo base_url($mostrar['ruta_img2']); ?>"></a></div>
                         <?php } ?>
                         <?php  if($mostrar['ruta_img3']!=""){?>
                         <div class="item"><a href="#"><img src="<?php echo base_url($mostrar['ruta_img3']); ?>"></a></div>
                         <?php } ?>
                         <?php  if($mostrar['ruta_img4']!=""){?>
                         <div class="item"><a href="#"><img src="<?php echo base_url($mostrar['ruta_img4']); ?>"></a></div>
                         <?php } ?>
                     </div>
                   </div>   
              </div>
              <div class="columns six" style="word-wrap: break-word;">
                <p><?php echo $mostrar['descripcion']; ?></p>
                <div class="extra_info">
                  <strong>Telefóno:</strong><br>
                  <?php echo $mostrar['telefono']; ?> <br>
                  <strong>Correo Electrónico:</strong><br>
                  <?php echo $mostrar['correo']; ?><br>
                  <!-- <strong>Costo:</strong><br>
                  <?php echo $mostrar['costo']; ?><br>
                  <strong>Forma de Pago:</strong><br> 
                  <?php echo $mostrar['forma_pago']; ?><br>-->
                </div>  
              </div>
            </div>
            <!-- Termina Clasificado Grande -->
            <?php }}else{ echo '<div class="row block item elite_item">No hay Clasificados Publicados</div>';} ?>
          </div>
        </section><!-- end block -->
        <aside class="columns four" id="side_bar">
          <!-- PAUTAS ACTIVACION -->
          <?php $this->load->view("includes/partials/banner_sidebar.php"); ?> 
          <!-- TERMINA PAUTAS ACTIVACION --> 
          <div class="block widgget_search">
            <?php echo form_open('servicios/buscador','id="search_form"');?>
              <input type="text" name="bucar_solicitud" id="input_search" placeholder="Buscar">
              <input type="submit" name="search" id="search" value="Buscar">
            <?php echo form_close(); ?>
          </div>
          <!-- PAUTAS ACTIVACION -->
          <?php $this->load->view("includes/partials/banner_sidebar1.php"); ?> 
          <!-- TERMINA PAUTAS ACTIVACION --> 
        </aside><!-- end block -->
      </div><!-- end block -->
      
    </section><!-- end container -->
