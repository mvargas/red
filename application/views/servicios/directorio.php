    <section class="container row">
      <div class="row block">
        <div class="twelve columns">
         <!-- slaider-->
        
         <!-- End slaider-->
        </div>
      </div>
      <div class="row block">
        <section class="columns eight" id="content_primary">
          <h2>Servicios / Directorio</h2>
          <div class="call_to_action block">
          <table>
            <tr> 
              <td><?php echo anchor('servicios/directorio/1','Asesoria laboral','class="button blue2 fancybox"'); ?></td>
              <td><?php echo anchor('servicios/directorio/2','Busqueda, evaluación y selección de talentos','class="button blue2 fancybox"'); ?></td>   
            </tr>
            <tr>
              <td><?php echo anchor('servicios/directorio/3','Clima, Cultura, Transformación y cambio','class="button blue2 fancybox"'); ?></td>   
              <td><?php echo anchor('servicios/directorio/4','Coaching','class="button blue2 fancybox"'); ?></td>   
            </tr>
            <tr>
              <td><?php echo anchor('servicios/directorio/5','Compensación y Beneficios','class="button blue2 fancybox"'); ?></td>   
              <td><?php echo anchor('servicios/directorio/6','Competencias','class="button blue2 fancybox"'); ?></td>   
            </tr>
              <td><?php echo anchor('servicios/directorio/7','Desarrollo y Capacitación','class="button blue2 fancybox"'); ?></td>   
              <td><?php echo anchor('servicios/directorio/8','Evaluación y Desempeño','class="button blue2 fancybox"'); ?></td>   
            </tr>
            <tr>
              <td><?php echo anchor('servicios/directorio/9','Outplacement','class="button blue2 fancybox"'); ?></td>   
              <td><?php echo anchor('servicios/directorio/10','Outsourcing en RRHH','class="button blue2 fancybox"'); ?></td>   
            </tr>
            <tr>
              <td><?php echo anchor('servicios/directorio/11','Responsabilidad Social','class="button blue2 fancybox"'); ?></td>   
              <td><?php echo anchor('servicios/directorio/12','Seguridad','class="button blue2 fancybox"'); ?></td>
            </tr>
            <tr>
              <td><?php echo anchor('servicios/directorio/13','Seguridad Industrial','class="button blue2 fancybox"'); ?></td>  
              <td><?php echo anchor('servicios/directorio/14','Otros','class="button blue2 fancybox"'); ?></td>           
            </tr>
          </table>
        </div>
            <?php if($directorio!=""){
              $this->load->view("servicios/mostrario_directorio");
            }?>
        </section>

        <!--pautas y buscador -->
        <aside class="columns four" id="side_bar">
          <!-- PAUTAS ACTIVACION -->
          <?php $this->load->view("includes/partials/banner_sidebar.php"); ?> 
          <!-- TERMINA PAUTAS ACTIVACION --> 
          <div class="block widgget_search">
            <?php echo form_open('servicios/buscador_directorio','id="search_form"');?>
              <input type="text" name="buscador_directorio" id="input_search" placeholder="Buscar">
              <input type="submit" name="search" id="search" value="Buscar">
            <?php echo form_close(); ?>
          </div>
          <?php
          if($this->session->userdata('id_user_red')){ 
            if($this->session->userdata('tipo_user_red')==2 OR $this->session->userdata('tipo_user_red')==3){ 
          ?>
          <div class="call_to_action block">
            <a href="#registrar_empresa_box" class="button blue fancybox">Registrar Empresa</a>
          </div>
          <?php }} ?>
          
          <!-- PAUTAS ACTIVACION -->
          <?php $this->load->view("includes/partials/banner_sidebar1.php"); ?> 
          <!-- TERMINA PAUTAS ACTIVACION -->

        </aside><!-- end block -->

        <?php
            if($this->session->userdata('id_user_red')){ 
              if($this->session->userdata('tipo_user_red')==2 OR $this->session->userdata('tipo_user_red')==3){ 
            ?>  
        <!--Fancy Box para registrar empresa-->
        <div id="registrar_empresa_box" class="fancy_skin_redgh" style="width:650px;display: none;">
              <h2>Registrar Empresa</h2>
              <div class="row">
                <?php echo form_open_multipart('servicios/insertar_directorio','id="oferta_empleo_form"'); 
                 $descripcion = array('name'=>'descripcion','class'=>'required','id'=>'descripcion','value'=> set_value('descripcion'),'rows'=>'6','cols'=> '6','maxlength'=>'400');
                ?>
                    <div class="columns twelve">
                      <label for="nombre_empresa">Nombre Empresa</label>
                      <input type="text" id="nombre_empresa" name="nombre_empresa" class="required" value="<?php echo set_value('nombre_empresa');?>">
                      <?php echo form_error('nombre_empresa'); ?>
                      <label for="descripcion">Descripción</label>
                      <?php echo form_textarea($descripcion); ?>
                      <p class='error_img'><?php echo form_error('descripcion'); ?>
                    </div>
                    <div class="field">
                      <div class="columns six">
                        <label for="persona_contacto">Persona de contacto</label>
                        <input type="text" id="persona_contacto" name="persona_contacto" class="required" value="<?php echo set_value('persona_contacto');?>">
                        <?php echo form_error('persona_contacto'); ?>
                      </div>
                      <div class="columns six">
                        <label for="telefono">Teléfono:</label>
                        <input type="text" id="telefono" name="telefono" class="required" value="<?php echo set_value('telefono');?>">
                        <?php echo form_error('telefono'); ?>
                      </div>
                      <div class="columns six">
                        <label for="Correo">Correo:</label>
                        <input type="text" id="email" name="email" class="required" value="<?php echo set_value('email');?>">
                        <?php echo form_error('email'); ?>
                      </div>
                    </div>
                    <div class="field">
                      <div class="columns six">
                        <label for="pagina_web">Página Web:</label>
                        <input type="text" id="pagina_web" name="pagina_web" class="required" value="<?php echo set_value('pagina_web');?>">
                        <?php echo form_error('pagina_web'); ?>
                      </div>
                      <div class="columns six">
                        <label>País</label>
                        <select name="pais" class="admin required" id="">
                          <option value="">--Selecione un país--</option>
                          <?php foreach ($paises as $mostrar_paises) {
                          echo "<option  value='".$mostrar_paises->nombre."'>".$mostrar_paises->nombre."</option>";
                          } ?>         
                        </select>
                        <?php echo form_error('pais'); ?>
                      </div>
                      <div class="columns six">
                        <label for="ciudad">Ciudad:</label>
                        <input type="text" id="ciudad" name="ciudad" class="required" value="<?php echo set_value('ciudad');?>">
                        <?php echo form_error('ciudad'); ?>
                      </div>
                      
                      </div>
                      <div class="field">
                      <div class="columns six">
                        <label for="clasificacion">Clasificación</label>

                        <input type="checkbox" name="clasificacion[]" value="1">Asesoria laboral<br>
                        <input type="checkbox" name="clasificacion[]" value="2">Busqueda, evaluación y selección de talentos<br>
                        <input type="checkbox" name="clasificacion[]" value="3">Clima, Cultura, Transformación y cambio<br>
                        <input type="checkbox" name="clasificacion[]" value="4">Coaching<br>
                        <input type="checkbox" name="clasificacion[]" value="5">Compensación y Beneficios<br>
                        <input type="checkbox" name="clasificacion[]" value="6">Competencias<br>
                        <input type="checkbox" name="clasificacion[]" value="7">Desarrollo y Capacitación<br>
                        <input type="checkbox" name="clasificacion[]" value="8">Evaluación y desempeño<br>
                        <input type="checkbox" name="clasificacion[]" value="9">Outplacement<br>
                        <input type="checkbox" name="clasificacion[]" value="10">Outsourcing en RRHH<br>
                        <input type="checkbox" name="clasificacion[]" value="11">Responsabilidad Social<br>
                        <input type="checkbox" name="clasificacion[]" value="12">Seguridad<br>
                        <input type="checkbox" name="clasificacion[]" value="13">Seguridad Industrial<br>
                        <input type="checkbox" name="clasificacion[]" value="14">Otros<br>
                      </div>
                      <?php echo form_error('clasificacion');?> 
                    </div>

                    <div class="columns twelve">
                      <p>Escribe los caracteres que ves en la imagen de abajo, teniendo en cuenta las Mayúscula y Minúsculas. </p>
                    </div>
                    <div class="columns two">
                      <img src="data:image/png;base64,<?php echo $img_captcha;?>">
                    </div>
                    <div class="columns three">
                    <!--parte donde se a colocado el captcha-->
                      <input type="text" name="captcha_aplicar_oferta" class="required">
                      <?php echo form_error('captcha_aplicar_oferta'); ?>
                    </div>                                      
                    <div class="clr"></div>
                    <!--parte para seleccionar la imagen -->                       
                    <div class="field">
                      <label for=logotipo>Logotipo</label>
                      <p>Selecciona el logotipo de tu empresa preferiblemente con dimensiones cuadradas </p>
                      <!--<p>La Imagen debe Tener dimensiones de 300 x 300 </p>-->
                      <div class="columns six">
                        <input type="file" id="logotipo" name="logotipo" class="required">
                        <?php echo form_error('logotipo');?>
                      </div>
                      <div class="clr"></div>
                      <div class="clr"></div>

                      <div class="field">
                        <label for="banner">Banner</label>
                      <p>Selecciona el banner de tu empresa con un tamaño preferible de 620*400</p>
                      </div>
                      <div class="columns six">
                        <input type="file" id="banner" name="banner" class="required">
                        <?php echo form_error('banner'); ?>
                      </div>
                      </div>

                    <div class="clr"></div>
                    <div class="align_center submit_content">
                      <input type="submit" class="button gray" value="Agregar al directorio">
                    </div>
                <?php echo form_close(); ?>
              </div>
            </div>
            <?php } } ?>
    </section><!-- end container -->

