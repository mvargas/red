<section class="container row">
      <div class="row block">
        <section class="columns eight" id="content_primary">
          <div class="clasificados_content">
            <?php if($clasificado_pagado!=false) {?>
            <?php foreach($clasificado_pagado as $mostrar){?>
            <!-- Clasificado Grande -->
            <div class="row block item elite_item">
              <div class="columns twelve">
                <h2><?php echo $mostrar->titulo; ?></h2>
              </div>
              <div class="columns six">
                <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                         <div class="active item"><a href="#"><img src="<?php echo base_url($mostrar->ruta_img); ?>"></a></div>
                         <?php  if($mostrar->ruta_img1!=""){?>
                         <div class="item"><a href="#"><img src="<?php echo base_url($mostrar->ruta_img1); ?>"></a></div>
                         <?php } ?>
                         <?php  if($mostrar->ruta_img2!=""){?>
                         <div class="item"><a href="#"><img src="<?php echo base_url($mostrar->ruta_img2); ?>"></a></div>
                         <?php } ?>
                         <?php  if($mostrar->ruta_img3!=""){?>
                         <div class="item"><a href="#"><img src="<?php echo base_url($mostrar->ruta_img3); ?>"></a></div>
                         <?php } ?>
                         <?php  if($mostrar->ruta_img4!=""){?>
                         <div class="item"><a href="#"><img src="<?php echo base_url($mostrar->ruta_img4); ?>"></a></div>
                         <?php } ?>
                     </div>
                   </div>
              </div>
              <div class="columns six" style="word-wrap: break-word;">
                <p><?php echo $mostrar->descripcion; ?></p>
                <div class="extra_info">
                  <strong>Telefóno:</strong><br>
                  <?php echo $mostrar->telefono; ?> <br>
                  <strong>Correo Electrónico:</strong><br>
                  <?php echo $mostrar->correo; ?><br>
                  <!-- <strong>Costo:</strong><br>
                  <?php echo $mostrar->costo; ?><br>
                  <strong>Forma de Pago:</strong><br>
                  <?php echo $mostrar->forma_pago; ?><br> -->
                </div>  
              </div>
            </div>
            <!-- Termina Clasificado Grande -->
            <?php }}else{ echo '<div class="row block item elite_item">No hay Clasificados Publicados</div>';} ?>
            <!-- Clasificados Pequeños -->
            <div class="row clasificados_clasicos block">
              <?php if($clasificado_gratis!=false) {?>
              <?php foreach($clasificado_gratis as $mostrar1){?>
              <!-- item Clasificado-->
              <div class="columns six item block clasificado_P" style="word-wrap: break-word;height: 350px;">
                <h2><?php echo $mostrar1->titulo; ?></h2>
                  <img src="<?php echo base_url($mostrar1->ruta_img); ?>" alt="">
                <p><?php echo $mostrar1->descripcion,150;?></p>
                <div class="extra_info">
                  <strong>Correo Electrónico:</strong><br>
                  <?php echo $mostrar1->correo; ?>
                </div> 
              </div>
              <!-- end item Clasificado-->
              <?php }} ?>
            </div>
          </div><!-- Termina Clasificados Pequeños -->
        </section><!-- end block -->
        <aside class="columns four" id="side_bar">
          <!-- PAUTAS ACTIVACION -->
          <?php $this->load->view("includes/partials/banner_sidebar.php"); ?> 
          <!-- TERMINA PAUTAS ACTIVACION --> 
          <div class="block widgget_search">
            <?php echo form_open('servicios/buscador','id="search_form"');?>
              <input type="text" name="bucar_solicitud" id="input_search" placeholder="Buscar">
              <input type="submit" name="search" id="search" value="Buscar">
            <?php echo form_close(); ?>
          </div>
          <?php
          if($this->session->userdata('id_user_red')){ 
            if($this->session->userdata('tipo_user_red')==2 OR $this->session->userdata('tipo_user_red')==3){ 
          ?>
          <div class="call_to_action block">
            <a href="#crear_clasificado_box" class="button blue fancybox">Crear Clasificado Elite</a>
            <a href="#crear_clasificado_p_box" class="button blue fancybox">Crear Clasificado Estandar</a>
          </div>
          <?php }} ?>
          <!-- PAUTAS ACTIVACION -->
          <?php $this->load->view("includes/partials/banner_sidebar1.php"); ?> 
          <!-- TERMINA PAUTAS ACTIVACION --> 
        </aside><!-- end block -->
      </div><!-- end block -->
            <?php
            if($this->session->userdata('id_user_red')){ 
              if($this->session->userdata('tipo_user_red')==2 OR $this->session->userdata('tipo_user_red')==3){ 
            ?>      
            <!-- Crear Clasificado Grande--> 
            <div id="crear_clasificado_box" class="fancy_skin_redgh" style="width:650px;display: none;">
              <h2>Crear Clasificado Elite</h2>
              <div class="row">
                <?php echo form_open_multipart('servicios/insertar_clasificado','id="oferta_empleo_form"'); 
                 $perfil_clasificado = array('name'=>'perfil_clasificado','class'=>'required','id'=>'perfil_clasificado','value'=> set_value('perfil_clasificado'),'rows'=>'6','cols'=> '6','maxlength'=>'400');
                ?>
                    <div class="columns twelve">
                      <label for="titulo_clasificado">Titulo del Clasificado:</label>
                      <input type="text" id="titulo_clasificado" name="titulo_clasificado" class="required" value="<?php echo set_value('titulo_clasificado');?>">
                      <?php echo form_error('titulo_clasificado'); ?>
                      <label for="perfil_oferta">Perfil del Clasificado:</label>
                      <?php echo form_textarea($perfil_clasificado); ?>
                      <p class='error_img'><?php echo form_error('perfil_clasificado'); ?>
                    </div>
                    <div class="field">
                      <div class="columns six">
                        <label for="telefono">Telefono:</label>
                        <input type="text" id="telefono" name="telefono" class="required" value="<?php echo set_value('telefono');?>">
                        <?php echo form_error('telefono'); ?>
                      </div>
                      <div class="columns six">
                        <label for="Correo">Correo:</label>
                        <input type="text" id="correo" name="correo" class="required" value="<?php echo set_value('correo');?>">
                        <?php echo form_error('correo'); ?>
                      </div>
                    </div>
                    <div class="field">
                      <div class="columns six">
                        <label>Tiempo de duracion</label>
                        <select name="tiempo" class="admin required" id="">
                        <option value="15">2 Semanas</option>
                        <option value="30">4 Semanas</option> 
                        <option value="45">6 Semanas</option>            
                        </select>
                        <?php echo form_error('tiempo'); ?>
                      </div>
                      <div class="columns six">
                        <!-- <label for="Correo">Valor Estimado</label>
                        <div class="valor">$10000</div> -->
                      </div>
                    </div>
                    <!-- <div class="field">
                      <div class="columns six">
                        <label for="costo">Costo:</label>
                        <input type="text" id="costo" name="costo" class="required" value="<?php echo set_value('costo');?>">
                        <?php //echo form_error('costo'); ?>
                      </div>
                      <div class="columns six">
                        <label for="forma_pago">Forma de Pago:</label>                        
                        <input type="text" id="forma_pago" name="forma_pago" class="required" value="<?php echo set_value('forma_pago');?>">
                        <?php //echo form_error('forma_pago'); ?>
                      </div>
                    </div> -->                    
                    <!--parte donde se a colocado el captcha -->
                    <div class="columns twelve">
                      <p>Escribe los caracteres que ves en la imagen de abajo, tediendo en cuenta las Mayúscula y Minúsculas. </p>
                    </div>
                    <div class="columns two">
                      <img src="data:image/png;base64,<?php echo $img_captcha;?>">
                    </div>
                    <div class="columns three">
                    <!--parte donde se a colocado el captcha -->
                      <input type="text" name="captcha_aplicar_oferta" class="required">
                      <?php echo form_error('captcha_aplicar_oferta'); ?>
                    </div>                                         
                    <div class="clr"></div>
                    <!--parte para seleccionar la imagen -->                       
                    <div class="field">
                      <p>La Imagen debe Tener dimensiones de 300 x 300 </p>
                      <div class="columns six">
                        <input type="file" id="adjuntar_oferta" name="img_oferta" class="required">
                        <?php echo form_error('img_oferta'); ?>
                      </div>
                      <div class="columns six">
                        <input type="file" id="adjuntar_oferta1" name="img_oferta1" class="">
                        <?php echo form_error('img_oferta1'); ?>
                      </div>
                    </div>
                    <div class="field">
                      <div class="columns six">
                        <input type="file" id="adjuntar_oferta" name="img_oferta2" class="">
                        <?php echo form_error('img_oferta2'); ?>
                      </div>
                      <div class="columns six">
                        <input type="file" id="adjuntar_oferta1" name="img_oferta3" class="">
                        <?php echo form_error('img_oferta3'); ?>
                      </div>
                    </div>
                    <div class="field">
                      <div class="columns six">
                        <input type="file" id="adjuntar_oferta" name="img_oferta4" class="">
                        <?php echo form_error('img_oferta4'); ?>
                      </div>
                    </div>
                    <div class="clr"></div>
                    <div class="align_center submit_content">
                      <input type="submit" class="button gray" value="Crear Clasificado">
                    </div>
                <?php echo form_close(); ?>
              </div>
            </div>
            <!-- Crear Clasificado Pequeño--> 
            <div id="crear_clasificado_p_box" class="fancy_skin_redgh" style="width:650px;display: none;">
              <h2>Crear Clasificado Estandar </h2>
              <div class="row">
                <?php echo form_open_multipart('servicios/insertar_pequeno','id="oferta_empleo_form"'); 
                 $perfil_clasificado = array('name'=>'perfil_clasificado','class'=>'required','id'=>'perfil_clasificado','value'=> set_value('perfil_clasificado'),'rows'=>'6','cols'=> '6','maxlength'=>'100');
                ?>
                    <div class="columns twelve">
                      <label for="titulo_clasificado">Titulo del Clasificado:</label>
                      <input type="text" id="titulo_clasificado" name="titulo_clasificado" class="required" value="<?php echo set_value('titulo_clasificado');?>">
                      <?php echo form_error('titulo_clasificado'); ?>
                      <label for="perfil_oferta">Perfil del Clasificado:</label>
                      <?php echo form_textarea($perfil_clasificado); ?>
                      <p class='error_img'><?php echo form_error('perfil_clasificado'); ?>
                    </div>
                    <div class="field">
                      <div class="columns six">
                        <label for="Correo">Correo:</label>
                        <input type="text" id="correo" name="correo" class="required" value="<?php echo set_value('correo');?>">
                        <?php echo form_error('correo'); ?>
                      </div>
                    </div>
                    <!--parte donde se a colocado el captcha -->
                    <div class="columns twelve">
                      <p>Escribe los caracteres que ves en la imagen de abajo, tediendo en cuenta las Mayúscula y Minúsculas. </p>
                    </div>
                    <div class="columns two">
                      <img src="data:image/png;base64,<?php echo $img_captcha;?>">
                    </div>
                    <div class="columns three">
                      <!--parte donde se a colocado el captcha -->
                      <input type="text" name="captcha_aplicar_oferta" class="required">
                      <?php echo form_error('captcha_aplicar_oferta'); ?>
                    </div>                               
                    <!--parte para seleccionar la imagen -->                    
                    <div class="columns twelve">
                      <p>La Imagen debe Tener Dimensiones de 200 x 170
                      </p>
                      <input type="file" id="adjuntar_oferta" name="img_oferta" class="">
                      <p class='error_img'></p><?php echo form_error('img_oferta'); ?>
                    </div>                                      
                    <div class="clr"></div>
                    <div class="align_center submit_content">
                      <input type="submit" class="button gray" value="Crear Clasificado">
                    </div>
                    <div>

                      
                    <div>
                <?php echo form_close(); ?>
              </div>
            </div>
            <!-- Termina Crear Clasificado -->
            <?php }} ?>
          -->
    </section><!-- end container -->