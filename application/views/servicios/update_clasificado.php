<section class="container row">
      <div class="row block">
        <h2>Actualizar Clasificado</h2>
        <div class="columns twelve">
                 <div class="columns two"><img src="<?php echo base_url($clasificado[0]['ruta_img']); ?>"></div>
                 <?php  if($clasificado[0]['ruta_img1']!=""){?>
                 <div class="columns two"><img src="<?php echo base_url($clasificado[0]['ruta_img1']); ?>"></div>
                 <?php } ?>
                 <?php  if($clasificado[0]['ruta_img2']!=""){?>
                 <div class="columns two"><img src="<?php echo base_url($clasificado[0]['ruta_img2']); ?>"></div>
                 <?php } ?>
                 <?php  if($clasificado[0]['ruta_img3']!=""){?>
                 <div class="columns two"><img src="<?php echo base_url($clasificado[0]['ruta_img3']); ?>"></div>
                 <?php } ?>
                 <?php  if($clasificado[0]['ruta_img4']!=""){?>
                 <div class="columns two"><img src="<?php echo base_url($clasificado[0]['ruta_img4']); ?>"></div>
                 <?php } ?>
        </div>
        <div class="columns twelve">
            <section class="eight columns" id="content_primary">
             <?php foreach ($clasificado as $mostrar) {?>
                <!-- Crear Clasificado --> 
                <div id="crear_clasificado_box" class="">
                  <div class="row">
                    <?php echo form_open_multipart('servicios/update_clasificado','id="oferta_empleo_form"'); 
                     $perfil_clasificado = array('name'=>'perfil_clasificado','class'=>'required','id'=>'perfil_clasificado','value'=>$mostrar['descripcion'],'rows'=>'6','cols'=> '6','maxlength'=>'400');
                    ?> 
                          <input type="hidden" name="ruta_img" value="<?php echo $mostrar['ruta_img'];?>">
                          <input type="hidden" name="ruta_img1" value="<?php echo $mostrar['ruta_img1'];?>">
                          <input type="hidden" name="ruta_img2" value="<?php echo $mostrar['ruta_img2'];?>">
                          <input type="hidden" name="ruta_img3" value="<?php echo $mostrar['ruta_img3'];?>">
                          <input type="hidden" name="ruta_img4" value="<?php echo $mostrar['ruta_img4'];?>">
                          <div class="field">
                            <div class="columns six">
                              <label for="titulo_clasificado">Titulo del Clasificado:</label>
                              <input type="text" id="titulo_clasificado" name="titulo_clasificado" class="required" value="<?php echo $mostrar['titulo'];?>">
                            </div>                       
                          </div>
                        <div class="columns twelve">            
                          <input type="hidden" id="id_clasificado" name="id_clasificado" class="required" value="<?php echo $mostrar['id'];?>">
                          <?php echo form_error('titulo_clasificado'); ?>
                          <label for="perfil_oferta">Perfil del Clasificado:</label>
                          <?php echo form_textarea($perfil_clasificado); ?>
                          <p class='error_img'><?php echo form_error('perfil_clasificado'); ?>
                        </div>
                        <div class="field">
                          <div class="columns six">
                            <label for="telefono">Telefono:</label>
                            <input type="text" id="telefono" name="telefono" class="required" value="<?php echo $mostrar['telefono'];?>">
                            <?php echo form_error('telefono'); ?>
                          </div>
                          <div class="columns six">
                            <label for="Correo">Correo:</label>
                            <input type="text" id="correo" name="correo" class="required" value="<?php echo $mostrar['correo'];?>">
                            <?php echo form_error('correo'); ?>
                          </div>
                        </div>
                        <!--  <div class="field">
                            <div class="columns six">
                              <label for="costo">Costo:</label>
                              <input type="text" id="costo" name="costo" class="required" value="<?php echo $mostrar['costo'];?>">
                              <?php echo form_error('costo'); ?>
                            </div>
                            <div class="columns six">
                              <label for="forma_pago">Forma de Pago:</label>                        
                              <input type="text" id="forma_pago" name="forma_pago" class="required" value="<?php echo $mostrar['forma_pago'];?>">
                              <?php echo form_error('forma_pago'); ?>
                            </div>
                          </div> -->
                          <div class="field">
                            <div class="columns six">
                              <label>Duración del Clasificado</label>
                                 <?php if($mostrar['tiempo']==30){?>
                                  <select name="tiempo" class="admin required" id="tiempo">                         
                                  <option value="30">4 Semanas</option> 
                                  <option value="15">2 Semanas</option>
                                  <option value="45">6 Semanas</option>
                                  </select>
                                <?php }elseif ($mostrar['tiempo']==45){ ?>
                                  <select name="tiempo" class="admin required" id="tiempo">
                                  <option value="45">6 Semanas</option>                        
                                  <option value="30">4 Semanas</option> 
                                  <option value="15">2 Semanas</option>
                                  </select>                        
                                <?php }else{?>
                                  <select name="tiempo" class="admin required" id="tiempo">                         
                                  <option value="15">2 Semanas</option>
                                  <option value="30">4 Semanas</option> 
                                  <option value="45">6 Semanas</option> 
                                  </select>
                                <?php } ?> 
                            </div>                            
                            <div class="columns six">
                              <label for="Correo">Valor Estimado</label>
                              <div class="valor">$10000</div>
                            </div>
                          </div>
                        </div>  
                        </div>                  
                        <!--parte donde se a colocado el captcha -->
                        <div class="columns twelve">
                            <p><strong style="color:red;">Nota: <br> -Seleccione una imagen si desea cambiar la predeterminada, de lo contrario no utilice esta función</strong></p>
                        </div>         
                        <div class="field">                   
                            <div class="columns six ">
                              <label for="Correo">Imagen 1</label>
                              <input type="file" id="adjuntar_oferta" name="img_oferta" class="">
                              <p class='error_img'></p><?php echo form_error('img_oferta'); ?>
                            </div>
                            <div class="columns six ">
                              <label for="Correo">Imagen 2</label>
                             <input type="file" id="adjuntar_oferta" name="img_oferta1" class="">
                              <p class='error_img'></p><?php echo form_error('img_oferta1'); ?>
                            </div>
                        </div> 
                        <div class="field">                   
                            <div class="columns six ">
                              <label for="Correo">Imagen 3</label>
                              <input type="file" id="adjuntar_oferta" name="img_oferta2" class="">
                              <p class='error_img'></p><?php echo form_error('img_oferta2'); ?>
                            </div>
                            <div class="columns six ">
                              <label for="Correo">Imagen 4</label>
                             <input type="file" id="adjuntar_oferta" name="img_oferta3" class="">
                              <p class='error_img'></p><?php echo form_error('img_oferta3'); ?>
                            </div>
                        </div>
                        <div class="columns six ">
                              <label for="Correo">Imagen 5</label>
                              <input type="file" id="adjuntar_oferta" name="img_oferta4" class="">
                              <p class='error_img'></p><?php echo form_error('img_oferta4'); ?>
                        </div>   
                        <div class="clr"></div>
                        <div class="align_center submit_content">
                          <?php echo anchor('servicios/mostrario/'.$mostrar['id'],'Regresar',array("class"=>"button blue"));?>
                          <input type="submit" class="button gray" value="Actualizar Clasificado">
                        </div>
                    <?php echo form_close(); }?>
            </section><!-- end block -->
        </div>
      </div><!-- end block -->
    </section><!-- end container -->