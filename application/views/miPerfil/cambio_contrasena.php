<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Cambio de Contraseña</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/perfil_nav.php"); ?>
    </div>
    <div class="columns nine">
      <?php echo form_open('miPerfil/actualizacion_contrasena','id="register_form" class="custom"');?>
      <label>Contraseña Actual</label>
      <input type="password" name="pass_actual">
      <?php echo form_error('pass_actual'); ?>
      <label>Contraseña Nueva</label>
      <input type="password" name="pass_nueva">
      <?php echo form_error('pass_nueva'); ?>
      <label>Repite Contraseña</label>
      <input type="password" name="pass_repite">
      <?php echo form_error('pass_repite'); ?>
      <input type="submit" value="enviar" name="enviar" class="success button">
      <?php echo form_close(); ?>
    </div>
  </div><!-- end block -->
</section><!-- end container -->