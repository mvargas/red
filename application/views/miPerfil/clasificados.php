<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Clasificados </h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/perfil_nav.php"); ?>
    </div>
    <div class="columns twelve">
      <?php echo $msg1; ?>
      <label>Clasificados Grandes</label>                     
      <table width="100%" id="users_table">
        <thead>
        <tr>
          <td>Titulo</td>
          <td>Descripcion</td>
          <td>Imagen</td>
          <td>Estado</td>
          <td>Costo</td>
          <td>Forma de Pago</td>
          <td>Fecha</td>
          <td>Duracion</td>
          <td>Estado</td>
        </tr>
        </thead>
        <?php if($clasificado_pagado!=false){
              foreach ($clasificado_pagado as $mostrar) { ?>
        <tr>
          <td><?php echo $mostrar->titulo; ?></td>
          <td><?php echo $mostrar->descripcion; ?></td>
          <td><img width="100" height="100" src="<?php echo base_url($mostrar->ruta_img);  ?>"></td>
          <td><?php if($mostrar->estado_clas==1){ echo "Activo";}else{echo "Desactivo";} ?></td>
          <td><?php echo $mostrar->costo; ?></td>
          <td><?php echo $mostrar->forma_pago; ?></td>
          <td><?php echo $mostrar->fecha_publicado; ?></td>
          <td><?php echo $mostrar->tiempo." Dias"; ?></td>
          <td><?php if($mostrar->estado_clas==0){echo "Desactivo"; }else{ echo "Activo"; } ?></td>
        </tr>
      <?php }
          }else{ echo "<tr><td>No hay ningun registro</td></tr>";}?>
     </table>
    </div> 
    <div class="columns twelve">
      <label>Clasificados Pequeños</label>                           
      <table width="100%" id="users_table">
        <thead>
        <tr>
          <td>Titulo</td>
          <td>Descripcion</td>
          <td>Imagen</td>
          <td>Estado</td>
          <td>Costo</td>
          <td>Forma de Pago</td>
          <td>Fecha</td>
          <td>Estado</td>
        </tr>
        </thead>
        <?php if($clasificado_gratis!=false){
              foreach ($clasificado_gratis as $mostrar) { ?>
        <tr>
          <td><?php echo $mostrar->titulo; ?></td>
          <td><?php echo $mostrar->descripcion; ?></td>
          <td><img width="100" height="100" src="<?php echo base_url($mostrar->ruta_img);  ?>"></td>
          <td><?php if($mostrar->estado_clas==1){ echo "Activo";}else{echo "Desactivo";} ?></td>
          <td><?php echo $mostrar->costo; ?></td>
          <td><?php echo $mostrar->forma_pago; ?></td>
          <td><?php echo $mostrar->fecha_publicado; ?></td>
          <td><?php if($mostrar->estado_clas==0){echo "Desactivo"; }else{ echo "Activo"; } ?></td>        </tr>
      <?php }
          }else{ echo "<tr><td>No hay ningun registro</td></tr>";}?>
     </table>
    </div>          
    </div>
  </div><!-- end block -->
</section><!-- end container -->