<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Usuarios</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/perfil_nav.php"); ?>
    </div>
    <div class="columns nine">
      <h2>¿Esta seguro que Eliminar su Cuenta?</h2>
      <?php echo anchor('miPerfil/eliminar','Si',array("class"=>"success button")); ?>
      <?php echo anchor('miPerfil/index','No',array("class"=>"alert button")); ?>
    </div>
  </div><!-- end block -->
</section><!-- end container -->