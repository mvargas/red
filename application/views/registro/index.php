
<section class="container row">
      <div class="row block">
        <div class="twelve columns">
         <!-- slaider-->
         <?php $this->load->view("includes/partials/slider.php"); ?>
         <!-- End slaider-->
        </div>
      </div><!-- end block -->
      <!-- register_form -->
      <?php echo form_open('registro/insertar','id="register_form" class=""');?>
        <div class="row block">
          <div class="twelve columns">
            <h2>Registro / Datos Personales</h2>
            <p>Gracias por creer en el apoyo, la colaboración entre las personas y los excelentes resultados que se generan.</p>
            <p>A continuación deberá escribir su información para la realización del REGISTRO. Una vez  envíe su información, usted estará registrado.  Su usuario será el email que usted registre. En la parte superior derecha de la página aparecerá: Bienvenido (Su nombre).  Lo cual indica que usted se encuentra dentro del sistema.</p>
            <p>Recuerde salir de su cuenta una vez termine su tiempo en ella.</p>
          </div>
        </div><!-- end block -->
        <div class="row block">
          <div class="field">
            <div class="four columns">
              <label for="name_user">Nombres:</label>
              <input type="text" id="name_user" name="nombre" class="required" value="<?php echo set_value('nombre');?>">
              <?php echo form_error('nombre'); ?>
            </div>
            <div class="four columns">
              <label for="lastname_user">Apellidos:</label>
              <input type="text" id="lastname_user" name="apellido" class="required" value="<?php echo set_value('apellido');?>">
              <?php echo form_error('apellido'); ?>
            </div>
            <div class="four columns">
              <label for="cedula_user">Cédula:</label>
              <input type="text" id="cedula_user" name="cedula" class="required" value="<?php echo set_value('cedula');?>">
              <?php echo form_error('cedula'); ?>
            </div>
          </div>
          <div class="field">
            <div class="four columns">
              <label for="email_user">E-mail:</label>
              <input type="text" id="email_user" name="correo" class="required email" value="<?php echo set_value('correo');?>">
              <?php echo form_error('correo'); ?>
            </div>
            <div class="four columns">
              <label for="phone_user">Teléfono:</label>
              <input type="text" id="phone_user" name="telefono" class="required" value="<?php echo set_value('telefono');?>">
              <?php echo form_error('telefono'); ?>
            </div>
            <div class="four columns">
              <label for="country_user">País de residencia:</label>
              <select name="pais" id="country_user" class="required" onChange= ver()>
                <option value="">-- Seleccionar un Páis --</option>
                <?php foreach ($paises as $mostrar_paises) {
                echo "<option  value='".$mostrar_paises->nombre."'>".$mostrar_paises->nombre."</option>";
                } ?>
              </select>
              <?php echo form_error('pais'); ?>
            </div>
          </div>
          <!-- div colombia-->
          <div class="four columns last" style="display:none" id="C2">
            <label for="city_user">Ciudad de residencia:</label>
            <select name="ciudad" id="city_user" class="required" style="width: 100% !important;">
              <option value="">-- Seleccionar un Ciudad --</option>
              <option value="Barranquilla">Barranquilla</option>
              <option value="Bogota">Bogotá</option>
              <option value="Bucaramanga">Bucaramanga</option>
              <option value="Cali">Cali</option>
              <option value="Cartagena">Cartagena</option>
              <option value="Medellin">Medellín</option>
              <option value="Otra Ciudad">Otra Ciudad</option>
            </select>
            <?php echo form_error('ciudad'); ?>
          </div>
          <!-- fin div colombia-->
        </div><!-- end block -->
        <div class="row block">
          <div class="twelve columns">
            <h2>Registro / Datos Profesionales</h2>
            <p>Le sugerimos diligencias todos los campos profesionales, para mantenerle activo en las búsquedas de los reclutadores.</p>
            <p>Si usted se encuentra sin empleo, por favor indique el sector en el que mayor experiencia tiene y en cargo escriba: En búsqueda.</p>
          </div>
        </div><!-- end block -->
        <div class="row block">
          <div class="four columns">
            <label for="profession_user">Profesión:</label>
            <input type="text" id="profession_user" name="profesion" class="required" value="<?php echo set_value('profesion');?>">
            <?php echo form_error('profesion'); ?>
          </div>
          <div class="four columns">
            <label for="company_user">Empresa dónde trabaja:</label>
            <input type="text" id="company_user" name="empresa" class="required">
          </div>
          <div class="four columns ">
            <label for="role_user">Cargo:</label>
            <input type="text" id="role_user" name="cargo" class="required">
          </div>
        </div><!-- end block -->
        <div class="row block">
          <div class="four columns">
          <label for="sector_user">Sector:</label>
          <select name="sector" id="sector_user" class="required">
            <option value="">-- Seleccionar un Sector --</option>
            <?php foreach ($sectores as $mostrar_sector) {
              echo "<option value='".$mostrar_sector->nombre."'>".$mostrar_sector->nombre."</option>";
            } ?>
          </select>
          <?php echo form_error('sector'); ?>
        </div> 
        </div>
        
        <div class="row block">
          <div class="twelve columns">
            <h2>Registro / Datos Ingreso a la Red</h2>
            <p>Es muy importante que su clave sea segura. Por lo tanto, le sugerimos crear una que incluya letras, números y signos. La clave debe ser confidencial y de manejo personal.</p>
          </div>
        </div><!-- end block -->
        <div class="row block">
          <div class="four columns">
            <label for="pass_user">Contraseña:</label>
            <input type="password" id="pass_user" name="contrasena" class="required">
            <?php echo form_error('contrasena'); ?>
          </div>
          <div class="four columns">
            <label for="repeatpass_user">Repetir Contraseña:</label>
            <input type="password" id="repeatpass_user" name="repit_contrasena" class="required">
            <?php echo form_error('repit_contrasena'); ?>
          </div>
        </div><!-- end block -->
        <div class="row block">
          <input type="checkbox" class="required">
          Yo declaro que leí, entendí, y acepto las <a href="<?php echo base_url(); ?>/inc/pdf/Condiciones de Uso y Servicio de la RED de GH.pdf" target="_blank">Políticas de Privacidad de la Red de GH.</a>
        </div>
        <input type="submit" class="button" value="Registrarse" class="required"><br><br>
        <p>Si tiene dudas/inquietudes/sugerencias por favor contáctenos a <a href="mailto:registro@reddegh.com" target="_blank">registro@reddegh.com</a>  para apoyarle.</p>
        <?php echo form_close(); ?><!-- end register_form -->
    </section><!-- end container -->

  