    <section class="container row">
      <div class="row block">
        <div class="twelve columns">
         <!-- slaider-->
         <?php $this->load->view("includes/partials/slider.php"); ?>
         <!-- End slaider-->
        </div>
      </div><!-- end block -->
      <div class="row block">
        <section class="columns eight" id="content_primary">
          <div class="ayudemonos_content">
            <div class="row block item">
              <div class="columns twelve">
                <h2>Apoyándonos</h2>
                <p>Creemos que el aporte en conjunto y dinámico crea sinergia y desarrollo. Como RED de GH podemos aportar a otros miembros a través de nuestros conocimientos, ideas, información, etc. 
                   APOYANDONOS es uno de los espacios EXCLUSIVOS de la RED de GH. 
                   A través de Apoyándonos se puede solicitar información sobre temas de Gestión Humana y responder a las mismas. La importancia de esta sección es que se responde on-line. Cada solicitud y respuesta están visibles para ustedes.</p>
              </div>
            </div><!-- end item -->
            <!-- PARTE DE LA SOLICITUD-->
            <?php if ($solicitudes!=false) { 
            foreach ($solicitudes as $mostrar){ ?>           
              <div class="row block item">
                <div class="columns twelve">
                  <h2><?php echo $mostrar->titulo ?></h2>
                  <p><?php echo $mostrar->razon_solicitud ?></p>
                </div>
                <div class="align_right">
                  <div class="replies_content">
                    <div class="replies_inner">
                      <!-- respuestas solicitud -->
                      <?php 
                       $this->load->model('model_apoyemonos');
                       $datos= $this->model_apoyemonos->mostrar_respuesta_solicitud($mostrar->id);
                       if ($datos!=false) {
                       foreach ($datos as $mostrar1) {
                      ?>
                      <div class="block item gray">
                        <h3><span class="name"><?php echo $mostrar1->nombre." ".$mostrar1->apellido;  ?></span> Responde <span class="date"><?php echo Apoyemonos::formato_fecha($mostrar1->fecha_respuesta); ?></span></h3>
                        <p><?php echo $mostrar1->respuesta_solicitudd; ?></p>
                      </div>
                      <?php }}else{ echo "No hay ninguna Respuesta a la Solicitud";} ?>
                      <!-- termina respuestas solicitud -->
                    </div>
                  </div>
                  <a href="#" class="link_toogle_replies button blue">Ver Respuestas</a>
                  <a href="javascript:void(0)" class="view_more button blue" onclick="responde_solicitud(<?php echo $mostrar->id; ?>)">Responder solicitud</a>
                </div>
              </div>
            <?php }}else{echo "No hay ninguna solicitud";} ?>
            <!-- TERMINA PARTE DE LA SOLICITUD-->
          </div><!-- end ofertas_content -->
        </section><!-- end block -->
        <aside class="columns four" id="side_bar">
          <!-- PAUTAS ACTIVACION -->
          <?php $this->load->view("includes/partials/banner_sidebar.php"); ?> 
          <!-- TERMINA PAUTAS ACTIVACION -->
          <div class="call_to_action block">
             <a href="#crear_solicitud_box" class="button blue fancybox">Crear solicitud</a>
            <div id="crear_solicitud_box" class="fancy_skin_redgh" style="width:650px;display: none;">
              <h2>Crear Solicitud</h2>
              <div class="row">
                <?php echo form_open('apoyemonos/crear_solicitud','id="crear_solicitud_form"'); 
                 $solicitud = array('name'=>'solicitud','class'=>'required','id'=>'solicitud','value'=> set_value('solicitud'),'cols'=> '30','cols'=> '10');
                ?>
                    <div class="columns twelve">
                      <label for="razon_oferta">Titulo Solicitud</label>
                      <input name="titulo" class="required" type="text" id="titulo" value="<?php echo set_value('titulo');?>">
                      <?php echo form_error('titulo'); ?>
                      <?php echo form_textarea($solicitud); ?>
                      <p class='error_img'><?php echo form_error('solicitud'); ?>
                    </div>
                    <div class="clr"></div>
                    <div class="columns twelve">
                      <p>Escribe los caracteres que ves en la imágen de abajo. </p>
                    </div>
                    <div class="columns two">
                      <img src="data:image/png;base64,<?php echo $img_captcha;?>">
                    </div>
                    <div class="columns three">
                      <input type="text" class="required" name="captcha_crear_solicitud">
                      <?php echo form_error('captcha_crear_solicitud'); ?>
                    </div>
                    <div class="clr"></div>
                    <div class="align_center submit_content">
                      <input type="submit" class="button gray" value="Aplicar">
                    </div>
               <?php echo form_close(); ?>
              </div>
            </div>
          </div> 
        </aside><!-- end block -->
            <div id="responder_solicitud_box" class="fancy_skin_redgh" style="width:650px;display: none;">
              <h2>Responder Solicitud</h2>
              <div class="row">
                <?php echo form_open('apoyemonos/responder_solicitud','id="aplicar_form"'); 
                      $responder_solicitud = array('name'=>'respoder_solicitud','class'=>'required','id'=>'respoder_solicitud','value'=> set_value('respoder_solicitud'),'cols'=> '30','cols'=> '10');
                ?>
                    <div class="columns twelve">
                      <label for="razon_oferta">Respuesta Solicitud</label>
                      <?php echo form_textarea($responder_solicitud); ?>
                      <p class='error_img'><?php echo form_error('respoder_solicitud'); ?>
                      <input name="id_solicitud" type="hidden" id="id_solicitud" value="<?php echo set_value('id_solicitud');?>">
                    </div>
                    <div class="clr"></div>
                    <div class="columns twelve">
                      <p>Escribe los caracteres que ves en la imágen de abajo. </p>
                    </div>
                    <div class="columns two">
                      <img src="data:image/png;base64,<?php echo $img_captcha;?>">
                    </div>
                    <div class="columns three">
                      <input type="text" class="required" name="captcha_responder_solicitud">
                      <?php echo form_error('captcha_responder_solicitud'); ?>
                    </div>
                    <div class="clr"></div>
                    <div class="align_center submit_content">
                      <input type="submit" class="button gray" value="Aplicar">
                    </div>
               <?php echo form_close(); ?>
              </div>
            </div>
      </div><!-- end block -->
    </section><!-- end container -->