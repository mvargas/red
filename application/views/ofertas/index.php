    <section class="container row">
      <div class="row block">
        <div class="twelve columns">
         <!-- slaider-->
         <?php $this->load->view("includes/partials/slider.php"); ?>
         <!-- End slaider-->
        </div>
      </div><!-- end block -->

      <div class="row block">
        <section class="columns eight" id="content_primary">
          <div class="ofertas_content">
           <?php if($ofertas !=FALSE){?> 
            <?php foreach ($ofertas as $mostrar) { ?>             
            <div class="row block item">
              <div class="columns twelve">
                <h2><?php echo $mostrar->titulo;?></h2>
              </div>
              <div class="columns three">
                <div class="img_oferta">
                  <img width="150" height="150 "src="<?php echo base_url($mostrar->logo); ?>" alt="">
                </div>
                <div class="call_to_action block">
                  <?php if($this->session->userdata('id_user_red')){ ?>
                  <!--<a href="#aplicar_oferta_box" class="button blue small fancybox">Aplicar a esta oferta</a>-->
                  <a href="javascript:void(0)" class="button blue small" onclick="aplicar_oferta(<?php echo $mostrar->id ?>)">Aplicar a esta oferta</a>
                  <?php }else{ ?>
                  <a href="javascript:void(0)" class="button blue small" onclick="login_oferta(<?php echo $mostrar->id ?>)">Aplicar a esta oferta</a>
                  <!--<a href="#login_form_box" class="button blue small fancybox">Aplicar a esta oferta</a>-->                  
                  <?php } ?>
                </div>
              </div>
              <div class="columns nine">
                <div class="info_oferta">
                  <h5>Perfil:</h5>
                  <div class="perfil_desc">
                    <p><?php echo $mostrar->perfil; ?></p>
                  </div>
                  <h5>Clase de contrato:</h5>
                  <div class="perfil_desc">
                    <p><?php echo $mostrar->tipo_contrato; ?></p> 
                  </div>
                  <h5>Salario:</h5>
                  <div class="perfil_desc">
                    <p><?php echo $mostrar->salario; ?></p>
                  </div>
                  <h5>Ubicación:</h5>
                  <div class="perfil_desc">
                    <p><?php echo $mostrar->ciudad.", ".$mostrar->pais; ?></p>
                  </div>
                  <h5>Fecha de Publicación:</h5>
                  <div class="perfil_desc">
                    <p><?php echo $mostrar->fecha ?></p>
                  </div>                  
                </div>
              </div>
            </div><!-- end item -->
            <?php } ?>
            <?php }  else {
                 echo 'No hay ofertas';
            }?>
            <?php if ($this->session->userdata('id_user_red')){ ?>            
            <div id="aplicar_oferta_box" class="fancy_skin_redgh" style="width:650px;display: none;">
              <h2>Aplicar a esta oferta</h2>
              <div class="row">
                <?php echo form_open_multipart('ofertas/aplicar_oferta','id="aplicar_form"'); 
                   $razon_oferta = array('name'=>'razon_oferta','class'=>'required','id'=>'razon_oferta','value'=> set_value('razon_oferta'),'cols'=> '30','cols'=> '10');
                ?>
                    <div class="columns twelve">
                      <label for="razon_oferta">Razones por las cuales cree que usted cumple con el perfil de la oferta:</label>
                      <?php echo form_textarea($razon_oferta); ?>
                      <p class='error_img'><?php echo form_error('razon_oferta'); ?>
                      <input name="id_oferta" type="hidden" id="idd_oferta" value="<?php echo set_value('id_oferta');?>">
                    </div>
                    <div class="clr"></div>
                    <div class="columns twelve">
                      <p>Escribe los caracteres que ves en la imágen de abajo. </p>
                    </div>
                    <div class="columns two">
                      <img src="data:image/png;base64,<?php echo $img_captcha;?>">
                    </div>
                    <div class="columns three">
                      <input type="text" name="captcha_aplicar_oferta">
                      <?php echo form_error('captcha_aplicar_oferta'); ?>
                    </div>
                    <div class="columns twelve">
                      <label for="adjuntar_oferta">Adjuntar Hoja de Vida:</label>
                      <input type="file" id="adjuntar_oferta" name="hoja_oferta" >
                      <p class='error_img'><?php echo form_error('hoja_oferta'); ?>
                      <p>Anexar su HV en formato Word y pdf únicamente. </p>
                    </div>
                    <div class="clr"></div>
                    <div class="align_center submit_content">
                      <input type="submit" class="button gray" value="Aplicar">
                    </div>
               <?php echo form_close(); ?>
              </div>
            </div>
            <?php } ?>
            <div id="login_form_box" class="fancy_skin_redgh" style="width:650px;display: none;">
              <h2>Aplicar a esta oferta</h2>
              <p class="align_center">Para aplicar a esta oferta debes haber iniciado sesión. Si no estas registrado haz <a href="">clic aquí</a>.  Recuerda que sólo los usuarios registrados pueden aplicar a las ofertas.</p>
              <div class="row">
                <form action="#" id="login_aplicar_form">
                    <div class="columns six">
                      <label for="email_user">Email:</label>
                      <input type="text" id="correo_oferta" class="required" name="email_user" />
                      <input type="hidden" id="id_oferta_login" name="id_oferta">
                    </div>
                    <div class="columns six">
                      <label for="pass_user">Contraseña:</label>
                      <input type="password" id="correo_pass" class="required" name="pass_user">
                    </div>
                    <div class="clr"></div>
                    <label class="error_oferta"></label>
                    <div class="align_center submit_content">
                      <?php echo anchor('registro','Registro','class="button gray"'); ?>
                      <a href="#" class="button gray">Recordar contraseña</a>
                      <input type="button" id="login_oferta" class="button gray" value="Aplicar a esta oferta">
                    </div>
                </form>
              </div>
            </div>
          </div><!-- end ofertas_content -->
        </section><!-- end block -->
        <aside class="columns four" id="side_bar">
          <div class="block widgget_search">
            <?php $attributes = array('class' => '', 'id' => 'search_form');
            echo form_open('ofertas/buscador',$attributes);
              $palabra=array('name'=>'palabra','id'=>'input_search','type'=>'text','placeholder'=>'Buscar');
              $btobuscar = array('value'=>'','id'=>'search','class'=>'search');?>
              <?php 
                echo form_input($palabra);
                echo form_error('palabra');
              ?>
              <?php echo form_submit($btobuscar); ?>
               <?php echo form_close(); ?>
          </div>
          <!-- PAUTAS ACTIVACION -->
          <?php $this->load->view("includes/partials/banner_sidebar.php"); ?>  
          <!-- TERMINA PAUTAS ACTIVACION -->  
          <div class="call_to_action block">
          <?php /* validacion solo las personar registradas podran crear ofertas y aplicar en algunas*/
          if($this->session->userdata('id_user_red')){ 
               if($this->session->userdata('tipo_user_red')==2 OR $this->session->userdata('tipo_user_red')==3){ 
          ?>
            <a href="#crear_oferta_box" class="button blue fancybox">Crear oferta de empleo</a>
            <div id="crear_oferta_box" class="fancy_skin_redgh" style="width:650px;display: none;">
              <h2>Crear oferta de empleo</h2>
              <div class="row">
                <?php echo form_open_multipart('ofertas/insertar_oferta','id="oferta_empleo_form"'); 
                 $perfil_oferta = array('name'=>'perfil_oferta','class'=>'required','id'=>'perfil_oferta','value'=> set_value('perfil_oferta'),'cols'=> '30','cols'=> '10');
                ?>
                    <div class="columns twelve">
                      <label for="titulo_oferta">Titulo de la Oferta:</label>
                      <input type="text" id="titulo_oferta" name="titulo_oferta" class="required" value="<?php echo set_value('titulo_oferta');?>">
                      <?php echo form_error('titulo_oferta'); ?>
                      <label for="perfil_oferta">Perfil de la oferta:</label>
                      <?php echo form_textarea($perfil_oferta); ?>
                      <p class='error_img'><?php echo form_error('perfil_oferta'); ?>
                    </div>
                    <div class="field">
                      <div class="columns six">
                        <label for="salario_oferta">Salario:</label>
                        <input type="text" id="salario_oferta" name="salario_oferta" class="required" value="<?php echo set_value('salario_oferta');?>">
                        <?php echo form_error('salario_oferta'); ?>
                      </div>
                      <div class="columns six">
                        <label for="tipo_oferta">Clase de contrato:</label>
                        <input type="text" id="tipo_oferta" name="tipo_oferta" class="required" value="<?php echo set_value('tipo_oferta');?>">
                        <?php echo form_error('tipo_oferta'); ?>
                      </div>
                    </div>
                    <div class="field">
                      <div class="columns six">
                        <label for="paais_oferta">País:</label>
                        <input type="text" id="paais_oferta" name="pais_oferta" class="required" value="<?php echo set_value('pais_oferta');?>">
                        <?php echo form_error('pais_oferta'); ?>
                      </div>
                      <div class="columns six">
                        <label for="ciudad_oferta">Ciudad:</label>                        
                        <input type="text" id="ciudad_oferta" name="ciudad_oferta" class="required" value="<?php echo set_value('ciudad_oferta');?>">
                        <?php echo form_error('ciudad_oferta'); ?>
                      </div>
                    </div>
                    <!--parte donde se a colocado el captcha -->
                    <div class="columns twelve">
                      <p>Escribe los caracteres que ves en la imagen de abajo, tediendo en cuenta las Mayúscula y Minúsculas. </p>
                    </div>
                    <div class="columns two">
                      <img src="data:image/png;base64,<?php echo $img_captcha;?>">
                    </div>
                    <div class="columns three">
                      <input type="text" name="captcha_aplicar_oferta">
                      <?php echo form_error('captcha_aplicar_oferta'); ?>
                    </div> 
                    <!--parte donde se a colocado el captcha -->
                    <div class="columns twelve">
                      <input type='checkbox' NAME="confidencial">Si desea mantener la información de su empresa como confidencial, por favor seleccione esta opcion!<P>
                    </div> 
                    
                    <div class="columns twelve">
                      <p>Si por el contrario, usted desea hacer publico el logo de su empresa, debe anexarlo a continuación en formato jpg, png, jpeg:</p>
                      <input type="file" id="adjuntar_oferta" name="img_oferta" class="">
                      <p class='error_img'></p><?php echo form_error('img_oferta'); ?>
                    </div>
                    <div class="clr"></div>
                    <div class="align_center submit_content">
                      <input type="submit" class="button gray" value="Crear Oferta">
                    </div>
                <?php echo form_close(); ?>
              </div>
            </div>
            <a href="#enviar_hdv_box" class="button blue fancybox">Enviar Hojas de Vida Referidos</a>
            <?php echo anchor("ofertas/referidas","Ver Hojas de Vida Referidas","class='button blue'"); ?>
            <?php //echo anchor("cronTest/Email","Probar contest","class='button blue'"); ?>
          <?php } ?>  
            <div id="enviar_hdv_box" class="fancy_skin_redgh" style="width:650px;display: none;">
              <h2>Referir Hoja de Vida</h2>
              <div class="row">
                <?php echo form_open_multipart('ofertas/referir_hv','id="oferta_empleo_form"'); 
                      $razones_referir = array('name'=>'razones_referir','class'=>'required','id'=>'razones_referir','value'=> set_value('razones_referir'),'cols'=> '30','cols'=> '10');
                ?>
                    <div class="columns twelve">
                      <label for="perfil_oferta">Razones para referir:</label>
                      <?php echo form_textarea($razones_referir); ?>
                      <?php echo form_error('razones_referir'); ?>
                    </div>                    
                   <div class="columns twelve">
                      <p>Escribe los caracteres que ves en la imágen de abajo. </p>
                      </div>
                      <div class="columns two">
                        <img src="data:image/png;base64,<?php echo $img_captcha;?>">
                      </div>
                      <div class="columns three">
                        <input type="text" name="captcha_razones_referir">
                        <?php echo form_error('captcha_razones_referir'); ?>
                   </div>
                    <div class="columns twelve">
                      <label for="adjuntar_oferta">Adjuntar Hoja de Vida:</label>
                      <input type="file" id="adjuntar_oferta" name="hoja_oferta">
                      <p class='error_img'><?php echo form_error('hoja_oferta'); ?>
                      <p>Anexar su HV en formato Word y pdf únicamente.</p>
                    </div>
                    <div class="clr"></div>
                    <div class="align_center submit_content">
                      <input type="submit" class="button gray" value="Enviar">
                    </div>
                <?php echo form_close(); ?>
              </div>
            </div>  
          <?php } ?>
          </div>
          <!-- PAUTAS ACTIVACION -->
          <?php $this->load->view("includes/partials/banner_sidebar1.php"); ?> 
          <!-- TERMINA PAUTAS ACTIVACION --> 
        </aside><!-- end block -->
      </div><!-- end block -->
    </section><!-- end container -->