<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Usuarios</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="clr"></div>
    <div class="columns three">
      <?php $attributes = array('class' => '', 'id' => 'search_form');
      echo form_open('admin/buscador',$attributes);
      $palabra=array('name'=>'palabra','id'=>'input_search','type'=>'text','placeholder'=>'Buscar');
      $btobuscar = array('value'=>'','id'=>'search','class'=>'search');?>
        <?php 
        echo form_input($palabra);
        echo form_error('palabra');
        ?>
      <?php echo form_submit($btobuscar); ?>
      <?php echo form_close(); ?>
    </div>
    <div class="columns twelve">
      <?php echo $msg1 ?>
      <table width="100%" id="users_table">
        <thead>
        <tr>
          <td>Nombre</td>
          <td>Apellido</td>
          <td>Cedula</td>
          <td>Email</td>
          <td>Telefono</td>
          <td>Pais</td>
          <td>Ciudad</td>
          <td>Profesion</td>
          <td>Empresa</td>
          <td>Sector</td>
          <td>Cargo</td>
          <td>Tipo de usuario</td> 
          <td>Editar</td>
          <td>Eliminar</td>
          <td>Estado</td>  
        </tr>
        </thead>
        <?php foreach ($usuario as $usuarios) { ?>
        <tr>
          <td><?php echo $usuarios['nombre'];?></td>
          <td><?php echo $usuarios['apellido'];?></td>
          <td><?php echo $usuarios['cedula'];?></td>
          <td><?php echo $usuarios['email'];?></td>
          <td><?php echo $usuarios['telefono'];?></td>
          <td><?php echo $usuarios['pais'];?></td>
          <td><?php echo $usuarios['ciudad'];?></td>
          <td><?php echo $usuarios['profesion'];?></td>
          <td><?php echo $usuarios['empresa'];?></td>
          <td><?php echo $usuarios['sector'];?></td>
          <td><?php echo $usuarios['cargo'];?></td>
          <td><?php echo $usuarios['idtipoUsuario'];?></td>
          <td><?php echo anchor('admin/editar_usuario/'.$usuarios['id'].'','Editar Usuario'); ?></td>
          <td><?php echo anchor('admin/eliminarUsuario/'.$usuarios['id'].'','Eliminar Usuario'); ?></td>
          <td><?php echo anchor('admin/cambiar_estado/'.$usuarios['id'].'','Cambiar Estado'); ?></td>
        </tr>
      <?php } ?>   
     </table>
     <ul class="pagination">
      <?php echo $this->pagination->create_links();?>
     </ul>
    </div>
  </div><!-- end block -->
</section><!-- end container -->