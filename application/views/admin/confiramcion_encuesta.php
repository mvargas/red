<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Confirmacion Activacion Encuesta</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns nine">
      <h2>¿Esta seguro que desea Activar esta encuesta?</h2>
      <?php echo anchor('admin/activar_encuesta/'.$id.'','Si',array("class"=>"success button")); ?>
      <?php echo anchor('admin/encuesta','No',array("class"=>"alert button")); ?>
    </div>
  </div><!-- end block -->
</section><!-- end container -->
