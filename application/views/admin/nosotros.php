<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Crear Contenido Nosotros</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns nine">
        <?php echo $msg?>
        <?php $attributes = array('class' => 'admin_crear_noticias custom', 'id' => 'register_form');
        echo  form_open_multipart('admin/crear_nosotros',$attributes); ?>
        <?php
        $titulo = array('name'=> 'titulo','id'=> 'titulo','class'=>'required','value'=>set_value('titulo'));
        $btoregistrar = array('value'=>'Guardar','class'=>'buttons bt_continuar'); ?>

        <label >Titulo:</label>
        <?php
        echo form_input($titulo);
        echo form_error('titulo');?>
       
        <label for="numero_hijos">Contenido:</label>
        <textarea id="textarea_contenido" name="contenido" rows="10" cols="50"></textarea>
        <?php echo form_error('contenido');?>

        <label for="adjuntar_oferta">Adjuntar imagen <small>(El tamaño de la imagen debe ser de 220px de ancho por 150px de alto)</small></label>
        <input type="file"  name="img_noticia" class="required">
        <p class='error_img'></p><?php echo form_error('img_noticia'); ?>
                        
        <div class="align_center">
          <?php echo form_submit($btoregistrar); ?>
        </div>
        <?php echo form_close(); ?>
     
    </div>
  </div><!-- end block -->
</section><!-- end container -->