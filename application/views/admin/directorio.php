<section class="container row">
  <div class="row block">
    <div class="columns twelve">
         <h1>Directorio</h1>
        <div class="columns two">
             <?php $this->load->view("includes/partials/admin_nav.php"); ?>
     	</div>
      
      <?php if ($directorio!=null){ ?>  
                      <table width="100%" id="users_table">
              <thead>
              <tr>
                <td>Id</td>
                <td>Nombre empresa</td>
                <td>Logotipo</td>
                <td>Persona de Contacto</td>
                <td>Telefono</td>
                <td>Email</td>
                <td>País</td>
                <td>Ciudad</td>
                <td>Pagina Web</td>
                <td>Clasificación</td>
                <td>Estado</td>
                <td>Editar</td>
                <td>Eliminar</td>
                
              </tr>
              </thead>

                <?php foreach ($directorio as $mostrar) { ?>
                <tr>
                  <td><?php echo $mostrar->id;?></td>
                  <td><?php echo $mostrar->nombre_empresa;?></td>
                  <td><img width="100" height="100" src="<?php echo base_url($mostrar->logotipo);?>"></td>
                  <td><?php echo $mostrar->persona_contacto;?></td>
                  <td><?php echo $mostrar->telefono;?></div></td>
                  <td><?php echo $mostrar->email;?></td>
                  <td><?php echo $mostrar->pais;?></td>
                  <td><?php echo $mostrar->ciudad;?></td>
                  <td><?php echo $mostrar->pagina_web;?></td>
                  <td><?php foreach ($clasificacion as $key) {
                    if($key['id_directorio'] == $mostrar->id)
                    echo $key['nombre'].", ";
                  }?></td>
                <?php if($mostrar->estado == 0){?>
                <?php echo "<td>".anchor('admin/activate_directorio/'.$mostrar->id,'Activar')."</td>"; }else {?>
                <?php echo "<td>".anchor('admin/desactivate_directorio/'.$mostrar->id,'Desactivar')."</td>"; }?>
                <td><?php echo anchor('admin/editar_directorio/'.$mostrar->id,'Editar'); ?></td>
                <td><?php echo anchor('admin/delete_directorio/'.$mostrar->id,'Eliminar'); ?></td>
              </tr>
              <?php }?>
            <?php } else{?>
              No hay directorios creados
           <?php  } ?>  
        </table>
  </div><!-- end block -->
</section>
