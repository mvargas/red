<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Eliminar Usuario</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns nine">
      <h2>¿Esta seguro que deseas Eliminar el Usuario?</h2>
      <?php echo anchor('admin/eliminar_user/'.$id.'','Si',array("class"=>"success button")); ?>
      <?php echo anchor('admin/estado_users','No',array("class"=>"alert button")); ?>
    </div>
  </div><!-- end block -->
</section><!-- end container -->