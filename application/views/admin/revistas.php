<section class="container row">
  <div class="row block">
    <div class="columns twelve">
         <h1>Revistas Publicadas</h1>
        <div class="columns two">
             <?php $this->load->view("includes/partials/admin_nav.php"); ?>
        </div>
        <div class="columns six">
            <?php echo $msg1; ?>
            <?php echo form_open('admin/insert_revista','id="aplicar_form"');
            $ubicacion = array('name'=>'ubicacion','class'=>'required','value'=> set_value('ubicacion'),'cols'=> '30','cols'=> '10');
            $nombre = array('name'=>'revista','class'=>'required','value'=> set_value('revista'));?>            
            <?php echo form_input($nombre); ?>
            <?php echo form_error('revista'); ?>
            <?php echo form_textarea($ubicacion); ?>
            <?php echo form_error('ubicacion'); ?>
            <input type="submit" name="enviar" value="Guardar" class="button">            
            <?php echo form_close();?>      
        </div>
    </div>
    <div class="twelve">
        <div class="columns nine">
            <label>Revistas</label>  
            <p><strong>Nota:</strong>La ultima revista insertada saldra por defecto de portada en la sección de revista</p>                             
            <table width="100%" id="users_table">
              <thead>
                <tr>
                  <td>Titulo</td>
                  <td>Revista</td>
                  <td>Eliminar</td>
                  <td>Editar</td>
                </tr>
              </thead>
                <?php if($revistas!=false){foreach ($revistas as $mostrar) {?>                
                <tr>
                  <td><?php echo $mostrar['nombre'] ?></td>
                  <td><?php echo $mostrar['ubicacion'] ?></td>
                  <td><?php echo anchor('admin/revista_delete/'.$mostrar['id'],'Eliminar'); ?></td>
                  <td><?php echo anchor('admin/updateRevista/'.$mostrar['id'],'Editar'); ?></td>
                </tr>
                <?php }} ?>
            </table>        
        </div>
    </div>
  </div><!-- end block -->
</section><!-- end container -->