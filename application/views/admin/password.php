<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Cambio Contraseña Administrador</h1> 
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns nine">
        <div class="row">
          <div class="columns four">
            <?php echo $msg?>  
            <?php   $attributes = array('class' => '', 'id' => 'register_form');
              echo form_open('admin/cambiar_password',$attributes); ?>
              <?php 
              $contrasena = array('name'=> 'contrasena','id'=> 'contrasena','class'=>'required');
              $repite_contrasena = array('name'=> 'repite_contrasena','id'=> 'repite_contrasena','class'=>'required');
              $btoregistrar = array('name'=> 'guardar','id'=> 'guardar','value'=>'Guardar','class'=>'button success');?>

              <label for="pass">Contraseña:</label>
              <?php
              echo form_password($contrasena);
              echo form_error('contrasena');?>
              <label for="repit_pass">Repite Contraseña:</label>
              <?php 
              echo form_password($repite_contrasena);
              echo form_error('repite_contrasena');?>
              <div class="codeign_error">
              </div>  
              <div class="align_center">
                <?php echo form_submit($btoregistrar); ?>
              </div>
            <?php echo form_close(); ?>
          </div>
        </div>
    </div>
  </div><!-- end block -->
</section><!-- end container -->