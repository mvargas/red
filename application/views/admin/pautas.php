<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Imagenes de las Pauas</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns nine">
      <?php echo $msg1; ?>
      <?php echo form_open_multipart('admin/pautas','id="aplicar_form"'); ?>
                    <div class="columns twelve">
                      <label for="adjuntar_oferta">Subir imagen</label>      
                      <input type="file" id="adjuntar_pautas" name="adjuntar_pautas" >
                      <p class='error_img'><?php echo form_error('adjuntar_pautas'); ?></p>                      
                      <p>Anexar archivos con extencion jpg,jpeg,png o swf<br>
                      <strong>Las dimensiones de la imagen deben ser de 300 de ancho por 320 de alto</strong></p>
                      <label for="adjuntar_oferta">Sitio de la Imagen</label> 
                      <input type="text" name="nombre">
                      <?php echo form_error('nombre'); ?>
                      <p>Url del Sitio donde se ira al hacer clic ejemplo:https://www.google.com</p>                      
                    </div>
                    <div class="align_center submit_content">
                      <input type="submit" class="button gray" value="Guardar">
                    </div>
        <?php echo form_close(); ?>
        <div class="columns twelve">          
          <?php
            if ($imagenes!=false) {
                echo '<label for="adjuntar_oferta">Selecciona la imagen para eliminar</label>';
                echo form_open('admin/imgHome','id="img_pauta_delete"'); 
                echo "<div class='content_img_home'>";
                foreach ($imagenes as $mostrar) {
                  if ($mostrar->tipo==4) {
                    echo '<div class="img_home"><object  width="100" height="100">';
                    echo '<param name="movie" value="'.base_url($mostrar->ruta).'" />';
                    echo '<param name="quality" value="high" />';
                    echo '<embed src="'.base_url($mostrar->ruta).'" quality="high" type="application/x-shockwave-flash" width="100" height="100" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>';
                    echo '</object><br>';                    
                    echo "<input type='radio' name='select_img' value='".$mostrar->id."'></div>";
                  }else{                  
                    echo "<div class='img_home'><img width='100' height='100' src='".base_url($mostrar->ruta)."'><br>";
                    echo "<input type='radio' name='select_img' value='".$mostrar->id."'></div>";
                  }                
                }
                echo "</div>";
                echo "<div class='clr'></div>";
                echo '<div class="align_center submit_content">';
                echo '<input type="hidden" name="eliminar_home" id="eliminar_home">   ';
                echo '<input type="button" class="alert button" id="delete_pauta" value="eliminar">';
                echo '</div>';
                echo form_close();
            }
          ?>
        </div>       
    </div>
  </div><!-- end block -->
</section><!-- end container -->