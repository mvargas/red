    <section class="container row">
      <div class="row block">
      </div><!-- end block -->
        <div class="columns three">
            <?php $this->load->view("includes/partials/admin_nav.php"); ?>
        </div>
      <div class="row block">
        <section class="columns eight" id="content_primary">
          <div class="noticias_content">
            <?php echo $msg; ?>
            <?php foreach ($noticia as $mostrar) { ?>            
            <div class="row block item">
              <div class="columns twelve">
                <h2><?php echo $mostrar->titulo; ?></h2>
              </div>
              <div class="columns five">
                <img src="<?php echo base_url($mostrar->img); ?>" alt="">
                <?php echo anchor('admin/editar_noticia/'.$mostrar->id.'','Editar',array('class'=>'view_more button blue','style'=>'width:100%;'))?>
                <?php echo anchor('admin/confirmacion_noticia/'.$mostrar->id.'','Eliminar',array('class'=>'view_more button blue','style'=>'width:100%;'))?>
              </div>
              <div class="columns seven" style="word-wrap: break-word;">
                <p><?php echo $mostrar->noticia;?></p>
              </div>
              <div class="align_right">
              </div>
            </div><!-- end item -->
            <?php } ?>
          </div><!-- end ofertas_content -->
        </section><!-- end block -->
      </div><!-- end block -->
    </section><!-- end container -->