

<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Nosotros</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns nine">
      <?php echo $msg?>
      <div class="ofertas_content">
        <?php if($nosotros!=null){?>
         <?php foreach ($nosotros as $mostrar) { ?>            
            <div class="nosotros_page">
              <div class="row block item">
                <div class="columns twelve">
                <h2><?php echo $mostrar->titulo; ?></h2>
              </div>
              <div class="columns three">
                <img src="<?php echo base_url($mostrar->foto); ?>" alt="">
                <?php echo anchor('admin/editar_nosotros/'.$mostrar->id.'','Editar', array('class'=>'button blue small','style'=>'width:100%;')); ?>
                <?php echo anchor('admin/confirmar_nosotros/'.$mostrar->id.'','Eliminar', array('class'=>'button blue small','style'=>'width:100%;')); ?>
              </div>
              <div class="columns nine">
                <p><?php echo $mostrar->texto?></p>
              </div>
            </div><!-- end item -->
            <?php } ?>
          <?php }else{
            echo 'No hay contenido';
          }  ?>
    </div>
  </div><!-- end block -->
</section><!-- end container -->            