<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Editar Ofertas</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns nine">
      <?php echo $msg?>
     <div class="row">
     <?php echo form_open_multipart('admin/guardar_oferta','id="oferta_empleo_form"'); 
      $id= array('name'=> 'id','id'=> 'idoferta','class'=>'required','type'=>'hidden','value'=>$ofertas[0]->id);
      $titulo = array('name'=> 'titulo','id'=> 'titulo','class'=>'','value'=>$ofertas[0]->titulo);
      $salario = array('name'=> 'salario','id'=> 'salario','class'=>'required','value'=>$ofertas[0]->salario);
      $clase_contrato = array('name'=> 'clase_contrato','id'=> 'clase_contrato','class'=>'required','value'=>$ofertas[0]->tipo_contrato);
      $pais= array('name'=> 'pais','id'=> 'pais','class'=>'required','value'=>$ofertas[0]->pais);
      $cuidad= array('name'=> 'cuidad','id'=> 'cuidad','class'=>'required','value'=>$ofertas[0]->ciudad);
      $btoregistrar = array('value'=>'Guardar','class'=>''); ?>
                <div class="columns twelve">
                      <label for="titulo_oferta">Titulo de la Oferta:</label>
                      <?php echo form_input($id);
                      echo form_input($titulo);
                       echo form_error('titulo'); ?>

                      <label for="perfil_oferta">Perfil de la oferta:</label>
                      <textarea type="text" id="perfil_oferta" name="perfil_oferta" cols="30" rows="10" class="required" value=""><?php echo $ofertas[0]->perfil;?></textarea>
                      <p class='error_img'><?php echo form_error('perfil_oferta'); ?>
                    </div>

                    <div class="field">
                      <div class="columns six">
                        <label for="salario_oferta">Salario:</label>
                        <?php echo form_input($salario);
                         echo form_error('salario'); ?>
                      </div>

                      <div class="columns six">
                        <label for="tipo_oferta">Clase de contrato:</label>
                        <?php echo form_input($clase_contrato);
                        echo form_error('clase_contrato'); ?>
                      </div>
                    </div>

                    <div class="field">
                      <div class="columns six">
                        <label for="paais_oferta">País:</label>
                        <?php echo form_input($pais);
                         echo form_error('pais'); ?>
                      </div>

                      <div class="columns six">
                        <label for="ciudad_oferta">Ciudad:</label> 
                        <?php echo form_input($cuidad);                       
                        echo form_error('cuidad'); ?>
                      </div>
                    </div>
                    <!--parte donde se a colocado el captcha -->
                    <div class="columns twelve">
                      <label for="adjuntar_oferta">Adjuntar imágen <small>(El tamaño de la imagen debe ser de 150px de ancho por 150px de alto)</small></label>
                      <input type="file" id="img_noticia" name="img_noticia" class="">
                      <p class='error_img'></p><?php echo form_error('img_noticia'); ?>
                    </div>
                    <div class="clr"></div>
                    <div class="align_center submit_content">
                      <input type="submit" class="button gray" value="Actualizar">
                      <?php echo anchor('admin/autorizar_ofertas','Cancelar',array('class'=>'button gray')); ?>
                    </div>
                <?php echo form_close(); ?>
              </div>
            </div>
    </div>
  </div><!-- end block -->
</section><!-- end container -->