<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Actualizacion Estado de las Solicitudes</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns nine">
      <?php echo $msg1; ?>
      <table width="100%" id="users_table">
        <thead>
        <tr>
          <td width="300" style="padding: 10px 20px 0px 30px;">
            <?php 
            $attributes = array('class' => '', 'id' => 'search_form');
            echo form_open('admin/buscar_solicitudes',$attributes);
            $palabra=array('name'=>'palabra','id'=>'input_search','type'=>'text','placeholder'=>'Buscar');
            $btobuscar = array('value'=>'','id'=>'search','class'=>'search');
            echo form_input($palabra);
            echo form_error('palabra');
            echo form_submit($btobuscar); 
            echo form_close(); ?>
          </td>
          <td width="300" style="padding: 10px 20px 0px 30px;">
            <?php echo form_open('admin/filtro_solicitud','id="filtro_solicitud_from"'); ?>
            <select name="filtro_solicitud" id="filtro_solicitud" class="admin required">
            <option value="">Filtrar Por:</option>
            <option value="0">Desactivados</option> 
            <option value="1">Activados</option>            
            </select>
            <?php echo form_close(); ?>
          </td>
        </tr>
        </thead>
      </table>                                
      <table width="100%" id="users_table">
        <thead>
        <tr>
          <td>Titulo</td>
          <td>Razon</td>
          <td>Usuario</td>
          <td>Estado</td>
          <td>Correo</td>
          <td>Fecha</td>
          <td>Cambiar</td>
          <td>Editar</td>
          <td>Eliminar</td>
        </tr>
        </thead>
        <?php if($solicitudes!=false){
              foreach ($solicitudes as $mostrar) { ?>
        <tr>
          <td><?php echo $mostrar->titulo; ?></td>
          <td><?php echo $mostrar->razon_solicitud; ?></td>
          <td><?php echo $mostrar->nombre; ?></td>
          <td><?php echo $mostrar->email; ?></td>
          <td><?php if($mostrar->estado==1){ echo "Activo";}else{echo "Desactivo";} ?></td>
          <td><?php echo $mostrar->fecha_solicitud; ?></td>
          <td><?php if($mostrar->estado==0){echo anchor('admin/estado_solicitud/'.$mostrar->id,'Cambiar');} ?></td>
          <td><?php echo anchor('admin/editar_solicitud/'.$mostrar->id,'Editar'); ?></td>
          <td><?php echo anchor('admin/delete_solicitud/'.$mostrar->id,'Eliminar'); ?></td>
        </tr>
      <?php }
          }else{ echo "<tr><td>No hay ningun registro</td></tr>";}?>   
     </table>
    </div>       
    </div>
  </div><!-- end block -->
</section><!-- end container -->