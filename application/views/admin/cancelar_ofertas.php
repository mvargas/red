<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Autorizar Ofertas</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns nine">
      <?php echo $msg?>
      <div class="ofertas_content">
        <?php if($ofertas !=FALSE){?> 
            <?php foreach ($ofertas as $mostrar) { ?>             
            <div class="row block item">
              <div class="columns twelve">
                <h2><?php echo $mostrar->titulo;?></h2>
              </div>
              <div class="columns three">
                <div class="img_oferta">
                  <img width="150" height="150 "src="<?php echo base_url($mostrar->logo); ?>" alt="">
                </div>
                <div class="call_to_action block">
                  <?php echo anchor('admin/cancelar/'.$mostrar->id.'','Despublicar', array('class'=>'button blue small')); ?>
                </div>
              </div>
              <div class="columns nine">
                <div class="info_oferta">
                  <h5>Perfil:</h5>
                  <div class="perfil_desc">
                    <p><?php echo $mostrar->perfil; ?></p>
                  </div>
                  <h5>Tipo Contrato:</h5>
                  <div class="perfil_desc">
                    <p><?php echo $mostrar->tipo_contrato; ?></p> 
                  </div>
                  <h5>Salario:</h5>
                  <div class="perfil_desc">
                    <p><?php echo $mostrar->salario; ?></p>
                  </div>
                  <h5>Ubicación:</h5>
                  <div class="perfil_desc">
                    <p><?php echo $mostrar->ciudad.", ".$mostrar->pais; ?></p>
                  </div>
                </div>
              </div>
            </div><!-- end item -->
            <?php } ?>
      <?php }  else {
           echo 'No hay ofertas';
      }?>  
    </div>
  </div><!-- end block -->
</section><!-- end container -->