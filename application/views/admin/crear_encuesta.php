<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Crear Encuesta</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns nine">
      <?php echo $msg?>
     <div class="row">
     <?php echo form_open_multipart('admin/insert_encuesta','id="register_form" class="custom"'); 
      $btoregistrar = array('value'=>'Crear','class'=>'button success'); ?>
                <div class="columns twelve">
                       <label for="numero_hijos">Pregunta:</label>
                       <textarea id="textarea_pregunta" name="pregunta" rows="10" cols="20" value=""></textarea>
                       <?php echo form_error('pregunta');?>
                  <label>Respuestas 1
                      <input name="respuesta1" value="" class ="required" type="text" id="encuesta1">
                  </label>
                </br>
                  <label>Respuestas 2
                      <input name="respuesta2" value=""  class ="required" type="text" id="encuesta2">
                  </label>
                </br>
                  <label>Respuestas 3
                      <input name="respuesta3" value="" class ="required" type="text" id="encuesta3">
                  </label>
                </br>
                </div>
                <?php echo form_submit($btoregistrar); ?>
                <?php echo form_close(); ?>
              </div>
            </div>
    </div>
  </div><!-- end block -->
</section><!-- end container -->
