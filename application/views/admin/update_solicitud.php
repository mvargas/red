<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Actualizar Solicitud <?php echo $solicitudes[0]->titulo; ?></h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns nine">
      <?php 
        echo $msg1;
        echo form_open('admin/update_solicitud');
        echo '<label>Titulo</label>
              <input name="id_solicitud" value="'.$solicitudes[0]->id.'" type="hidden" id="titulo">
              <input name="titulo" value="'.$solicitudes[0]->titulo.'" type="text" id="titulo">';
        echo form_error('titulo');
        echo '<label>Editar Solicitud</label>
              <textarea id="solicitud" name="solicitud" rows="10" cols="50">'.$solicitudes[0]->razon_solicitud.'</textarea>';
        echo form_error('solicitud');
        echo '<input type="submit" class="button success" value="Editar">    ';
        echo anchor('admin/solicitudes','Regresar',array("class"=>"button red"));
        echo form_close();      
      ?>                            
    </div>       
    </div>
  </div><!-- end block -->
</section><!-- end container -->