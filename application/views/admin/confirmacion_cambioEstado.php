<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Usuarios</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns nine">
      <h2>¿Esta seguro que deseas cambiar el estado de este usuario?</h2>
      <?php echo anchor('admin/confirmacion_cambiar_estado/'.$id.'','Si',array("class"=>"success button")); ?>
      <?php echo anchor('admin/estado_users','No',array("class"=>"alert button")); ?>
    </div>
  </div><!-- end block -->
</section><!-- end container -->
