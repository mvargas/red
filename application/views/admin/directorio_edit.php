<section class="container row">
      <div class="row block">
        <h2>Actualizar Directorio</h2>
        <div class="columns twelve">
          <div class="columns two">
            <img src="<?php echo base_url($directorio[0]['logotipo']); ?>">
          </div>
        </div>
        <div class="columns twelve">
          <div class="columns two">
            <p><h3>Banner</h3>
            <img src="<?php echo base_url($directorio[0]['banner']); ?>"></p>
          </div>
        </div>
        <div class="columns twelve">
            <section class="eight columns" id="content_primary">
             <?php foreach($directorio as $mostrar) { ?>
                <!-- Crear Clasificado --> 
                <div id="crear_clasificado_box" class="">
                  <div class="row">
                    <?php   echo form_open_multipart('admin/edit_directorio', 'id="oferta_empleo_form"');
                      $descripcion = array(
                        'name' => 'descripcion',
                        'class' => 'required',
                        'id' => 'descripcion',
                        'value' => $mostrar['descripcion'],
                        'rows' => '6',
                        'cols' => '6',
                        'maxlength' => '400'
                      );?> 
                        <input type="hidden" name="logotipo" value="<?php  echo $mostrar['logotipo']; ?>">
                        <input type="hidden" name="banner" value="<?php  echo $mostrar['banner']; ?>">
                          <div class="field">
                            <div class="columns six">
                              <label for="nombre_empresa">Nombre Empresa:</label>
                              <input type="text" id="nombre_empresa" name="nombre_empresa" class="required" value="<?php echo $mostrar['nombre_empresa']; ?>">
                            </div>                       
                          </div>
                        <div class="columns twelve">            
                          <input type="hidden" id="id" name="id" class="required" value="<?php echo $mostrar['id']; ?>">
                          <?php echo form_error('nombre_empresa'); ?>
                          <label for="descripcion">Descripción:</label>
                          <?php echo form_textarea($descripcion); ?>
                          <p class='error_img'><?php echo form_error('descripcion'); ?>
                        </div>
                        <div class="field">
                          <div class="columns six">
                            <div class="columns eleven">
                              <label for="persona_contacto">Persona de contacto:</label>
                              <input type="text" id="persona_contacto" name="persona_contacto" class="required" value="<?php echo $mostrar['persona_contacto']; ?>">
                              <?php echo form_error('persona_contacto'); ?>
                            </div>
                            <div class="columns nine">
                              <label for="telefono">Teléfono:</label>
                              <input type="text" id="telefono" name="telefono" class="required" value="<?php echo $mostrar['telefono']; ?>">
                              <?php echo form_error('telefono'); ?>
                            </div>
                            </div>
                          </div>
                        </div>
                        <div class="field">
                          <div class="columns nine">
                              <label for="email">Email:</label>
                              <input type="text" id="email" name="email" class="required" value="<?php echo $mostrar['email']; ?>">
                              <?php echo form_error('email'); ?>
                          </div>
                        </div>
                        <div class="field">
                        <div class="columns nine">
                            <label for="pagina_web">Pagina Web:</label>
                            <input type="text" id="pagina_web" name="pagina_web" class="required" value="<?php echo $mostrar['ciudad']; ?>">
                            <?php echo form_error('pagina_web'); ?>
                          </div>
                        </div>
                          <div class="field">
                            <div class="columns six">
                              <label>País</label>
                                  <select name="pais" class="admin required" id="pais">                         
                                  <option value="<?php echo $directorio[0]['pais']; ?>"><?php echo $directorio[0]['pais']; ?></option>
                                  <?php foreach ($paises as $mostrar_paises) {
                                  echo "<option  value='".$mostrar_paises->nombre."'>".$mostrar_paises->nombre."</option>";
                                  } ?> 
                                  </select>
                            </div>
                          <div class="columns six">
                            <label for="ciudad">Ciudad:</label>
                            <input type="text" id="ciudad" name="ciudad" class="required" value="<?php echo $mostrar['ciudad']; ?>">
                            <?php echo form_error('ciudad'); ?>
                          </div>
                          </div>
                          <div class="field">
                            <div class="columns six">
                              <label>Clasificación</label>
                              <?php 

                              function clas($c, $clasificacion){
                              foreach ($clasificacion as $value) {
                                if($value['id_clasificacion'] == $c){
                                  return "Checked" ;}
                              } 
                              }?>
                              <input type="checkbox" name="clasificacion[]" value="1" <?php echo clas(1,$clasificacion); ?> >Asesoria laboral<br>
                              <input type="checkbox" name="clasificacion[]" value="2" <?php echo clas(2,$clasificacion); ?> >Busqueda, evaluación y selección de talentos<br>
                              <input type="checkbox" name="clasificacion[]" value="3" <?php echo clas(3,$clasificacion); ?> >Clima, Cultura, Transformación y cambio<br>
                              <input type="checkbox" name="clasificacion[]" value="4" <?php echo clas(4,$clasificacion); ?> >Coaching<br>
                              <input type="checkbox" name="clasificacion[]" value="5" <?php echo clas(5,$clasificacion); ?> >Compensación y Beneficios<br>
                              <input type="checkbox" name="clasificacion[]" value="6" <?php echo clas(6,$clasificacion); ?> >Competencias<br>
                              <input type="checkbox" name="clasificacion[]" value="7" <?php echo clas(7,$clasificacion); ?> >Desarrollo y Capacitación<br>
                              <input type="checkbox" name="clasificacion[]" value="8" <?php echo clas(8,$clasificacion); ?> >Evaluación y desempeño<br>
                              <input type="checkbox" name="clasificacion[]" value="9" <?php echo clas(9,$clasificacion); ?> >Outplacement<br>
                              <input type="checkbox" name="clasificacion[]" value="10" <?php echo clas(10,$clasificacion); ?> >Outsourcing en RRHH<br>
                              <input type="checkbox" name="clasificacion[]" value="11" <?php echo clas(11,$clasificacion); ?> >Responsabilidad Social<br>
                              <input type="checkbox" name="clasificacion[]" value="12" <?php echo clas(12,$clasificacion); ?> >Seguridad<br>
                              <input type="checkbox" name="clasificacion[]" value="13" <?php echo clas(13,$clasificacion); ?> >Seguridad Industrial<br>
                              <input type="checkbox" name="clasificacion[]" value="14" <?php echo clas(14,$clasificacion); ?> >Otros<br>
                            </div>
                          </div>
                        </div>  
                        </div>                  
                        <!--parte donde se cambia el logo y el banner -->
                        <div class="columns twelve">
                            <p><strong style="color:red;">Nota: <br/> -Seleccione una imagen si desea cambiar el logotipo, de lo contrario no utilice esta función</strong></p>
                        </div>         
                        <div class="field">                   
                            <div class="columns six ">
                              <label for="Correo">Logotipo</label>
                              <input type="file" id="logotipo" name="logotipo" class="">
                              <p class='error_img'></p><?php echo form_error('logotipo'); ?>
                            </div>
                        </div> 
                        <div class="columns twelve">
                            <p><strong style="color:red;">Nota: <br/> -Seleccione una imagen si desea cambiar el banner, de lo contrario no utilice esta función</strong></p>
                        </div>         
                        <div class="field">                   
                            <div class="columns six ">
                              <label for="Correo">Banner</label>
                              <input type="file" id="banner" name="banner" class="">
                              <p class='error_img'></p><?php echo form_error('banner'); ?>
                            </div>
                        </div> 
                        <div class="clr"></div>
                        <div class="align_center submit_content">
                          <?php echo anchor('admin/directorio', 'Regresar', array( "class" => "button blue" )); ?>
                          <input type="submit" class="button gray" value="Actualizar Directorio">
                        </div>
                    <?php echo form_close(); }?>
            </section><!-- end block -->
        </div>
      </div><!-- end block -->
    </section><!-- end container -->