<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Actualizacion Estado de las Solicitudes</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns twelve">
      <?php echo $msg1; ?>
      <label>Clasificados Grandes</label>
      <table width="100%" id="users_table">
        <thead>
        <tr>
          <td width="300" style="padding: 10px 20px 0px 30px;">
            <?php 
            $attributes = array('class' => '', 'id' => 'search_form');
            echo form_open('admin/buscar_clasificado',$attributes);
            $palabra=array('name'=>'palabra','id'=>'input_search','type'=>'text','placeholder'=>'Buscar');
            $btobuscar = array('value'=>'','id'=>'search','class'=>'search');
            echo form_input($palabra);
            echo form_error('palabra');
            echo form_submit($btobuscar); 
            echo form_close(); ?>
          </td>
          <td width="300" style="padding: 10px 20px 0px 30px;">
            <?php echo form_open('admin/filtro_clasificados','id="filtro_solicitud_from"'); ?>
            <select name="filtro_solicitud" id="filtro_solicitud" class="admin required">
            <option value="">Filtrar Por:</option>
            <option value="0">Desactivados</option> 
            <option value="1">Activados</option>            
            </select>
            <?php echo form_close(); ?>
          </td>
        </tr>
        </thead>
      </table>                                
      <table width="100%" id="users_table">
        <thead>
        <tr>
          <td>Usuario</td>
          <!--<td>Correo</td>-->
          <td>Titulo</td>
          <td>Descripcion</td>
          <td>Imagen</td>
          <td>Estado</td>
          <td>Costo</td>
          <td>Forma de Pago</td>
          <td>Fecha</td>
          <td>Duracion</td>
          <td>Activar</td>
          <td>Editar</td>
          <td>Eliminar</td>
        </tr>
        </thead>
        <?php if($clasificado_pagado!=false){
              foreach ($clasificado_pagado as $mostrar) { ?>
        <tr>
          <td><?php echo $mostrar->nombre; ?></td>
          <!--<td><?php echo $mostrar->email; ?></td>-->
          <td><?php echo $mostrar->titulo; ?></td>
          <td><div style="word-wrap: break-word; width: 400px; height: auto;"><?php echo $mostrar->descripcion; ?></div></td>
          <td><img width="100" height="100" src="<?php echo base_url($mostrar->ruta_img);  ?>"></td>
          <td><?php if($mostrar->estado_clas==1){ echo "Activo";}else{echo "Desactivo";} ?></td>
          <td><?php echo $mostrar->costo; ?></td>
          <td><?php echo $mostrar->forma_pago; ?></td>
          <td><?php echo $mostrar->fecha_publicado; ?></td>
          <td><?php echo $mostrar->tiempo." Dias"; ?></td>
          <td><?php if($mostrar->estado_clas==0){echo anchor('admin/estado_clasificado/'.$mostrar->idC,'Activar');} ?></td>
          <td><?php echo anchor('admin/editar_clasificado/'.$mostrar->idC,'Editar'); ?></td>
          <td><?php echo anchor('admin/delete_clasificado/'.$mostrar->idC,'Eliminar'); ?></td>
        </tr>
      <?php }
          }else{ echo "<tr><td>No hay ningun registro</td></tr>";}?>
     </table>
    </div> 
    <div class="columns twelve">
      <label>Clasificados Pequeños</label>                           
      <table width="100%" id="users_table">
        <thead>
        <tr>          
          <td>Titulo</td>
          <td>Descripcion</td>
          <td>Imagen</td>
          <td>Estado</td>
          <td>Fecha</td>
          <td>Activar</td>
          <td>Eliminar</td>
        </tr>
        </thead>
        <?php if($clasificado_gratis!=false){
              foreach ($clasificado_gratis as $mostrar) { ?>
        <tr>
          <td><?php echo $mostrar->titulo; ?></td>
          <td><?php echo $mostrar->descripcion; ?></td>
          <td><img width="100" height="100" src="<?php echo base_url($mostrar->ruta_img);  ?>"></td>
          <td><?php if($mostrar->estado_clas==1){ echo "Activo";}else{echo "Desactivo";} ?></td>
          <td><?php echo $mostrar->fecha_publicado; ?></td>
          <td><?php if($mostrar->estado_clas==0){echo anchor('admin/estado_clasificadoP/'.$mostrar->id,'Activar');} ?></td>
          <td><?php echo anchor('admin/delete_clasificadoP/'.$mostrar->id,'Eliminar'); ?></td>
        </tr>
      <?php }
          }else{ echo "<tr><td>No hay ningun registro</td></tr>";}?>
     </table>
    </div>          
    </div>
  </div><!-- end block -->
</section><!-- end container -->