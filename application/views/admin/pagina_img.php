<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Imagenes de la Pagina</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns nine">
      <?php echo $msg1; ?>
        <?php echo form_open_multipart('admin/insert_pagina_img','id="aplicar_form"'); ?>
                    <div class="columns twelve">
                      <label for="adjuntar_oferta">Selecciona la Imagen a Subir</label><br>     
                      <input type="file" id="img_oferta" name="img_oferta" >
                      <input type="hidden" id="img_oferta" name="nombre" value="1">
                      <p class='error_img'><?php echo form_error('img_oferta'); ?></p>                      
                      <p>Anexar archivos con extencion jpg,jpeg,png<br>
                      <strong>La imagen debe tener 940 de ancho</strong></p>
                    </div>
                    <div class="align_center submit_content">
                      <input type="submit" class="button gray" value="Guardar">
                    </div>
        <?php echo form_close(); ?>
    </div>
       <div class="columns nine">
        <table width="100%" id="users_table">
		<thead>
			<tr>
			<td>Imagen</td>
			<td>Eliminar</td>
			</tr>
		</thead>
    <?php if($img_pagina!=false){ ?>
    	<?php foreach ($img_pagina as $mostrar) {?>
    			<tr>
                  <td><img src="<?php echo base_url($mostrar->ruta); ?>"></td>
    			  <td><?php echo anchor('admin/elmina_imgP/'.$mostrar->id,'Eliminar','class="button alert"'); ?></td>
    	        </tr>    	    
    	<?php }}else{echo '<tr><td>No hay Ninguna Imagen</td></tr>';} ?>
    	</table>
      <div>	                  
    </div>
  </div><!-- end block -->
</section><!-- end container -->