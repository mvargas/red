<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Hojas de Vida Referidas</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns nine">
      <?php echo $msg1; ?>
      <table width="100%" id="users_table">
        <thead>
        <tr>
          <td>Nombre</td>
          <td>Apellido</td>
          <td>Razones</td>
          <td>Hoja de Vida</td>
          <td>Eliminar</td>
        </tr>
        </thead>
        <?php if($hojas_referidas!=false){
              foreach ($hojas_referidas as $mostrar) { ?>
        <tr>
          <td><?php echo $mostrar->nombre;?></td>
          <td><?php echo $mostrar->apellido;?></td>
          <td><?php echo $mostrar->razones;?></td>
          <td><?php echo "<a href='".base_url($mostrar->hv_url)."'>".$mostrar->hv_url."</a>"; ?></td>
          <td><?php echo anchor('admin/deleteHV/'.$mostrar->id,'Eliminar'); ?></td>
        </tr>
      <?php }
          }else{ echo "<tr><td>No hay ningun registro</td></tr>";}?>   
     </table>
  </div>
</section> 