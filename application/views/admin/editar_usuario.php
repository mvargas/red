<section class="container row">
      <!-- register_form -->
      <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
      </div>
      <h1>Editar Datos Usuarios</h1>
      <div class="columns nine">
        <?php echo $msg1 ?>
      <?php echo form_open('admin/actualizar_datos_usuarios','id="register_form" class="custom"');?>
        <div class="row block">
          <div class="field">
            <div class="four columns">
              <label for="email_user">E-mail:</label>
              <input type="hidden" id="id" value="<?php echo $usuarios[0]->id ?>" name="id" class="">
              <input type="text" id="email_user" value="<?php echo $usuarios[0]->email ?>" name="correo" class="required email">
              <?php echo form_error('correo'); ?>
            </div>
            <div class="four columns">
              <label for="phone_user">Teléfono:</label>
              <input type="text" id="phone_user" value="<?php echo $usuarios[0]->telefono ?>" name="telefono" class="required">
              <?php echo form_error('telefono'); ?>
            </div>
            
            <div class="four columns">
              <label for="country_user">País de residencia:</label>
              <select name="pais" id="country_user" class="required">
                <option value="<?php echo $usuarios[0]->pais ?>"><?php echo $usuarios[0]->pais ?></option>
                <?php foreach ($paises as $mostrar_paises) {
                echo "<option value='".$mostrar_paises->nombre."'>".$mostrar_paises->nombre."</option>";
                } ?>
              </select>
              <?php echo form_error('pais'); ?>
            </div>
          </div>
          <?php if ($usuarios[0]->pais!='Colombia'){
            $view='display:none';
          }else {
            $view='display';
          }?>
          <!-- div colombia-->
          <div class="four columns last" style="<?php echo $view ?>" id="C2">
            <label for="city_user">Ciudad de residencia:</label>
            <select name="ciudad" id="city_user" class="required">
              <option value="<?php echo $usuarios[0]->ciudad ?>"><?php echo $usuarios[0]->ciudad ?></option>
              <option value="Barranquilla">Barranquilla</option>
              <option value="Bogota">Bogotá</option>
              <option value="Bucaramanga">Bucaramanga</option>
              <option value="Cali">Cali</option>
              <option value="Cartagena">Cartagena</option>
              <option value="Medellin">Medellín</option>
              <option value="Otra Ciudad">Otra Ciudad</option>
            </select>
            <?php echo form_error('ciudad'); ?>
          </div>
          <!-- fin div colombia-->
        </div><!-- end block -->
        <div class="row block">
          <div class="four columns">
            <label for="profession_user">Profesión:</label>
            <input type="text" id="profession_user" value="<?php echo $usuarios[0]->profesion ?>" name="profesion" class="required">
            <?php echo form_error('profesion'); ?>
          </div>
          <div class="four columns">
            <label for="company_user">Empresa:</label>
            <input type="text" id="company_user" value="<?php echo $usuarios[0]->empresa ?>" name="empresa">
          </div>
          <div class="four columns">
            <label for="sector_user">Sector:</label>
            <select name="sector" id="sector_user" class="required" style="width:1000px">
              <option value="<?php echo $usuarios[0]->cargo ?>"><?php echo $usuarios[0]->cargo ?></option>
              <?php foreach ($sectores as $mostrar_sector) {
                echo "<option value='".$mostrar_sector->nombre."'>".$mostrar_sector->nombre."</option>";
              } ?>
            </select>
            <?php echo form_error('sector'); ?>
          </div>
          <div class="four columns last">
            <label for="role_user">Cargo:</label>
            <input type="text" id="role_user" value="<?php echo $usuarios[0]->cargo ?>" name="cargo">
          </div>
          <div class="four columns ">
              <label for="phone_user">Contraseña:</label>
              <input type="password" id="contrasena" value="" name="contrasena" class="">
              <?php echo form_error('contrasena'); ?>
          </div>
          <div class="four columns">
              <label for="phone_user">Confirmar Contraseña:</label>
              <input type="password" id="repite_contrasena" value="" name="repite_contrasena" class="">
              <?php echo form_error('repite_contrasena'); ?>
          </div>
        </div><!-- end block -->
        <input type="submit" class="button" value="Editar" class="required"><br><br>
        <?php echo form_close(); ?><!-- end register_form -->
      </div>
</section><!-- end container -->