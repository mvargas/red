<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Autorizar Ofertas</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns nine">
      <?php echo $msg?>
      <div class="clr"></div>
        <div class="columns ten">
        <table width="100%" id="users_table">
          <thead>
          <tr>
            <td width="300" style="padding: 10px 20px 0px 30px;">
                <?php $attributes = array('class' => '', 'id' => 'search_form');
                echo form_open('admin/buscadorOfertas',$attributes);
                $palabra=array('name'=>'palabra','id'=>'input_search','type'=>'text','placeholder'=>'Buscar');
                $btobuscar = array('value'=>'','id'=>'search','class'=>'search');
                echo form_input($palabra);
                echo form_error('palabra');
                echo form_submit($btobuscar);
                echo form_close(); ?>
            </td>
            <td width="100" style="padding: 10px 20px 0px 30px;">
                <?php $attributes = array('class' => '', 'id' => 'filtro_ofertas_from');
                echo form_open('admin/filtro',$attributes);
                $palabra=array('name'=>'palabra','id'=>'input_search','type'=>'text','placeholder'=>'Buscar');?>                
                <select name="filtro" id="filtro_ofertas" class="admin required">
                <option value="">Filtrar Por :</option>
                <option value="0">Sin aprobar</option>
                <option value="1">Aprobado</option>
                </select>
                <?php echo form_error('filtro'); ?>
                <?php echo form_close(); ?>
            </td>
          </tr>
          </thead>
        </table>    
        </div>
           <?php if ($ofertas!=FALSE) { ?>
      <div class="ofertas_content">
            <?php foreach ($ofertas as $mostrar) { ?>             
            <div class="row block item">
              <div class="columns twelve">
                <h2><?php echo $mostrar->titulo;?></h2>
                <?php $nombre=$this->model_admin->NombreUsuarios($mostrar->id); ?>
                <h3>Creado por: <?php echo $nombre[0]->nombre;?></h3>
              </div>
              <div class="columns three">
                <div class="img_oferta">
                  <img width="150" height="150 "src="<?php echo base_url($mostrar->logo); ?>" alt="">
                </div>
                <div class="call_to_action block">
                 <?php if ($mostrar->estado==0){ echo anchor('admin/publicar/'.$mostrar->id.'','Publicar', array('class'=>'button blue small')); }?>
                  <?php echo anchor('admin/editar_oferta/'.$mostrar->id.'','Editar', array('class'=>'button blue small')); ?>
                  <?php echo anchor('admin/confirmar_oferta/'.$mostrar->id.'','Eliminar', array('class'=>'button blue small')); ?>
                </div>
              </div>
              <div class="columns nine">
                <div class="info_oferta">
                  <h5>Perfil:</h5>
                  <div class="perfil_desc">
                    <p><?php echo $mostrar->perfil; ?></p>
                  </div>
                  <h5>Tipo Contrato:</h5>
                  <div class="perfil_desc">
                    <p><?php echo $mostrar->tipo_contrato; ?></p> 
                  </div>
                  <h5>Salario:</h5>
                  <div class="perfil_desc">
                    <p><?php echo $mostrar->salario; ?></p>
                  </div>
                  <h5>Ubicación:</h5>
                  <div class="perfil_desc">
                    <p><?php echo $mostrar->ciudad.", ".$mostrar->pais; ?></p>
                  </div>
                  <h5>Estado:</h5>
                  <div class="perfil_desc">
                    <p><?php if ($mostrar->estado==0){
                      echo 'Sin aprobar';
                      }else {
                        echo 'Aprobado';
                    } ?></p>
                  </div>
                  <h5>Correo:</h5>
                  <div class="perfil_desc">
                    <p><?php echo $nombre[0]->email; ?></p>
                  </div>
                </div>
              </div>
            </div><!-- end item -->
            <?php } ?>
        <?php } else {
          echo '<div class="columns nine">';
          echo 'No hay Ofertas pos Publicar';
          echo '</div>';
        }?>
    </div>
  </div><!-- end block -->
</section><!-- end container -->