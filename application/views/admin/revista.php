<section class="container row">
  <div class="row block">
    <div class="columns twelve">
         <h1>Revistas Publicadas</h1>
        <div class="columns two">
             <?php $this->load->view("includes/partials/admin_nav.php"); ?>
        </div>
        <div class="columns six">
            <?php echo form_open('admin/update_revista','id="aplicar_form"');
            $ubicacion = array('name'=>'ubicacion','class'=>'required','value'=> $revista[0]['ubicacion'],'cols'=> '30','cols'=> '10');
            $nombre = array('name'=>'revista','class'=>'required','value'=> $revista[0]['nombre']);?>
            <input type="hidden" value="<?php echo $revista[0]['id'] ?>" name="idRevista">
            <?php echo form_input($nombre); ?>
            <?php echo form_error('revista'); ?>
            <?php echo form_textarea($ubicacion); ?>
            <?php echo form_error('ubicacion'); ?>
            <input type="submit" name="enviar" value="Actualizar" class="button">
            <?php echo anchor('admin/revistas','Regresar','class="button success"'); ?>            
            <?php echo form_close();?>      
        </div>
    </div>
  </div><!-- end block -->
</section><!-- end container -->