<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Descargar Base de datos</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns nine">
      <?php echo $msg1; ?>
      <?php echo form_open_multipart('admin/base_datos','id="aplicar_form"'); ?>
          <label for="adjuntar_oferta">Formato  Base de Datos en SQL</label>
          <strong><input type="radio" id="bd" name="bd" > Selecionar para descargar base de datos de datos Completa</strong><br><br>
          <input type="submit" class="button blue" value="Descargar">
      <?php echo form_close(); ?>
    </div> 
    <div class="columns nine">
      <?php echo form_open_multipart('admin/usuario_descarga','id="aplicar_form"'); ?>
          <label for="adjuntar_oferta">Descargar usuario en Excel</label>
          <p><strong>Selecciona el Tipo de Usuario para Descargar</strong></p>
            <select name="tipo_usuario" id="" class="admin required">
            <option value="1">Usuario Tipo 1</option> 
            <option value="2">Usuario Tipo 2</option>            
            </select><br><br>
          <input type="submit" class="button blue" value="Descargar">
      <?php echo form_close(); ?>
    </div>                
    </div>
  </div><!-- end block -->
</section><!-- end container -->
