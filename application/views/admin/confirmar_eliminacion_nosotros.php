<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Eliminar Nosotros</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns nine">
      <h2>¿Esta seguro que deseas Eliminar el contenido?</h2>
      <?php echo anchor('admin/eliminar_nosotros/'.$id.'','Si',array("class"=>"success button")); ?>
      <?php echo anchor('admin/ver_nosotros','No',array("class"=>"alert button")); ?>
    </div>
  </div><!-- end block -->
</section><!-- end container -->