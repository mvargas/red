<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Banner Home</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns nine">
      <?php echo $msg1; ?>
      <?php echo form_open_multipart('admin/insert_miniBanner','id="aplicar_form"'); ?>
                    <div class="columns twelve">
                      <div class="columns five">
                          <label for="adjuntar_oferta">Nombre de la Imagen</label> 
                          <input type="text" name="nombre_img" class="required" value="<?php echo set_value('nombre_img');?>">
                          <?php echo form_error('nombre_img'); ?> 
                      </div>
                      <div class="columns six">
                          <label for="adjuntar_oferta">Subir imagen</label>
                          <input type="file" id="mini_banner" name="mini_banner" class="required">
                          <p class='error_img'><?php echo form_error('mini_banner'); ?></p>
                          <p>Anexar archivos con extensión jpg,jpeg,png</p> 
                          <p>El Archivo debe tener dimensiones de 1000x100</p>
                      </div>                                          
                    </div>
                    <div class="align_center submit_content">
                      <input type="submit" class="button gray" value="Guardar">
                    </div>
               <?php echo form_close(); ?>
        <div class="columns twelve">          
          <?php
            if ($imagenes!=false) {
                echo '<label>Selecciona la imagen ha eliminar</label>';
                echo form_open('admin/delete_miniBanner','id="img_home_delete"'); 
                echo "<div class='content_img_miniBanner'>";
                foreach ($imagenes as $mostrar) {
                    echo "<div class='img_home'><img width='100' height='100' src='".base_url($mostrar['url_img'])."'><br>";
                    echo "<input type='radio' name='delete' value='".$mostrar['id']."'></div>";
                }
                echo "</div>";
                echo "<div class='clr'></div>";
                echo '<div class="align_center submit_content">';
                echo '<input type="submit" class="alert button" value="eliminar">';
                echo '</div>';
                echo form_close();
            }
          ?>
        </div>       
    </div>
  </div><!-- end block -->
</section><!-- end container -->