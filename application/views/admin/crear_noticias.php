 <script type="text/javascript" src="<?php echo base_url(); ?>js/tiny_mce/tiny_mce.js"></script>
        <script type="text/javascript">
                 tinyMCE.init({
                // General options
                mode : "textareas",
                theme : "advanced",
                plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks",

                // Theme options
                theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
                theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
                theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft,visualblocks",
                theme_advanced_toolbar_location : "top",
                theme_advanced_toolbar_align : "left",
                theme_advanced_statusbar_location : "bottom",
                theme_advanced_resizing : true,

                // Example content CSS (should be your site CSS)
                content_css : "css/content.css",

                // Drop lists for link/image/media/template dialogs
                template_external_list_url : "lists/template_list.js",
                external_link_list_url : "lists/link_list.js",
                external_image_list_url : "lists/image_list.js",
                media_external_list_url : "lists/media_list.js",

                // Style formats
                style_formats : [
                    {title : 'Bold text', inline : 'b'},
                    {title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
                    {title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
                    {title : 'Example 1', inline : 'span', classes : 'example1'},
                    {title : 'Example 2', inline : 'span', classes : 'example2'},
                    {title : 'Table styles'},
                    {title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
                ],

                // Replace values for the template plugin
                template_replace_values : {
                    username : "Some User",
                    staffid : "991234"
                }
            });
        </script>
<section class="container row">
  <div class="row block">
    <div class="columns twelve">
      <h1>Crear Noticas</h1>
    </div>
    <div class="columns three">
      <?php $this->load->view("includes/partials/admin_nav.php"); ?>
    </div>
    <div class="columns nine">
        <?php echo $msg?>
        <?php $attributes = array('class' => 'admin_crear_noticias custom', 'id' => 'register_form');
        echo  form_open_multipart('admin/crear_noticias',$attributes); ?>
        <?php
        $titulo = array('name'=> 'titulo','id'=> 'titulo','class'=>'required','value'=>set_value('titulo'));
        $btoregistrar = array('value'=>'Guardar','class'=>'buttons bt_continuar'); ?>
        <label >Titulo:</label>
        <?php
        echo form_input($titulo);
        echo form_error('titulo');?>
        

        <label for="numero_hijos">Noticia:</label>
        <textarea id="textarea_noticia" name="noticia" rows="10" cols="50"></textarea>
        <?php echo form_error('noticia');?>

       
        <label for="adjuntar_oferta">Adjuntar imgen <small>(El tamaño de la imagen debe ser de 620px de ancho por 380px de alto)</small></label>
        <input type="file"  name="img_noticia" class="required">
        <p class='error_img'></p><?php echo form_error('img_noticia'); ?>
                        
        <div class="align_center">
          <?php echo form_submit($btoregistrar); ?>
        </div>
        <?php echo form_close(); ?>


     
    </div>
  </div><!-- end block -->
</section><!-- end container -->

