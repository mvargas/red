    <section class="container row">
      <div class="row block">
        <div class="twelve columns">
         <!-- slaider-->
         <?php $this->load->view("includes/partials/slider.php"); ?>
         <!-- End slaider-->
        </div>
      </div><!-- end block -->
      <div class="row block">
        <section class="columns eight" id="content_primary">
          <div class="noticias_content">
            <?php foreach ($noticia as $mostrar) { ?>            
            <div class="row block item">
              <div class="columns twelve">
                <h2><?php echo $mostrar->titulo; ?></h2>
              </div>
              <div class="columns five">
                <img src="<?php echo base_url($mostrar->img); ?>" alt="">
              </div>
              <div class="columns seven" style="word-wrap: break-word;">
                <p><?php echo Index::corta_palabra($mostrar->noticia,200)."...";?></p>
              </div>
              <div class="align_right">
                <?php echo anchor('index/noticia/'.$mostrar->id.'','Ver noticia completa','class="view_more button blue"'); ?>
              </div>
            </div><!-- end item -->
            <?php } ?>
          </div><!-- end ofertas_content -->
        </section><!-- end block -->
        <aside class="columns four" id="side_bar">
          <!-- PAUTAS ACTIVACION -->
          <?php $this->load->view("includes/partials/banner_sidebar.php"); ?> 
          <!-- TERMINA PAUTAS ACTIVACION --> 
        </aside><!-- end block -->
      </div><!-- end block -->
    </section><!-- end container -->