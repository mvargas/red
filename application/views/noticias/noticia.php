<section class="container row">
      <div class="row block">
        <section class="columns eight" id="content_primary">
          <div class="noticias_content">
          	<div class="block post_content">
          		<h2><?php echo $noticia[0]->titulo; ?></h2>
          		<div class="img_noticia">
	          		<img src="<?php echo base_url($noticia[0]->img); ?>" alt="">
	          	</div>
	          	<?php echo $noticia[0]->noticia; ?>
          	</div>
          </div><!-- end ofertas_content -->
        </section><!-- end block -->
        <aside class="columns four" id="side_bar">
          <!-- PAUTAS ACTIVACION -->
          <?php $this->load->view("includes/partials/banner_sidebar.php"); ?>  
          <!-- TERMINA PAUTAS ACTIVACION --> 
        </aside><!-- end block -->
      </div><!-- end block -->
    </section><!-- end container -->
