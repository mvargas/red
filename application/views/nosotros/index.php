    <section class="container row">
      <div class="row block">
        <div class="twelve columns">
         <!-- slaider-->
         <?php $this->load->view("includes/partials/slider.php"); ?>
         <!-- End slaider-->
        </div>
      </div><!-- end block -->
      <?php if ($nosotros!=NULL){?>
       <?php foreach ($nosotros as $mostrar) { ?>            
            <div class="nosotros_page">
              <div class="row block item">
                <div class="columns twelve">
                <h2><?php echo $mostrar->titulo; ?></h2>
              </div>
              <div class="columns three">
                <img src="<?php echo base_url($mostrar->foto); ?>" alt="">
              </div>
              <div class="columns nine">
                <p><?php echo $mostrar->texto?></p>
              </div>
            </div><!-- end item -->
            <?php }}else{echo "No hay noticias por mostrar";} ?>

      </div><!-- end block -->
    </section><!-- end container -->