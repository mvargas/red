<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Servicios extends CI_Controller {

  public function __construct ()
  {       
      parent :: __construct (); 
      $this->load->model('model_index');
      $this->load->model('model_servicios');
      $this->load->model('model_admin');
      $this->load->model('model_registro');       
  }
    /*-----------------------------------
    FUNCION QUE INICIALIZARA POR DEFECTO*/
  public function index($msg = ""){
      $clasificado_pagado=$this->model_servicios->clasificados();
      $clasificado_gratis=$this->model_servicios->clasificados1();
          /*funciones para eliminar clasificado a los 30 dias*/    
            if($clasificado_pagado!=false){
              foreach ($clasificado_pagado as $mostrar) {
                $fecha=date('Y-m-d');
                $dias=$this->diferenciaDias($mostrar->fecha_publicado,$fecha);
                  if($dias>$mostrar->tiempo){
                    unlink($mostrar->ruta_img);
                    if ($mostrar->ruta_img1!=""){unlink($mostrar->ruta_img1);}
                    if ($mostrar->ruta_img2!=""){unlink($mostrar->ruta_img2);}
                    if ($mostrar->ruta_img3!=""){unlink($mostrar->ruta_img3);}
                    if ($mostrar->ruta_img4!=""){unlink($mostrar->ruta_img4);}
                    $this->model_servicios->delete_clasificado($mostrar->id);
                  }
            }}
            if($clasificado_gratis!=false){
              foreach ($clasificado_gratis as $mostrar1) {
                $fecha=date('Y-m-d');
                $dias=$this->diferenciaDias($mostrar1->fecha_publicado,$fecha);
                  if($dias>15){
                    unlink($mostrar1->ruta_img);
                    $this->model_servicios->delete_clasificado1($mostrar1->id);
                  }
            }}
            /***************************************/ 
      $clasificado_pagado_new=$this->model_servicios->clasificados();
      $clasificado_gratis_new=$this->model_servicios->clasificados1();     
      $img_pautas=$this->model_index->imagen_pauta();
      $img_pautas1=$this->model_index->imagen_pauta();
      $data=array(
        'img_pauta'=>$img_pautas,
        'img_pauta1'=>$img_pautas1,
        'img_captcha' => $this->img_captcha(),
        'msg'     => $msg,
        'clasificado_gratis' =>$clasificado_gratis_new,
        'clasificado_pagado' => $clasificado_pagado_new,
        'view'    => "servicios/index",
      );  
      $this->load->view('includes/template',$data);
  }
  /*----------------------------------------
  FUNCION QUE MOSTRAR DIAS DE DIFERENCIAS */    
  public function diferenciaDias($inicio, $fin)
  {
      $inicio = strtotime($inicio);
      $fin = strtotime($fin);
      $dif = $fin - $inicio;
      $diasFalt = ((($dif/60)/60)/24);
      return ceil($diasFalt);
  }
  /*----------------------------------------------
  FUNCION QUE MOSTRAR UNA CANTIDAD DE CARANTERES*/  
  public static function corta_palabra($palabra,$num)
  {
    $largo=strlen($palabra);//indicarme el largo de una cadena
    $cadena=substr($palabra,0,$num);
    return $cadena;
  }  
    /*----------------------------------------
    FUNCION PARA GENERAR UN NUEMERO ALEATORIO*/ 
    public function Captchar_String($length,$uc=TRUE,$n=TRUE,$sc=FALSE){
    $source = 'abcdefghijklmnopqrstuvwxyz';
    if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      if($n==1) $source .= '1234567890';
        // if($sc==1) $source .= '|@#~$%()=^*+[]{}-_';
        if($length>0){
        $rstr = "";
        $source = str_split($source,1);
        for($i=1; $i<=$length; $i++){
          mt_srand((double)microtime() * 1000000);
          $num = mt_rand(1,count($source));
          $rstr .= $source[$num-1];
        }
      }
      return $rstr;
    } 
    /*----------------------------------------
    FUNCION PARA IMAGEN CON CODIGO DE CAPTCHA*/     
    public function img_captcha(){
      $numero= $this->Captchar_String(7,TRUE,TRUE,TRUE); 
      $imagen_p =   imagecreate(100, 50);
      $fondo = imagecolorallocate($imagen_p, 250, 14, 100);
      $color_texto = imagecolorallocate($imagen_p, 255, 255, 255);
      imagestring($imagen_p, 20, 15, 15, $numero, $color_texto);
                ob_start (); 
            imagepng($imagen_p);
            $image_data = ob_get_contents (); 
            ob_end_clean (); 
            $image_data_base64 = base64_encode ($image_data);

             $captcha = array(
             'numero_captcha' => $numero
            );
            $this->session->set_userdata($captcha);
            return $image_data_base64;
    }
    /*--------------------------------
    FUNCION PARA INSERTAR ClASIFICADO*/    
    public function insertar_clasificado(){
      if($_POST){
           $this->form_validation->set_rules('titulo_clasificado','Titulo','required');
           $this->form_validation->set_rules('perfil_clasificado','Perfil Clasificado','required|max_length[400]');
           $this->form_validation->set_rules('telefono','Telefono','required');
           $this->form_validation->set_rules('correo','Correo','required|valid_email');
           // $this->form_validation->set_rules('costo','Costo','required');
           // $this->form_validation->set_rules('forma_pago','Forma de Pago','required');
           $this->form_validation->set_rules('captcha_aplicar_oferta','Campo','required|callback__validacion_captcha');
           $this->form_validation->set_rules('tiempo','Tiempo','required');
           /*validaciones de imagenes*/
           $this->form_validation->set_rules('img_oferta','Imagen','callback__validacion_img');
           $this->form_validation->set_message('_validacion_img','El formato debe ser jpeg,jpg,png,gif con dimenciones 300*300');           
           if (isset($_FILES['img_oferta1']) AND $_FILES['img_oferta1']['name']!="") {
               $this->form_validation->set_rules('img_oferta1','Imagen','callback__validacion_img1');
               $this->form_validation->set_message('_validacion_img1','El formato debe ser jpeg,jpg,png,gif con dimenciones 300*300');           
           }
           if (isset($_FILES['img_oferta2']) AND $_FILES['img_oferta2']['name']!="") {
               $this->form_validation->set_rules('img_oferta2','Imagen','callback__validacion_img2');
               $this->form_validation->set_message('_validacion_img2','El formato debe ser jpeg,jpg,png,gif con dimenciones 300*300');           
           }
           if (isset($_FILES['img_oferta3']) AND $_FILES['img_oferta3']['name']!="") {
               $this->form_validation->set_rules('img_oferta3','Imagen','callback__validacion_img3');
               $this->form_validation->set_message('_validacion_img3','El formato debe ser jpeg,jpg,png,gif con dimenciones 300*300');           
           }
           if (isset($_FILES['img_oferta4']) AND $_FILES['img_oferta4']['name']!="") {
               $this->form_validation->set_rules('img_oferta4','Imagen','callback__validacion_img4');
               $this->form_validation->set_message('_validacion_img4','El formato debe ser jpeg,jpg,png,gif con dimenciones 300*300');           
           }
           /*Termina validaciones de imagenes*/
           $this->form_validation->set_message('valid_email','Correo no valido');
           $this->form_validation->set_message('required','%s es requerido');
           $this->form_validation->set_message('max_length','El maximo de caracteres son 400');
           $this->form_validation->set_message('_validacion_captcha','El Codigo ingresado es Invalido');
           if ($this->form_validation->run()==FALSE) {            
             $msg = '<script type="text/javascript" language="text/javascript">setTimeout(function(){$.fancybox($("#crear_clasificado_box"));},100);</script>';
             $this->index($msg);
           }else{
               $usuario=$this->session->userdata('nombre_user_red');
               $rutaDestino1="";
               $rutaDestino2="";
               $rutaDestino3="";
               $rutaDestino4="";
               $rutaServidor="images/clasificado_imagen";
               /* Parte para validar si vienen imagenes e insertarlas*/
               if (isset($_FILES['img_oferta1']) AND $_FILES['img_oferta1']['name']!="") {
                   $rutaTemporal1= $_FILES["img_oferta1"]["tmp_name"];
                   $info1=getimagesize($_FILES["img_oferta1"]["tmp_name"]); 
                   $nombre1= date("Y_m_d_H_i_s").$usuario.$_FILES["img_oferta1"]["name"];
                   $rutaDestino1= $rutaServidor.'/'.$nombre1;
                   move_uploaded_file($rutaTemporal1, $rutaDestino1) ; 
                   $thumb_width1 = ($info1[0] > 300) ? 300 : $info1[0];
                   $thumb_height1 = ($info1[1] > 300) ? 300 : $info1[1];
                   $this->thumbnail($rutaDestino1,$info1[0],$info1[1],$nombre1,$thumb_width1, $thumb_height1);
               }
               if (isset($_FILES['img_oferta2']) AND $_FILES['img_oferta2']['name']!="") {
                   $rutaTemporal2= $_FILES["img_oferta2"]["tmp_name"];
                   $info2=getimagesize($_FILES["img_oferta2"]["tmp_name"]); 
                   $nombre2= date("Y_m_d_H_i_s").$usuario.$_FILES["img_oferta2"]["name"];
                   $rutaDestino2= $rutaServidor.'/'.$nombre2;
                   move_uploaded_file($rutaTemporal2, $rutaDestino2) ;
                   $thumb_width2 = ($info2[0] > 300) ? 300 : $info2[0];
                   $thumb_height2 = ($info2[1] > 300) ? 300 : $info2[1];
                   $this->thumbnail($rutaDestino2,$info2[0],$info2[1],$nombre2,$thumb_width2, $thumb_height2);
               }
               if (isset($_FILES['img_oferta3']) AND $_FILES['img_oferta3']['name']!="") {
                   $rutaTemporal3= $_FILES["img_oferta3"]["tmp_name"];
                   $info3=getimagesize($_FILES["img_oferta3"]["tmp_name"]); 
                   $nombre3= date("Y_m_d_H_i_s").$usuario.$_FILES["img_oferta3"]["name"];
                   $rutaDestino3= $rutaServidor.'/'.$nombre3;
                   move_uploaded_file($rutaTemporal3, $rutaDestino3) ;
                   $thumb_width3 = ($info3[0] > 300) ? 300 : $info3[0];
                   $thumb_height3 = ($info3[1] > 300) ? 300 : $info3[1];
                   $this->thumbnail($rutaDestino3,$info3[0],$info3[1],$nombre3,$thumb_width3, $thumb_height3);
               }
               if (isset($_FILES['img_oferta4']) AND $_FILES['img_oferta4']['name']!="") {
                   $rutaTemporal4= $_FILES["img_oferta4"]["tmp_name"];
                   $info4=getimagesize($_FILES["img_oferta4"]["tmp_name"]); 
                   $nombre4= date("Y_m_d_H_i_s").$usuario.$_FILES["img_oferta4"]["name"];
                   $rutaDestino4= $rutaServidor.'/'.$nombre4;
                   move_uploaded_file($rutaTemporal4, $rutaDestino4) ;
                   $thumb_width4 = ($info4[0] > 300) ? 300 : $info4[0];
                   $thumb_height4 = ($info4[1] > 300) ? 300 : $info4[1];
                   $this->thumbnail($rutaDestino4,$info4[0],$info4[1],$nombre4,$thumb_width4, $thumb_height4);
               }

               $rutaTemporal= $_FILES["img_oferta"]["tmp_name"];
               $info=getimagesize($_FILES["img_oferta"]["tmp_name"]); 
               $nombre= date("Y_m_d_H_i_s").$usuario.$_FILES["img_oferta"]["name"];
               $rutaDestino= $rutaServidor.'/'.$nombre;
               move_uploaded_file($rutaTemporal, $rutaDestino) ;
               $thumb_width = ($info[0] > 300) ? 300 : $info[0];
               $thumb_height = ($info[1] > 300) ? 300 : $info[1];
               $this->thumbnail($rutaDestino,$info[0],$info[1],$nombre,$thumb_width, $thumb_height);

               
               $rutaDestino='images/clasificado_imagen/final/'.$nombre;
               if(isset($nombre1) && $nombre1 != ""){
                $rutaDestino1='images/clasificado_imagen/final/'.$nombre1;
                }
               if(isset($nombre2) &&$nombre2 != ""){
               $rutaDestino2='images/clasificado_imagen/final/'.$nombre2;
                }
               if(isset($nombre3) && $nombre3 != ""){
               $rutaDestino3='images/clasificado_imagen/final/'.$nombre3;
                }
               if(isset($nombre4) && $nombre4 != ""){
               $rutaDestino4='images/clasificado_imagen/final/'.$nombre4;
                }

               $datos=array(
                'id' => $this->session->userdata('id_user_red'),   
                'titulo' => $this->input->post('titulo_clasificado'),
                'tipo_clasificado' => $this->input->post('tipo_clasificado'),
                'perfil' => $this->input->post('perfil_clasificado'),
                'telefono' => $this->input->post('telefono'),
                'correo' => $this->input->post('correo'),
                // 'costo' => $this->input->post('costo'),
                // 'forma_pago' => $this->input->post('forma_pago'),
                'ruta_img' =>  $rutaDestino,
                'ruta_img1' => $rutaDestino1,
                'ruta_img2' => $rutaDestino2,
                'ruta_img3' => $rutaDestino3,
                'ruta_img4' => $rutaDestino4,
                'fecha' => date("Y-m-d H:i:s"),
                'tiempo' => $this->input->post('tiempo'),
               );
               $this->model_servicios->registro($datos); 
               $ultimio_id=mysql_insert_id();
               redirect('servicios/mostrario/'.$ultimio_id);
           }
    }else{        
      redirect('servicios');
    }   
    } 
    /*--------------------------------
    FUNCION PARA INSERTAR ClASIFICADO*/    
    public function insertar_pequeno(){
      if($_POST){
           $this->form_validation->set_rules('titulo_clasificado','Titulo','required');
           $this->form_validation->set_rules('perfil_clasificado','Perfil Clasificado','required|max_length[100]');
           $this->form_validation->set_rules('correo','Correo','required|valid_email');
           $this->form_validation->set_rules('captcha_aplicar_oferta','Campo','required|callback__validacion_captcha');
           $this->form_validation->set_rules('img_oferta','Imagen','callback__validacion_img0');
           $this->form_validation->set_message('_validacion_img0','El formato debe ser jpeg,jpg,png,gif con Dimensiones de 200 x 170');           
           $this->form_validation->set_message('valid_email','Correo no valido');
           $this->form_validation->set_message('required','%s es requerido');
           $this->form_validation->set_message('max_length','El maximo de caracteres son 100');
           $this->form_validation->set_message('_validacion_captcha','El Codigo ingresado es Invalido');
           if ($this->form_validation->run()==FALSE) {            
             $msg = '<script type="text/javascript" language="text/javascript">setTimeout(function(){$.fancybox($("#crear_clasificado_p_box"));},100);</script>';
             $this->index($msg);
           }else{
               $usuario=$this->session->userdata('nombre_user_red');
               $rutaServidor="images/clasificado_imagen";
               $rutaTemporal= $_FILES["img_oferta"]["tmp_name"];
               $info=getimagesize($_FILES["img_oferta"]["tmp_name"]); 
               $nombre= date("Y_m_d_H_i_s").$usuario.$_FILES["img_oferta"]["name"];
               $rutaDestino= $rutaServidor.'/'.$nombre;
               move_uploaded_file($rutaTemporal, $rutaDestino) ;
               $thumb_width = ($info[0] > 200) ? 200 : $info[0];
               $thumb_height = ($info[1] > 170) ? 170 : $info[1];
               $this->thumbnail($rutaDestino,$info[0],$info[1],$nombre,$thumb_width, $thumb_height);

               $datos=array(
                'id' => $this->session->userdata('id_user_red'),   
                'titulo' => $this->input->post('titulo_clasificado'),
                'perfil' => $this->input->post('perfil_clasificado'),
                'correo' => $this->input->post('correo'),
                'ruta_img' => 'images/clasificado_imagen/final/'.$nombre,
                'fecha' => date("Y-m-d H:i:s"),
               );
               $this->model_servicios->registro_pequeno($datos); 
                    echo "<script type='text/javascript'>
                    alert('\"Su Clasificado estara en linea en menos de 12 horas, si cumple con los parametros legales permitidos\"');
                    window.location='../servicios';
                    </script>";
           }
    }else{        
      redirect('servicios');
    }   
    }    
    /*-------------------------------------------------------
    FUNCION PARA MOSTAR COMO SE VE EL CLASIFICADO EN AL WEB*/    
    public  function mostrario($id)    { 
      if (isset($id) AND $id!="") {
          $img_pautas=$this->model_index->imagen_pauta();
          $img_pautas1=$this->model_index->imagen_pauta();
          $clasificado=$this->model_servicios->clasificado_id($id);
          if ($clasificado[0]['aprovado']==0) {
            $data=array(
              'img_pauta'=>$img_pautas,
              'img_pauta1'=>$img_pautas1,
              'msg'     => '',
              'clasificado_pagado' => $clasificado,
              'view'    => "servicios/mostrario",
            );  
            $this->load->view('includes/template',$data);
          }else{
            redirect('servicios');
          }
      }else{ redirect('servicios'); }     
    }
        /*-------------------------------------------------------
    FUNCION PARA MOSTAR COMO SE VE EL CLASIFICADO EN AL WEB*/    
    public  function aprovacion($id){
      if (isset($id) AND $id!="") {
        $this->model_servicios->aprovar($id);
        redirect('servicios');
      }else{ redirect('servicios'); }     
    }
    /*-----------------------------------
    FUNCION PARA ELIMINAR  UN CLASIFICADO*/ 
    public function cancela($id){
      if (isset($id) AND $id!="") {
        $clasificado=$this->model_servicios->clasificado_id($id);
        if ($clasificado[0]['aprovado']==0) {
          unlink($clasificado[0]['ruta_img']);
          $this->model_servicios->delete_clasificado($id);
          redirect("servicios");
        }else{ redirect("servicios");}
      }else{ redirect('servicios'); }  
    }    
    /*-----------------------------
    FUNCION PARA BUSCAR SOLICITUDES*/
    public function buscador(){
      if ($_POST AND $_POST['bucar_solicitud']!="") {
          $clasificado_pagado=$this->model_servicios->buscador($this->input->post('bucar_solicitud'));
          $clasificado_gratis=$this->model_servicios->buscador1($this->input->post('bucar_solicitud'));
          $img_pautas=$this->model_index->imagen_pauta();
          $img_pautas1=$this->model_index->imagen_pauta();
          $data=array(
            'img_pauta'=>$img_pautas,
            'img_pauta1'=>$img_pautas1,
            'img_captcha' => $this->img_captcha(),
            'msg'     => '',
            'clasificado_gratis' =>$clasificado_gratis,
            'clasificado_pagado' => $clasificado_pagado,
            'view'    => "servicios/index",
          );  
          $this->load->view('includes/template',$data);
      }else{
          redirect('servicios');  
      }
    }     
    /*-----------------------------
    FUNCION PARA VALIDAR EL CAPTCHA*/ 
      public function _validacion_captcha($captcha){
     if ($this->session->userdata('numero_captcha') == $captcha){
      return true;}else{return false;}  
    } 
    /*--------------------------------------------------------
    FUNCION PARA VALIDAR EL TIPO DE IMAGEN CLASIFICADO GRANDE 1*/ 
    public function _validacion_img($FILES){
     if ($_FILES["img_oferta"]["type"]=="image/jpeg" || $_FILES["img_oferta"]["type"]=="image/pjpeg" || $_FILES["img_oferta"]["type"]=="image/gif" || $_FILES["img_oferta"]["type"]=="image/bmp" || $_FILES["img_oferta"]["type"]=="image/png")
      {
          $info=getimagesize($_FILES["img_oferta"]["tmp_name"]);       
          if ($info[0]>=300 AND $info[1]>=300)
          { return true;}else{ return false;}; 
      }else{return false;}  
    } 
    /*--------------------------------------------------------
    FUNCION PARA VALIDAR EL TIPO DE IMAGEN CLASIFICADO GRANDE 2*/ 
    public function _validacion_img1($FILES){
     if ($_FILES["img_oferta1"]["type"]=="image/jpeg" || $_FILES["img_oferta1"]["type"]=="image/pjpeg" || $_FILES["img_oferta1"]["type"]=="image/gif" || $_FILES["img_oferta1"]["type"]=="image/bmp" || $_FILES["img_oferta1"]["type"]=="image/png")
      {
          $info=getimagesize($_FILES["img_oferta1"]["tmp_name"]);       
          if ($info[0]>=300 AND $info[1]>=300)
          { return true;}else{ return false;}; 
      }else{return false;}  
    }
    /*--------------------------------------------------------
    FUNCION PARA VALIDAR EL TIPO DE IMAGEN CLASIFICADO GRANDE 3*/ 
    public function _validacion_img2($FILES){
     if ($_FILES["img_oferta2"]["type"]=="image/jpeg" || $_FILES["img_oferta2"]["type"]=="image/pjpeg" || $_FILES["img_oferta2"]["type"]=="image/gif" || $_FILES["img_oferta2"]["type"]=="image/bmp" || $_FILES["img_oferta2"]["type"]=="image/png")
      {
          $info=getimagesize($_FILES["img_oferta2"]["tmp_name"]);       
          if ($info[0]>=300 AND $info[1]>=300)
          { return true;}else{ return false;}; 
      }else{return false;}  
    }
    /*--------------------------------------------------------
    FUNCION PARA VALIDAR EL TIPO DE IMAGEN CLASIFICADO GRANDE 4*/ 
    public function _validacion_img3($FILES){
     if ($_FILES["img_oferta3"]["type"]=="image/jpeg" || $_FILES["img_oferta3"]["type"]=="image/pjpeg" || $_FILES["img_oferta3"]["type"]=="image/gif" || $_FILES["img_oferta3"]["type"]=="image/bmp" || $_FILES["img_oferta3"]["type"]=="image/png")
      {
           $info=getimagesize($_FILES["img_oferta3"]["tmp_name"]);       
          if ($info[0]>=300 AND $info[1]>=300)
          { return true;}else{ return false;};
      }else{return false;}  
    }
    /*--------------------------------------------------------
    FUNCION PARA VALIDAR EL TIPO DE IMAGEN CLASIFICADO GRANDE 5*/ 
    public function _validacion_img4($FILES){
     if ($_FILES["img_oferta4"]["type"]=="image/jpeg" || $_FILES["img_oferta4"]["type"]=="image/pjpeg" || $_FILES["img_oferta4"]["type"]=="image/gif" || $_FILES["img_oferta4"]["type"]=="image/bmp" || $_FILES["img_oferta4"]["type"]=="image/png")
      {
          $info=getimagesize($_FILES["img_oferta4"]["tmp_name"]);       
          if ($info[0]>=300 AND $info[1]>=300)
          { return true;}else{ return false;}; 
      }else{return false;}  
    }                    
    /*--------------------------------------------------------
    FUNCION PARA VALIDAR EL TIPO DE IMAGEN CLASIFICADO PEQUEÑA*/ 
    public function _validacion_img0($FILES){
     if ($_FILES["img_oferta"]["type"]=="image/jpeg" || $_FILES["img_oferta"]["type"]=="image/pjpeg" || $_FILES["img_oferta"]["type"]=="image/gif" || $_FILES["img_oferta"]["type"]=="image/bmp" || $_FILES["img_oferta"]["type"]=="image/png")
      {
          $info=getimagesize($_FILES["img_oferta"]["tmp_name"]);       
          if ($info[0]>=200 AND $info[1]>=170)
          { return true;}else{ return false;}; 
      }else{return false;}  
    }
    /*FUNCION QUE VALIDA LA INSERCION DE LOGOTIPO EN EL DIRECTORIO*/
    public function _validacion_logotipo($FILES){
     if ($_FILES["logotipo"]["type"]=="image/jpeg" || $_FILES["logotipo"]["type"]=="image/pjpeg" || $_FILES["logotipo"]["type"]=="image/gif" || $_FILES["logotipo"]["type"]=="image/bmp" || $_FILES["logotipo"]["type"]=="image/png")
      {
           $info=getimagesize($_FILES["logotipo"]["tmp_name"]);       
          if ($info[0] == $info[1])
          { return true;}else{ return false;};
      }else{return false;}  
    }
    /*FUNCION PARA VALIDAR EL BANNER DEL DIRECTORIO*/
    public function _validacion_banner($FILES){
     if ($_FILES["banner"]["type"]=="image/jpeg" || $_FILES["banner"]["type"]=="image/pjpeg" || $_FILES["banner"]["type"]=="image/gif" || $_FILES["banner"]["type"]=="image/bmp" || $_FILES["banner"]["type"]=="image/png")
      {
          $info=getimagesize($_FILES["banner"]["tmp_name"]);       
          if ($info[0]>=600 AND $info[1]>=400)
          { return true;}else{ return false;};
      }else{return false;}  
    }

    public function editar($id,$datos="")
    {
     $img_pautas=$this->model_index->imagen_pauta();
     $img_pautas1=$this->model_index->imagen_pauta();
      if ($datos!=""){
         $data=array(
         'img_pauta'=>$img_pautas,
         'img_pauta1'=>$img_pautas1,
         'msg'     => '',
         'view'    => "servicios/update_clasificado",
         'clasificado'     => $datos,
         ); 
         $this->load->view('includes/template',$data); 
      }else{
         $clasificado = $this->model_servicios->clasificado_id($id);
         if ($clasificado[0]['aprovado']==0) {
           $data=array(
           'img_pauta'=>$img_pautas,
           'img_pauta1'=>$img_pautas1,
           'msg'     => '',
           'view'    => "servicios/update_clasificado",
           'clasificado'     => $clasificado,
           ); 
           $this->load->view('includes/template',$data);
         }else{
          redirect('servicios');
         }
      }
    } 
 /*-------------------------------------
  FUNCION PARA ACTUALIZAR EL CLASIFICADO*/
  public function update_clasificado(){
        $this->form_validation->set_rules('titulo_clasificado','Titulo','required');
        if (isset($_FILES['img_oferta']) AND $_FILES['img_oferta']['name']!="") {
           $this->form_validation->set_rules('img_oferta','Imagen','callback__validacion_img');
           $this->form_validation->set_message('_validacion_img','El formato debe ser jpeg,jpg,png,gif con dimenciones 300*300');           
        }
        if (isset($_FILES['img_oferta1']) AND $_FILES['img_oferta1']['name']!="") {
           $this->form_validation->set_rules('img_oferta1','Imagen','callback__validacion_img1');
           $this->form_validation->set_message('_validacion_img1','El formato debe ser jpeg,jpg,png,gif con dimenciones 300*300');           
        }
        if (isset($_FILES['img_oferta2']) AND $_FILES['img_oferta2']['name']!="") {
           $this->form_validation->set_rules('img_oferta2','Imagen','callback__validacion_img2');
           $this->form_validation->set_message('_validacion_img2','El formato debe ser jpeg,jpg,png,gif con dimenciones 300*300');           
        }
        if (isset($_FILES['img_oferta3']) AND $_FILES['img_oferta3']['name']!="") {
           $this->form_validation->set_rules('img_oferta3','Imagen','callback__validacion_img3');
           $this->form_validation->set_message('_validacion_img3','El formato debe ser jpeg,jpg,png,gif con dimenciones 300*300');           
        }
        if (isset($_FILES['img_oferta4']) AND $_FILES['img_oferta4']['name']!="") {
           $this->form_validation->set_rules('img_oferta4','Imagen','callback__validacion_img4');
           $this->form_validation->set_message('_validacion_img4','El formato debe ser jpeg,jpg,png,gif con dimenciones 300*300');           
        }
        $this->form_validation->set_message('required','%s es requerido');
     if ($this->form_validation->run()==FALSE) {
         $datos=array(0=>array(
          'titulo'=>$this->input->post('titulo_clasificado'),
          'id'=>$this->input->post('id_clasificado'),
          'descripcion'=>$this->input->post('perfil_clasificado'),
          'telefono'=>$this->input->post('telefono'),
          'correo'=>$this->input->post('correo'),
          'costo'=>$this->input->post('costo'),
          'ruta_img'=>$this->input->post('ruta_img'),
          'ruta_img1'=>$this->input->post('ruta_img1'),
          'ruta_img2'=>$this->input->post('ruta_img2'),
          'ruta_img3'=>$this->input->post('ruta_img3'),
          'ruta_img4'=>$this->input->post('ruta_img4'),
          'forma_pago'=>$this->input->post('forma_pago'),
          'tiempo'=>$this->input->post('tiempo'),)
         );    
         $this->editar($this->input->post('id_clasificado'),$datos);
     }else{
           $usuario=$this->session->userdata('nombre_user_red');
           $rutaDestino=  $this->input->post('ruta_img');
           $rutaDestino1= $this->input->post('ruta_img1');
           $rutaDestino2= $this->input->post('ruta_img2');
           $rutaDestino3= $this->input->post('ruta_img3');
           $rutaDestino4= $this->input->post('ruta_img4');
           $rutaServidor= "images/clasificado_imagen";
           /* Parte para validar si vienen imagenes e insertarlas*/
           if (isset($_FILES['img_oferta']) AND $_FILES['img_oferta']['name']!="") {
               $rutaTemporal= $_FILES["img_oferta"]["tmp_name"];
               $nombre= date("Y-m-d H:i:s").$usuario.".jpeg";
               $rutaDestino= $rutaServidor.'/'.$nombre;
               move_uploaded_file($rutaTemporal, $rutaDestino);                
           }
           /* Parte para validar si vienen imagenes e insertarlas*/
           if (isset($_FILES['img_oferta1']) AND $_FILES['img_oferta1']['name']!="") {
               $rutaTemporal1= $_FILES["img_oferta1"]["tmp_name"];
               $nombre1= date("Y-m-d H:i:s").$usuario."1.jpeg";
               $rutaDestino1= $rutaServidor.'/'.$nombre1;
               move_uploaded_file($rutaTemporal1, $rutaDestino1);                
           }
           if (isset($_FILES['img_oferta2']) AND $_FILES['img_oferta2']['name']!="") {
               $rutaTemporal2= $_FILES["img_oferta2"]["tmp_name"];
               $nombre2= date("Y-m-d H:i:s").$usuario."2.jpeg";
               $rutaDestino2= $rutaServidor.'/'.$nombre2;
               move_uploaded_file($rutaTemporal2, $rutaDestino2);
           }
           if (isset($_FILES['img_oferta3']) AND $_FILES['img_oferta3']['name']!="") {
               $rutaTemporal3= $_FILES["img_oferta3"]["tmp_name"];
               $nombre3= date("Y-m-d H:i:s").$usuario."3.jpeg";
               $rutaDestino3= $rutaServidor.'/'.$nombre3;
               move_uploaded_file($rutaTemporal3, $rutaDestino3);
           }
           if (isset($_FILES['img_oferta4']) AND $_FILES['img_oferta4']['name']!="") {
               $rutaTemporal4= $_FILES["img_oferta4"]["tmp_name"];
               $nombre4= date("Y-m-d H:i:s").$usuario."4.jpeg";
               $rutaDestino4= $rutaServidor.'/'.$nombre4;
               move_uploaded_file($rutaTemporal4, $rutaDestino4);
           }    
          $datos=array( 
          'id' => $this->input->post('id_clasificado'),  
          'titulo' => $this->input->post('titulo_clasificado'),
          'perfil' => $this->input->post('perfil_clasificado'),
          'telefono' => $this->input->post('telefono'),
          'correo' => $this->input->post('correo'),
          'costo' => $this->input->post('costo'),
          'forma_pago' => $this->input->post('forma_pago'),
          'tiempo'=>$this->input->post('tiempo'),
          'ruta_img' => $rutaDestino,
          'ruta_img1' => $rutaDestino1,
          'ruta_img2' => $rutaDestino2,
          'ruta_img3' => $rutaDestino3,
          'ruta_img4' => $rutaDestino4,
          );
          $idC=$this->input->post('id_clasificado');
          $this->model_admin->update_clasificado($datos);
          redirect('servicios/mostrario/'.$idC);
      } 
  }    

  /*************redicir imagen *///
  private function thumbnail($image_info,$image_height,$image_width,$nombre,$width = 200, $height = 170) {
    $this->load->library('image_lib');
    //Create Default Thumbnail
    $thumb_config = array(
        'image_library' => 'gd2',
        'source_image' => $image_info,
        'new_image' => 'images/clasificado_imagen/final/',
        'create_thumb' => true,
        'thumb_marker' => '',
        'maintain_ratio' => true
    );

    if ($image_height >= $image_width) {
      $thumb_config['width'] = $width;
      $thumb_config['height'] = intval(($image_height * $thumb_config['width']) / $image_width);
    } else {
      $thumb_config['height'] = $height;
      $thumb_config['width'] = intval(($image_width * $thumb_config['height']) / $image_height);
    }

    $this->image_lib->initialize($thumb_config);
    $this->image_lib->resize();

    $leftCrop = intval(($thumb_config['width'] - $width) / 2);
    $crop_config = array(
        'source_image' => 'images/clasificado_imagen/final/'.$nombre,
        'width' => $width,
        'height' => ($thumb_config['height'] < $height) ? $thumb_config['height'] : $height,
        'x_axis' => $leftCrop,
        'y_axis' => 0,
        'maintain_ratio' => true
    );
    $this->image_lib->initialize($crop_config);
    $this->image_lib->crop();
    $this->image_lib->clear();
  }  

  private function thumbnail2($image_info,$image_height,$image_width,$nombre,$width = 300, $height = 300) {
    $this->load->library('image_lib');
    //Create Default Thumbnail
    $thumb_config = array(
        'image_library' => 'gd2',
        'source_image' => $image_info,
        'new_image' => 'images/clasificado_imagen/final/',
        'create_thumb' => true,
        'thumb_marker' => '',
        'maintain_ratio' => true
    );

    if ($image_height >= $image_width) {
      $thumb_config['width'] = $width;
      $thumb_config['height'] = intval(($image_height * $thumb_config['width']) / $image_width);
    } else {
      $thumb_config['height'] = $height;
      $thumb_config['width'] = intval(($image_width * $thumb_config['height']) / $image_height);
    }

    $this->image_lib->initialize($thumb_config);
    $this->image_lib->resize();

    $leftCrop = intval(($thumb_config['width'] - $width) / 2);
    $crop_config = array(
        'source_image' => 'images/clasificado_imagen/final/' . $nombre,
        'width' => $width,
        'height' => ($thumb_config['height'] < $height) ? $thumb_config['height'] : $height,
        'x_axis' => $leftCrop,
        'y_axis' => 0,
        'maintain_ratio' => false
    );
    $this->image_lib->initialize($crop_config);
    $this->image_lib->crop();
    $this->image_lib->clear();
  } 

  /*FUNCIONES DE DIRECTORIO*/
  public function directorio ($id=0, $msg=""){
      //$img_pagina=$this->model_index->imagen_pagina2();
      $img_pautas=$this->model_index->imagen_pauta();
      $img_pautas1=$this->model_index->imagen_pauta();
      $directorio=$this->model_servicios->directorio($id);
      $paises = $this->model_registro->Mostrar_paises();  
      $data=array(
        'img_pauta'=>$img_pautas,
        'img_pauta1'=>$img_pautas1,
        'msg'     => $msg,
        'img_captcha' => $this->img_captcha(),  
        'directorio' => $directorio,
        'view'    => "servicios/directorio",
        'paises' => $paises,
      );  
      $this->load->view('includes/template',$data);
  }


  public function _validacion_clasificacion($clas){
    if($clas!=""){
      return true;
    }else{
      return false;
    }
  }

  /*FUNCION PARA INSERTAR EN EL DIRECTORIO*/
  public function insertar_directorio(){
      if($_POST){
           $clasificacion = $this->input->post('clasificacion');
           $this->form_validation->set_rules('nombre_empresa','Nombre','required');
           $this->form_validation->set_rules('descripcion','Descripcion','required|max_length[100]');
           $this->form_validation->set_rules('email','Email','required|valid_email');
           $this->form_validation->set_rules('ciudad','Ciudad','requiered');
           $this->form_validation->set_rules('pais','Pais','requiered');
           $this->form_validation->set_rules('ciudad','Ciudad','requiered');
           $this->form_validation->set_rules('clasificacion[]','Clasificacion','requiered|min(clasificacion[])_length[1]');  
           $this->form_validation->set_rules('captcha_aplicar_oferta','Campo','required|callback__validacion_captcha');
           $this->form_validation->set_rules('logotipo','Logotipo','callback__validacion_logotipo');
           $this->form_validation->set_rules('banner','Banner','callback__validacion_banner');
           //$this->form_validation->set_rules('clasificacion','Clasificacion','callback__validacion_clasificacion($clasificacion)');
           if (isset($_FILES['logotipo']) AND $_FILES['logotipo']['name']!="") {
               $this->form_validation->set_rules('logotipo','Logotipo','callback__validacion_logotipo');
               $this->form_validation->set_message('_validacion_logotipo','El formato debe ser jpeg,jpg,png,gif con dimenciones cuadradas');           
           }
           if (isset($_FILES['banner']) AND $_FILES['banner']['name']!="") {
               $this->form_validation->set_rules('banner','Banner','callback__validacion_banner');
               $this->form_validation->set_message('_validacion_banner','El formato debe ser jpeg,jpg,png,gif con dimenciones cuadradas');           
           }
           //$this->fopen(filename, mode)rm_validation->set_message('_validacion_logotipo','El formato debe ser jpeg,jpg,png,gif con dimenciones cuadradas');
           //$this->form_validation->set_message('_validacion_banner','El formato debe ser jpeg,jpg,png,gif con dimenciones de almenos 620*400');             
           $this->form_validation->set_message('valid_email','Correo no valido');
           $this->form_validation->set_message('required','%s es requerido'); 
           //$this->form_validation->set_message('_validacion_clasificacion','debe seleccionar al menos una clasificacion');
           $this->form_validation->set_message('max_length','El maximo de caracteres son 100');
           $this->form_validation->set_message('_validacion_captcha','El Codigo ingresado es Invalido');
      
           if ($this->form_validation->run()==FALSE) {            
             $msg = '<script type="text/javascript" language="text/javascript">setTimeout(function(){$.fancybox($("#registrar_empresa_box"));},1);</script>';
             $this->directorio(0,$msg);
           }else{
               $usuario=$this->session->userdata('nombre_user_red');
               $rutaServidor="images/directorio";
               $rutaTemporal= $_FILES["logotipo"]["tmp_name"];
               $nombre= date("Y-m-d H:i:s").$usuario.".jpeg";
               $rutaDestino= $rutaServidor.'/'.$nombre;
               move_uploaded_file($rutaTemporal, $rutaDestino) ;


               $rutaServidor2="images/directorio/banners";
               $rutaTemporal2= $_FILES["banner"]["tmp_name"];
               $nombre2= date("Y-m-d H:i:s").$usuario.".jpeg";
               $rutaDestino2= $rutaServidor2.'/'.$nombre2;
               move_uploaded_file($rutaTemporal2, $rutaDestino2) ;
               
               $datos=array(
                'id' => $this->session->userdata('id_user_red'),   
                'nombre_empresa' => $this->input->post('nombre_empresa'),
                'descripcion' => $this->input->post('descripcion'),
                'persona_contacto' => $this->input->post('persona_contacto'),
                'telefono' => $this->input->post('telefono'),
                'email' => $this->input->post('email'),
                'pais' => $this->input->post('pais'),
                'ciudad' => $this->input->post('ciudad'),
                'pagina_web' => $this->input->post('pagina_web'),
                'logotipo' => $rutaDestino,
                //'clasificacion' => $this->input->post('clasificacion'),
                'banner' => $rutaDestino2,
               );
               $this->model_servicios->ingresa_directorio($datos);

               //if($clasificacion !=""){
               foreach ( $clasificacion as $value) {
                 $this->model_servicios->insert_clasificacion($value); 
               }
                    echo "<script type='text/javascript'>
                    alert('\"Su informacion sera parte del Directorio en menos de 12 horas, si cumple con los parametros legales permitidos\"');
                    window.location='../servicios/directorio';
                    </script>";
              }
            //}
    }else{        
      redirect('servicios/directorio');
    }   
    }

    /*-----------------------------
    FUNCION PARA BUSCAR EN DIRECTORIO*/
    public function buscador_directorio(){
      if ($_POST AND $_POST['buscador_directorio']!="") {
          $directorio=$this->model_servicios->buscador_directorio($this->input->post('buscador_directorio'));
          $img_pautas=$this->model_index->imagen_pauta();
          $img_pautas1=$this->model_index->imagen_pauta();
          $img_pagina=$this->model_index->imagen_pagina2();
          $data=array(
            'img_pauta'=>$img_pautas,
            'img_pauta1'=>$img_pautas1,
            'img_captcha' => $this->img_captcha(),
            'img_pagina'=>$img_pagina,
            'msg'     => '',
            'directorio' =>$directorio,
            'view'    => "servicios/directorio",
          );  
          $this->load->view('includes/template',$data);
      }else{
          redirect('servicios/directorio');  
      }
    }

    /*-----------------------------
    FUNCION PARA MOSTRAR BANNER DE DIRECTORIO*/
    public function directorio_banner($id){
      $img_pautas=$this->model_index->imagen_pauta();
      $img_pautas1=$this->model_index->imagen_pauta();
      $directorio=$this->model_servicios->select_directorio($id);
      $paises = $this->model_registro->Mostrar_paises();
      $img_pautas=$this->model_index->imagen_pauta();  
      $data=array(
        'directorio' => $directorio,
        'view'    => "servicios/mas_directorio",
        'img_pauta' => $img_pautas,
      );  
      $this->load->view('includes/template',$data);
    }    
}//FINAL DE LA CLASE
