<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MiPerfil extends CI_Controller {

  public function __construct ()  {       
      parent :: __construct (); 
      $this->load->model('model_miperfil');
      $this->_logeo_in();
  }

  function _logeo_in(){
      $login_in = $this->session->userdata('login_red');
      if ($login_in !=true){
        redirect ('index');
      }
    }
  public function index($msg = ""){
    $data['msg'] =  $msg;
    $data['view']="miPerfil/index";
    $this->load->view('includes/template',$data);
  } 
  public function confirmacionEliminacio(){
    $data['msg']='';
    $data['view']='miPerfil/confirmacionEliminacion';
    $this->load->view('includes/template',$data);
  } 
  public function eliminar(){
    $id=$this->session->userdata('id_user_red');
    $eliminar=$this->model_miperfil->eliminar($id);
    $this->session->sess_destroy();
    redirect('index');
  } 
  public function cambio_contrasena(){
    $data['msg']='';
    $data['view']='miPerfil/cambio_contrasena';
    $this->load->view('includes/template',$data);
  }
  public function actualizacion_contrasena(){
      $this->form_validation->set_rules('pass_actual','Contraseña Actual','required|callback__verificar_pass');
      $this->form_validation->set_rules('pass_nueva','Contraseña Nueva','required');
      $this->form_validation->set_rules('pass_repite','Repite Contraseña','required|matches[pass_nueva]|md5');
      $this->form_validation->set_message('required','%s es requerido');
      $this->form_validation->set_message('matches','Las contraseñas no coinciden');
      $this->form_validation->set_message('_verificar_pass','La Contraseña Actual es incorrecta');
      if ($this->form_validation->run()==FALSE) {
        $this->cambio_contrasena();
      }else{
        $datos=array(
          'nueva_contrasena' => $this->input->post('pass_repite'),
          'id' => $this->session->userdata('id_user_red'),
        );
        $this->model_miperfil->actualizar_contrasena($datos);
        redirect('miPerfil');
      }
  }
  /*--------------------------------------
  FUNCION VERIFICAR SI YA EXISTE EL CORREO*/    
  public function _verificar_pass($correo){
   $id=$this->session->userdata('id_user_red');
   $correo_md5=md5($correo);
   return $this->model_miperfil->contrasena_verificacion($id,$correo_md5);
  }
  public function editar(){
    $id=$this->session->userdata('id_user_red');
    $usuarios=$this->model_miperfil->usuarios($id);
    $paises = $this->model_miperfil->Mostrar_paises();
    $sectores = $this->model_miperfil->Mostrar_sectores(); 
    $data=array(
      'msg1' => '',
      'msg' => '',
      'usuarios' => $usuarios,
      'view' => 'miPerfil/editar_datos',
      'paises' => $paises,
      'sectores' => $sectores,
    );
    $this->load->view('includes/template',$data);
  }

  public function updateUser(){
    $this->form_validation->set_rules('correo','correo','required|valid_email|callback__verificarcorreo');
    $this->form_validation->set_rules('telefono','teléfono','required|numeric');
    $this->form_validation->set_rules('pais','país','required');
    $this->form_validation->set_rules('ciudad','ciudad','required');
    $this->form_validation->set_rules('profesion','profesión','required');
    $this->form_validation->set_message('required','%s es requerido');
    $this->form_validation->set_message('valid_email','Correo no valido');
    $this->form_validation->set_message('matches','Las contraseñas no coinciden');
    $this->form_validation->set_message('numeric','%s debe ser numerico');
    $this->form_validation->set_message('_verificarcorreo','El correo ya existe');
    if ($this->form_validation->run()==FALSE) {
      $this->editar();
    }else{
      $id=$this->session->userdata('id_user_red');
     $datos=array(
             'id' => $id,
             'correo' => $this->input->post('correo'),
             'telefono' => $this->input->post('telefono'),
             'pais' => $this->input->post('pais'),
             'ciudad' => $this->input->post('ciudad'),
             'profesion' => $this->input->post('profesion'),
             'empresa' => $this->input->post('empresa'),
             'sector' => $this->input->post('sector'),
             'cargo' => $this->input->post('cargo'),
      );
     $this->model_miperfil->editar($datos);
      $id=$this->session->userdata('id_user_red');
    $usuarios=$this->model_miperfil->usuarios($id);
    $paises = $this->model_miperfil->Mostrar_paises();
    $sectores = $this->model_miperfil->Mostrar_sectores(); 
    $data['usuarios']=$usuarios;
    $data['paises']=$paises;
    $data['sectores']=$sectores;
    $data['msg']='';
    $data['msg1']='
    <div class="alert-box success">
       Actualizacion Exitosa!!
       <a href="" class="close">&times;</a>
     </div>';
    $data['view']="miPerfil/editar_datos";
    $this->load->view('includes/template',$data);
    }
  }
  /*-----------------------------------
  FUNCION PARA MOSTRAR LOS CLASIFICADOS*/ 
  public function clasificados($msg=""){
      $idusuario=$this->session->userdata('id_user_red');
      if ($msg==1){ $msg='<div class="alert-box success">Clasificado Activado<a href="" class="close">&times;</a></div>';}    
      if ($msg==2){ $msg='<div class="alert-box alert">Clasificado Eliminada<a href="" class="close">&times;</a></div>';}
      if ($msg==3){ $msg='<div class="alert-box success">Clasificado Actualizado<a href="" class="close">&times;</a></div>';}
      $clasificado_pagado=$this->model_miperfil->clasificados($idusuario);
      $clasificado_gratis=$this->model_miperfil->clasificados1($idusuario);
      $data=array(
        'msg1'     => $msg,
        'clasificado_gratis' =>$clasificado_gratis,
        'clasificado_pagado' => $clasificado_pagado,
        'view'    => "miPerfil/clasificados",
      );  
      $this->load->view('includes/template',$data);
  }   
}