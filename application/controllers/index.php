<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller {

	public function __construct ()
	{   		
	    parent :: __construct (); 
	    $this->load->model('model_index');
      $this->load->model('model_admin');       
	}
    /*-----------------------------------
    FUNCION QUE INICIALIZARA POR DEFECTO*/
	public function index($ms1=''){  
    $order='order by rand()';
    $miniBanner=$this->model_index->miniBanner($order); 
		$noticias=$this->model_index->noticiaIndex();
		$ofertas=$this->model_index->ofertas();
    $preguntas=$this->model_index->mostrarpregunta();
    $respuestas=$this->model_index->mostrarrespuestasid($preguntas[0]->id);
    /*******valida la ip*///////
           if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
           $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
            elseif (isset($_SERVER['HTTP_VIA'])) {
               $ip = $_SERVER['HTTP_VIA'];
            }
            elseif (isset($_SERVER['REMOTE_ADDR'])) {
               $ip = $_SERVER['REMOTE_ADDR'];
            }
            else {
               $ip = "unknown";
            }
      $img_pagina=$this->model_index->imagen_pagina();
      $valido=$this->model_index->validaIp($ip);
      $img_home=$this->model_index->imagen_home();
      $img_pautas=$this->model_index->imagen_pauta();
     if($valido==true){
        $data=array(
        'img_pagina'=>$img_pagina,
        'miniBanner'=>$miniBanner,
        'img_pauta'=>$img_pautas,
        'img_home'=>$img_home,
        'preguntas'=>$preguntas,
        'respuestas'=>$respuestas,
        'msg1'     =>  'Gracias por tu voto',
        'ofertas' => $ofertas,
        'noticia' => $noticias,
        'view'    => "index/index",
        );
     }else{
       $data=array(
        'img_pagina'=>$img_pagina,
        'miniBanner'=>$miniBanner,
        'img_pauta'=>$img_pautas,
        'img_home'=>$img_home,
        'preguntas'=>$preguntas,
        'respuestas'=>$respuestas,
        'msg1'     =>  '',
        'ofertas' => $ofertas,
        'noticia' => $noticias,
        'view'    => "index/index",
        );
     }       

		$this->load->view('includes/template',$data);
	}
    /*----------------------------------------------
    FUNCION QUE MOSTRAR UNA CANTIDAD DE CARANTERES*/	
	public static function corta_palabra($palabra,$num)
	{
		$largo=strlen($palabra);//indicarme el largo de una cadena
		$cadena=substr($palabra,0,$num);
		return $cadena;
	}
    /*-----------------------------------------
    FUNCION PARA MOSTRAR UNA NOTICIA CON SU ID*/
    public function noticia($id){
    $noticia=$this->model_index->mostrarNoticia($id);
    $img_pautas=$this->model_index->imagen_pauta();
    $data=array( 
    'noticia' => $noticia,
    'view'    => "noticias/noticia",
    'img_pauta' => $img_pautas,
    );
    $this->load->view('includes/template',$data);
    }	
    /*---------------------------------------
    FUNCION PARA MOSTRAR LA VISTA DE REVISTA*/    
    public function revista(){
      $consulta='order by id DESC';
      $revistas=$this->model_index->revistas($consulta);
      $definida=$this->model_index->revistasdefinida();
      $data= array(
        'definida'=>$definida,
        'revistas'=>$revistas,
        'view' => 'index/revista', 
        );
      $this->load->view('includes/template',$data);
    }  
    /*---------------------------------------
    FUNCION PARA MOSTRAR LA VISTA DE NOTICIAS*/    
    public function noticias(){
      $img_pagina=$this->model_index->imagen_pagina();
      $noticias=$this->model_index->noticia();
      $img_pautas=$this->model_index->imagen_pauta();
      $data= array( 
        'img_pagina'=>$img_pagina,
        'view'    => 'noticias/index', 
        'noticia' => $noticias,
        'img_pauta'=>$img_pautas,
      );
      $this->load->view('includes/template',$data);
    }        
    /*-------------------------------------------
    FUNCION PARA MOSTRAR LA VISTA DE CONTACTENOS*/    
    public function contactenos(){
      $img_pagina=$this->model_index->imagen_pagina();
      $img_pautas=$this->model_index->imagen_pauta();
      $data= array(
        'img_pagina'=>$img_pagina,
        'view' => 'index/contactenos',
        'img_pauta'     => $img_pautas,
        );
      $this->load->view('includes/template',$data);
    }   
    /*---------------------------------------------
    FUNCION PARA RECIBIR EL MENSAJES DEL CONTACTENOS*/ 
    public function contacto_mensaje(){
      $this->form_validation->set_rules('name_contact','Nombre','required');
      $this->form_validation->set_rules('email_contact','Correo','required|valid_email');
      $this->form_validation->set_rules('asunto_contact','Asunto','required');
      $this->form_validation->set_rules('message_contact','Mensaje','required');
      $this->form_validation->set_message('required','%s es requerido');
      $this->form_validation->set_message('valid_email','Correo no valido');
      if ($this->form_validation->run()==FALSE) {
        $this->contactenos();
      }else{
       $data= array(
              'correo_contacto' => $this->input->post('email_contact'), 
              'nombre_contacto' => $this->input->post('name_contact'),
              'asunto_contacto' => $this->input->post('asunto_contact'),
              'message_contacto' => $this->input->post('message_contact'),
              );
       $this->contacto_email($data);
       redirect('index/contactenos');
      }
    }
    /*---------------------------------------------
    FUNCION PARA ENVIAR LOS MENSAJES DE CONTACTENOS*/     
    public function contacto_email($data){
      $fecha=date("d-m-Y");
      $hora=date("H:m:s");            
      $correo='nelidaforero@reddegh.com'; 
      $remitente= "'".$data['nombre_contacto']."'"."<".$data['correo_contacto'].">";
      $asunto= $data['asunto_contacto'];
      $cuerpo= $data['message_contacto'];
      $sheader="From:".$remitente."\nReply-To:".$remitente."\n";
      $sheader=$sheader."Mime-Version: 1.0\n";
      $sheader=$sheader."Content-Type: text/html";
      mail($correo,$asunto,$cuerpo,$sheader);     
    } 

    public function encuentas(){

    $idrespuesta=$this->input->post('radio');
     if ($idrespuesta==''){

            $this->index();
     }else{
        $idpregunta=$this->input->post('idpregunta');
        $data=array(
            'idpregunta'=>$idpregunta,
            'idrespuesta'=>$idrespuesta,
          );
          $this->model_index->encuesta($data);
          /*************
          valida la ip*/
           if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
           $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
            elseif (isset($_SERVER['HTTP_VIA'])) {
               $ip = $_SERVER['HTTP_VIA'];
            }
            elseif (isset($_SERVER['REMOTE_ADDR'])) {
               $ip = $_SERVER['REMOTE_ADDR'];
            }
            else {
               $ip = "unknown";
            }
          $this->model_index->inserIp($ip);
          redirect('index');
      }
    }  

    public function ip(){
      $ip = $_SERVER['SERVER_ADDR'];
      echo 'ip'.$ip.'</br>';
    }

    public function miniBanner(){
      $order='order by rand()';
      $miniBanner=$this->model_index->miniBanner($order); 
      $data['minibanner']=$miniBanner;
      echo json_encode($data);

    }  
}