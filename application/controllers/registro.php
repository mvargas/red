<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registro extends CI_Controller {

	public function __construct ()
	{   		
	    parent :: __construct (); 
	    $this->load->model('model_registro');  
      $this->load->model('model_index'); 
	}
    /*-----------------------------------
    FUNCION QUE INICIALIZARA POR DEFECTO*/
	public function index(){
        if ($this->session->userdata('id_user_red')) {
            redirect('index');
        }else{
        $img_pagina=$this->model_index->imagen_pagina();
        $sectores=$this->model_registro->Mostrar_sectores();
        $paises = $this->model_registro->Mostrar_paises();           
          $data=array(
          'img_pagina'=>$img_pagina,
          'view'     => "registro/index",
          'sectores' => $sectores,
          'paises'   => $paises,
          );
    		$this->load->view('includes/template',$data);
        }
	}
    /*-----------------------------------
    FUNCION REGISTRAR UN USUARIO EN LA DB*/	
	public function insertar(){
		$this->form_validation->set_rules('nombre','nombre','required');
		$this->form_validation->set_rules('apellido','apellido','required');
		$this->form_validation->set_rules('cedula','cédula','required|numeric|callback__verificarcedula');
		$this->form_validation->set_rules('correo','correo','required|valid_email|callback__verificarcorreo');
		$this->form_validation->set_rules('telefono','teléfono','required');
		$this->form_validation->set_rules('pais','país','required');
    $this->form_validation->set_rules('sector','sector','required');
    $this->form_validation->set_rules('empresa','Empresa','required');
    $this->form_validation->set_rules('cargo','Cargo','required');
		//$this->form_validation->set_rules('ciudad','ciudad','required');
		$this->form_validation->set_rules('profesion','profesión','required');
		$this->form_validation->set_rules('contrasena','contraseña','required');
		$this->form_validation->set_rules('repit_contrasena','repetir contraseña','required|matches[contrasena]|md5');
		$this->form_validation->set_message('required','%s es requerido');
		$this->form_validation->set_message('valid_email','Correo no valido');
		$this->form_validation->set_message('matches','Las contraseñas no coinciden');
		$this->form_validation->set_message('numeric','%s debe ser numerico');
		$this->form_validation->set_message('_verificarcorreo','El correo ya existe');
    $this->form_validation->set_message('_verificarcedula','La cedula ya existe');
		if ($this->form_validation->run()==FALSE) {
			$this->index();
		}else{
		 $datos=array(
             'nombre' => $this->input->post('nombre'),
             'apellido' => $this->input->post('apellido'),
             'cedula' => $this->input->post('cedula'),
             'correo' => $this->input->post('correo'),
             'telefono' => $this->input->post('telefono'),
             'pais' => $this->input->post('pais'),
             'ciudad' => $this->input->post('ciudad'),
             'profesion' => $this->input->post('profesion'),
             'empresa' => $this->input->post('empresa'),
             'sector' => $this->input->post('sector'),
             'cargo' => $this->input->post('cargo'),
             'contrasena' => $this->input->post('repit_contrasena'),
		 	);
		 $this->model_registro->registro($datos);
     echo "<script type='text/javascript'>
           alert('\"Todos los datos han sido registrados correctamente\"');
           window.location='../index';
           </script>";
         $validacion=$this->model_registro->verificacion_login($datos);
            $session = array(
             'login_red' => TRUE,
             'id_user_red' =>$validacion[0]->id,
             'nombre_user_red' =>$validacion[0]->nombre,
             'apellido_user_red' =>$validacion[0]->apellido,
             'tipo_user_red' =>$validacion[0]->idtipoUsuario
            );
            $this->session->set_userdata($session);    
		 redirect('index');
		}
	}
    /*--------------------------------------
    FUNCION VERIFICAR SI YA EXISTE EL CORREO*/		
	public function _verificarcorreo($correo){
	return $this->model_registro->verificacion_correo($correo);
    }
   // FUNCION VERIFICAR cedual ya existe*/    
  public function _verificarcedula($cedula){
  return $this->model_registro->verificacion_cedula($cedula);
    }
    /*-----------------------------------
    FUNCION INICIAR SESSION DEL USUARIO */
    public function login(){
    if (isset($_POST['correo_login']) AND isset($_POST['pass_login'])) {
    	$datos=array(
         'correo'     => $this->input->post('correo_login'),
         'contrasena' => md5($this->input->post('pass_login')),
    	);
    	$validacion=$this->model_registro->verificacion_login($datos);
    	if ($validacion != FALSE) {
    		$session = array(
    		 'login_red' => TRUE,
    		 'id_user_red' =>$validacion[0]->id,
             'nombre_user_red' =>$validacion[0]->nombre,
             'apellido_user_red' =>$validacion[0]->apellido,
             'tipo_user_red' =>$validacion[0]->idtipoUsuario
    		);
    		$this->session->set_userdata($session);
    		$data['validacion']="bien";
    	}else{
            $data['validacion']="error";
    	}
    	echo json_encode($data);
    }else{
    	redirect('index');
    }
    } 
    /*----------------------------------------------------
    FUNCION INICIAR SESSION DEL USUARIO AL APLICAR OFERTA*/
    public function login_oferta(){
    if (isset($_POST['correo_login']) AND isset($_POST['pass_login'])) {
        $datos=array(
         'correo'     => $this->input->post('correo_login'),
         'contrasena' => md5($this->input->post('pass_login')),
        );
        $validacion=$this->model_registro->verificacion_login($datos);
        if ($validacion != FALSE) {
            $session = array(
             'login_red' => TRUE,
             'id_user_red' =>$validacion[0]->id,
             'nombre_user_red' =>$validacion[0]->nombre,
             'apellido_user_red' =>$validacion[0]->apellido,
             'tipo_user_red' =>$validacion[0]->idtipoUsuario
            );
            $this->session->set_userdata($session);
            $img_captcha = $this->img_captcha();
            $data['formulario']='<div id="aplicar_oferta_box" class="fancy_skin_redgh" style="width:650px;">
              <h2>Aplicar a esta oferta</h2>
              <div class="row">
                '.form_open_multipart('ofertas/aplicar_oferta','id="aplicar_form"').'
                    <div class="columns twelve">
                      <label for="razon_oferta">Razones por las cuales cree que usted cumple con el perfil de la oferta:</label>
                      <textarea type="text" id="razon_oferta" name="razon_oferta" cols="30" rows="10" value="'.set_value('razon_oferta').'"></textarea>
                      <p class="error_img">'.form_error('razon_oferta').'
                      <input name="id_oferta" type="hidden" id="idd_oferta" value="'.$_POST['id_oferta'].'">
                    </div>
                    <div class="clr"></div>
                    <div class="columns twelve">
                      <p>Escribe los caracteres que ves en la imágen de abajo. </p>
                    </div>
                    <div class="columns two">
                      <img src="data:image/png;base64,'.$img_captcha.'">
                    </div>
                    <div class="columns three">
                      <input type="text" name="captcha_aplicar_oferta">
                      '.form_error('captcha_aplicar_oferta').'
                    </div>
                    <div class="columns twelve">
                      <label for="adjuntar_oferta">Adjuntar Hoja de Vida:</label>
                      <input type="file" id="adjuntar_oferta" name="hoja_oferta" >
                      <p class="error_img">'.form_error('hoja_oferta').'
                      <p>Anexar su HV en formato Word y pdf únicamente. </p>
                    </div>
                    <div class="clr"></div>
                    <div class="align_center submit_content">
                      <input type="submit" class="button gray" value="Aplicar">
                    </div>
                    '.form_close().'
              </div>
            </div>';
            $data['validacion']="bien";
        }else{
            $data['validacion']="error";
        }
        echo json_encode($data);
    }else{
        redirect('index');
    }
    }     
    /*--------------------------------
    FUNCION CERAR SESSION DEL USUARIO */     
    public function loginout(){   
     $this->session->sess_destroy();
     redirect('index');
    }
    /*-----------------------------------
    FUNCION PARA RECUPERAR LA CONTRASEÑA */     
    public function recuperar_contrasena(){
      if ($_POST) {
          $this->form_validation->set_rules('correo_contrasena','correo','callback__verificarcorreo');
          if ($this->form_validation->run()==TRUE) {
            $data['confirmacion']=false;
          }else{

            $aleatorio = $this->aleatorio_String(7,TRUE,TRUE,TRUE);
            $datos=array(
                'nueva_contrasena'=>md5($aleatorio),
                'correo_user'     =>$_POST['correo_contrasena'],
            );

            $datos1=array(
                'nueva_contrasena'=>$aleatorio,
                'correo_user'     =>$_POST['correo_contrasena'],
            );

            $this->model_registro->Actualizar_contrasena($datos);
            $this->contrasena_email($datos1);
            $data['confirmacion']="<div class='fancy_skin_redgh'><h2>Se ha enviado tu nueva contraseña al correo electrónico, recuerda revisar tu bandeja de spam</h2></div>";
          }          
          echo json_encode($data);
      }
    } 
    /*----------------------------------------
    FUNCION PARA GENERAR UN NUEMERO ALEATORIO*/ 
    public function aleatorio_String($length,$uc=TRUE,$n=TRUE,$sc=FALSE){
    $source = 'abcdefghijklmnopqrstuvwxyz';
    if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      if($n==1) $source .= '1234567890';
        // if($sc==1) $source .= '|@#~$%()=^*+[]{}-_';
        if($length>0){
        $rstr = "";
        $source = str_split($source,1);
        for($i=1; $i<=$length; $i++){
          mt_srand((double)microtime() * 1000000);
          $num = mt_rand(1,count($source));
          $rstr .= $source[$num-1];
        }
      }
      return $rstr;
    }
    /*----------------------------------------
    FUNCION PARA IMAGEN CON CODIGO DE CAPTCHA*/     
    public function img_captcha(){
      $numero= $this->aleatorio_String(7,TRUE,TRUE,TRUE); 
      $imagen_p =   imagecreate(100, 50);
      $fondo = imagecolorallocate($imagen_p, 250, 14, 100);
      $color_texto = imagecolorallocate($imagen_p, 255, 255, 255);
      imagestring($imagen_p, 20, 15, 15, $numero, $color_texto);
                ob_start (); 
            imagepng($imagen_p);
            $image_data = ob_get_contents (); 
            ob_end_clean (); 
            $image_data_base64 = base64_encode ($image_data);

             $captcha = array(
             'numero_captcha' => $numero
            );
            $this->session->set_userdata($captcha);
            return $image_data_base64;
    } 
    /*------------------------------------------------------
    FUNCION PARA ENVIAR LOS MENSAJES DE CAMBIO DE CONTRASEÑA*/     
    public function contrasena_email($data){
      $fecha=date("d-m-Y");
      $hora=date("H:m:s");            
      $correo=$data['correo_user']; 
      $remitente= "nelidaforero@reddegh.com";
      $asunto= "Cambio de contraseña";
      $cuerpo= "Su nueva contraseña es la siguiente: ".$data['nueva_contrasena'];
      $sheader="From:".$remitente."\nReply-To:".$remitente."\n";
      $sheader=$sheader."Mime-Version: 1.0\n";
      $sheader=$sheader."Content-Type: text/html";
      mail($correo,$asunto,$cuerpo,$sheader);     
    }  
    /*--------------------------------------------------
    FUNCION PARA MOSTRAR DEPARTAMENTOS POR MEDIO DE AJAX*/     
    public function departamentos(){
    if ($_POST) {
       $option= "<option values='1'>1</option><option values='2'>2</option>";
       echo json_encode($option);
     } 
    }        
}	