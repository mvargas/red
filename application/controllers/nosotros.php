<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nosotros extends CI_Controller {

	public function __construct ()
	{   		
	    parent :: __construct (); 
	    $this->load->model('model_nosotros');
	    $this->load->model('model_index');       
	}
    /*-----------------------------------
    FUNCION QUE INICIALIZARA POR DEFECTO*/
	public function index($msg = ""){
	  $img_pagina=$this->model_index->imagen_pagina();
	  $nosotros=$this->model_nosotros->nosotros();
      $data= array(
      	'img_pagina'=>$img_pagina,
        'view'    => 'nosotros/index', 
        'nosotros' => $nosotros
      );
    	$this->load->view('includes/template',$data);
	}
}	