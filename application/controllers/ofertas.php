<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ofertas extends CI_Controller {

	public function __construct ()
	{   		
	    parent :: __construct (); 
	    $this->load->model('model_ofertas');
      $this->load->model('model_index');       
	}
    /*-----------------------------------
    FUNCION QUE INICIALIZARA POR DEFECTO*/
	public function index($msg = ""){
      $img_pagina=$this->model_index->imagen_pagina();
      $ofertas=$this->model_ofertas->ofertas();
      $img_pautas=$this->model_index->imagen_pauta();
      $img_pautas1=$this->model_index->imagen_pauta();
      if($ofertas==FALSE){
        $data=array(
        'img_pagina'=>$img_pagina,
        'img_pauta'=>$img_pautas,
        'img_pauta1'=>$img_pautas1,
        'img_captcha' => '',
        'msg'     => 'No hay ofertas',
        'ofertas' => '',
        'view'    => "ofertas/sinOfertas",
      ); 
      }
      $data=array(
        'img_pagina'=>$img_pagina,
        'img_pauta'=>$img_pautas,
        'img_pauta1'=>$img_pautas1,
        'img_captcha' => $this->img_captcha(),
        'msg'     => $msg,
        'ofertas' => $ofertas,
        'view'    => "ofertas/index",
      );  
    	$this->load->view('includes/template',$data);
	}
    /*------------------------------------
    FUNCION PARA MOSTRAR OFERTAS POR SU ID*/
    public function oferta($id,$msg=""){
    $oferta=$this->model_ofertas->mostrarOferta($id);
    $data=array(
    'view'   => "ofertas/oferta",
    'oferta' => $oferta,
    'msg'  => $msg,
    );
    $this->load->view('includes/template',$data);
    }	
    /*----------------------------------------
    FUNCION PARA INSERTAR OFERTAS DEL USUARIO*/    
    public function insertar_oferta(){
    if($this->input->post('confidencial')=='on'){
          if ($_POST) {
         $this->form_validation->set_rules('titulo_oferta','Titulo','required');
         $this->form_validation->set_rules('perfil_oferta','Perfil oferta','required');
         $this->form_validation->set_rules('salario_oferta','Salario','required');
         $this->form_validation->set_rules('tipo_oferta','Tipo oferta','required');
         $this->form_validation->set_rules('pais_oferta','País','required');
         $this->form_validation->set_rules('ciudad_oferta','Ciudad','required');
         $this->form_validation->set_rules('captcha_aplicar_oferta','Campo','required|callback__validacion_captcha');
         $this->form_validation->set_message('required','%s es requerido');
         $this->form_validation->set_message('_validacion_captcha','El Codigo ingresado es Invalido');
         if ($this->form_validation->run()==FALSE) {
          $msg = '<script type="text/javascript" language="text/javascript">setTimeout(function(){$.fancybox($("#crear_oferta_box"));},100);</script>';
           $this->index($msg);
         }else{
             //print_r($_FILES);
            $rutaDestino='images/ofertas_img/predeterminada.jpg' ;
             $datos=array(
              'id' => $this->session->userdata('id_user_red'),   
              'titulo' => $this->input->post('titulo_oferta'),
              'logo' => $rutaDestino,
              'perfil' => $this->input->post('perfil_oferta'),
              'salario' => $this->input->post('salario_oferta'),
              'contrato' => $this->input->post('tipo_oferta'),
              'pais' => $this->input->post('pais_oferta'),
              'ciudad' => $this->input->post('ciudad_oferta'),
              'fecha' => date("Y-m-d H:i:s"),
             );
             $this->model_ofertas->insertarOferta($datos);
             //redirect('ofertas');
             echo "<script type='text/javascript'>
                  alert('\"Su oferta estara en linea en menos de 12 horas, si cumple con los parametros legales permitidos\"');
                  window.location='ofertas';
                  </script>";
         }
      }else{
        redirect('ofertas');
      }
    }else{
        if ($_POST) {
         $this->form_validation->set_rules('titulo_oferta','Titulo','required');
         $this->form_validation->set_rules('perfil_oferta','Perfil oferta','required');
         $this->form_validation->set_rules('salario_oferta','Salario','required');
         $this->form_validation->set_rules('tipo_oferta','Tipo oferta','required');
         $this->form_validation->set_rules('pais_oferta','País','required');
         $this->form_validation->set_rules('ciudad_oferta','Ciudad','required');
         $this->form_validation->set_rules('captcha_aplicar_oferta','Campo','required|callback__validacion_captcha');
         $this->form_validation->set_rules('img_oferta','Imagen','callback__validacion_img');
         $this->form_validation->set_message('required','%s es requerido');
         $this->form_validation->set_message('_validacion_img','El formato de Archivo no esta permitido');
         $this->form_validation->set_message('_validacion_captcha','El Codigo ingresado es Invalido');
         if ($this->form_validation->run()==FALSE) {
           $msg = '<script type="text/javascript" language="text/javascript">setTimeout(function(){$.fancybox($("#crear_oferta_box"));},100);</script>';
           $this->index($msg);
         }else{
             //print_r($_FILES);
             $usuario=$this->session->userdata('nombre_user_red');
             $rutaServidor="images/ofertas_img";
             $rutaTemporal= $_FILES["img_oferta"]["tmp_name"];
             $nombre= date("Y-m-d H:i:s").$usuario.".jpeg";
             $rutaDestino= $rutaServidor.'/'.$nombre;
             move_uploaded_file($rutaTemporal, $rutaDestino) ;
             $datos=array(
              'id' => $this->session->userdata('id_user_red'),   
              'titulo' => $this->input->post('titulo_oferta'),
              'logo' => $rutaDestino,
              'perfil' => $this->input->post('perfil_oferta'),
              'salario' => $this->input->post('salario_oferta'),
              'contrato' => $this->input->post('tipo_oferta'),
              'pais' => $this->input->post('pais_oferta'),
              'ciudad' => $this->input->post('ciudad_oferta'),
              'fecha' => date("Y-m-d H:i:s"),
             );
             $this->model_ofertas->insertarOferta($datos); 
             /*$msg="<script type='text/javascript'>
                   $.fancybox('<h2>YA has referido una hoja de vida</h2>');
                   </script>";            
             redirect('ofertas/index/'.$msg);*/
             echo "<script type='text/javascript'>
                  alert('\"Su oferta estara en linea en menos de 12 horas, si cumple con los parametros legales permitidos\"');
                  window.location='ofertas';
                  </script>";
         }
      }else{
        
        redirect('ofertas');
      }

    }
    }
    /*-----------------------------------
    FUNCION PARA APLICAR A UNA  OFERTAS*/    
    public function aplicar_oferta(){
      if ($_POST) {
         $this->form_validation->set_rules('razon_oferta','Razones','required');
         $this->form_validation->set_rules('id_oferta','id_oferta','required');
         $this->form_validation->set_rules('captcha_aplicar_oferta','Campo','required|callback__validacion_captcha');
         $this->form_validation->set_rules('hoja_oferta','Hoja de vida','callback__validacion_hoja');
         $this->form_validation->set_message('required','%s es requerido');
         $this->form_validation->set_message('_validacion_hoja','El Formato debe ser doc,docx,pdf o a excedido el tamaño normal');
         $this->form_validation->set_message('_validacion_captcha','El Codigo ingresado es Invalido');
         if ($this->form_validation->run()==FALSE) {
           $msg = '<script type="text/javascript" language="text/javascript">setTimeout(function(){$.fancybox($("#aplicar_oferta_box"));},100);</script>';
           $this->index($msg);
         }else{
          //print_r($_FILES);
             $usuario=$this->session->userdata('id_user_red');
             $rutaServidor= "ofertas_hoja_vida";
             $rutaTemporal= $_FILES["hoja_oferta"]["tmp_name"];
               if($_FILES["hoja_oferta"]["type"]=="application/msword"){
                 $nombre= date("Y-m-d")."_".date("H:i:s").$usuario.".doc";
               }
               if($_FILES["hoja_oferta"]["type"]=="application/pdf"){
                 $nombre= date("Y-m-d")."_".date("H:i:s").$usuario.".pdf";
               }
               if($_FILES["hoja_oferta"]["type"]=="application/vnd.openxmlformats-officedocument.wordprocessingml.document"){
                 $nombre= date("Y-m-d")."_".date("H:i:s").$usuario.".docx";
               }             
             $rutaDestino= $rutaServidor.'/'.$nombre;
             move_uploaded_file($rutaTemporal, $rutaDestino) ;        
            $data=array(
              'idUsuario' => $this->session->userdata('id_user_red'),
              'razones'   => $this->input->post('razon_oferta'),
              'idOferta'  => $this->input->post('id_oferta'),
              'hoja_vida' => $rutaDestino,
            );
            $this->model_ofertas->aplicarOferta($data);
            $oferta=$this->model_ofertas->Datos_oferta($this->input->post('id_oferta'));
            $datos=array(
              'nombre' => $this->session->userdata('nombre_user_red'),
              'apellido' => $this->session->userdata('apellido_user_red'),
              'razones'   => $this->input->post('razon_oferta'),
              'hoja_vida' => $rutaDestino,
              'correo_oferta' =>$oferta[0]->email,
              'nombre_oferta' =>$oferta[0]->titulo,
            );
            $this->HV_email($datos);
            //redirect('ofertas');
            echo "<script type='text/javascript'>
                  alert('\"Gracias por aplicar a esta Oferta\"');
                  window.location='ofertas';
                  </script>";
         }
      }else{
        redirect('ofertas');
      }
    }
    /*------------------------------------
    FUNCION PARA REFERIR LAS HOJAS DE VIDA*/    
    public function referir_hv(){
      if ($_POST) {
        //print_r($_POST);
         $this->form_validation->set_rules('razones_referir','Razones','required');
         $this->form_validation->set_rules('captcha_razones_referir','Campo','required|callback__validacion_captcha');
         $this->form_validation->set_rules('hoja_oferta','Hoja de vida','callback__validacion_hoja');
         $this->form_validation->set_message('required','%s es requerido');
         $this->form_validation->set_message('_validacion_hoja','El Formato debe ser doc,docx,pdf o a excedido el tamaño normal');
         $this->form_validation->set_message('_validacion_captcha','El Codigo ingresado es Invalido');
         if ($this->form_validation->run()==FALSE) {
           $msg = '<script type="text/javascript" language="text/javascript">setTimeout(function(){$.fancybox($("#enviar_hdv_box"));},100);</script>';
           $this->index($msg);
         }else{
             $usuario=$this->session->userdata('id_user_red');
             $rutaServidor= "referir_hoja_vida";
             $rutaTemporal= $_FILES["hoja_oferta"]["tmp_name"];
               if($_FILES["hoja_oferta"]["type"]=="application/msword"){
                 $nombre= date("Y-m-d")."_".date("H:i:s").$usuario.".doc";
               }
               if($_FILES["hoja_oferta"]["type"]=="application/pdf"){
                 $nombre= date("Y-m-d")."_".date("H:i:s").$usuario.".pdf";
               }
               if($_FILES["hoja_oferta"]["type"]=="application/vnd.openxmlformats-officedocument.wordprocessingml.document"){
                 $nombre= date("Y-m-d")."_".date("H:i:s").$usuario.".docx";
               }             
             $rutaDestino= $rutaServidor.'/'.$nombre;
             move_uploaded_file($rutaTemporal, $rutaDestino) ;        
            $data=array(
              'idUsuario' => $this->session->userdata('id_user_red'),
              'razones'   => $this->input->post('razones_referir'),
              'hoja_vida' => $rutaDestino,
            );
            $this->model_ofertas->referirOferta($data);
            //redirect('ofertas');
            echo "<script type='text/javascript'>
                  alert('\"La hoja de vida ha sido referida\"');
                  window.location='ofertas';
                  </script>";
         }
      }else{
        redirect('ofertas');
      }
    }
    /*-----------------------------
    FUNCION PARA VALIDAR EL CAPTCHA*/ 
    public function _validacion_captcha($captcha){
     if ($this->session->userdata('numero_captcha') == $captcha)
      {
       return true;  
      }else{
        return false;
      }  
    }    
    /*------------------------------------
    FUNCION PARA VALIDAR EL TIPO DE IMAGEN*/ 
    public function _validacion_img($FILES){
     if ($_FILES["img_oferta"]["type"]=="image/jpeg" || $_FILES["img_oferta"]["type"]=="image/pjpeg" || $_FILES["img_oferta"]["type"]=="image/gif" || $_FILES["img_oferta"]["type"]=="image/bmp" || $_FILES["img_oferta"]["type"]=="image/png")
      {
       return true;  
      }else{
        return false;
      }  
    }  
    /*------------------------------------
    FUNCION PARA VALIDAR EL TIPO DE FORMATO*/ 
    public function _validacion_hoja($FILES){
     if ($_FILES["hoja_oferta"]["type"]=="application/msword" || $_FILES["hoja_oferta"]["type"]=="application/pdf" || $_FILES["hoja_oferta"]["type"]=="application/vnd.openxmlformats-officedocument.wordprocessingml.document" AND $_FILES["hoja_oferta"]["size"] < 1800000)
      {
       return true;  
      }else{
        return false;
      }  
    } 
    /*----------------------------------------
    FUNCION PARA GENERAR UN NUEMERO ALEATORIO*/ 
    public function Captchar_String($length,$uc=TRUE,$n=TRUE,$sc=FALSE){
    $source = 'abcdefghijklmnopqrstuvwxyz';
    if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      if($n==1) $source .= '1234567890';
        // if($sc==1) $source .= '|@#~$%()=^*+[]{}-_';
        if($length>0){
        $rstr = "";
        $source = str_split($source,1);
        for($i=1; $i<=$length; $i++){
          mt_srand((double)microtime() * 1000000);
          $num = mt_rand(1,count($source));
          $rstr .= $source[$num-1];
        }
      }
      return $rstr;
    } 
    /*----------------------------------------
    FUNCION PARA IMAGEN CON CODIGO DE CAPTCHA*/     
    public function img_captcha(){
      $numero= $this->Captchar_String(7,TRUE,TRUE,TRUE); 
      $imagen_p =   imagecreate(100, 50);
      $fondo = imagecolorallocate($imagen_p, 250, 14, 100);
      $color_texto = imagecolorallocate($imagen_p, 255, 255, 255);
      imagestring($imagen_p, 20, 15, 15, $numero, $color_texto);
                ob_start (); 
            imagepng($imagen_p);
            $image_data = ob_get_contents (); 
            ob_end_clean (); 
            $image_data_base64 = base64_encode ($image_data);

             $captcha = array(
             'numero_captcha' => $numero
            );
            $this->session->set_userdata($captcha);
            return $image_data_base64;
    } 
    public function buscador(){

      $this->form_validation->set_rules('palabra','palabra','required');
      $this->form_validation->set_message('required','la %s es requerido');
      if ($this->form_validation->run()==FALSE) {
           $this->index();
      }else{
        $palabra=$this->input->post('palabra');
        $ofertas=$this->model_ofertas->buscar($palabra);
        if ($ofertas==FALSE){
          redirect('ofertas');
        }
        $img_pagina=$this->model_index->imagen_pagina();
        $img_pautas=$this->model_index->imagen_pauta();
        $img_pautas1=$this->model_index->imagen_pauta();
        $msg='';
        $data=array(
        'img_pagina' =>$img_pagina,
        'img_pauta'=>$img_pautas,
        'img_pauta1'=>$img_pautas1,
        'img_captcha' => $this->img_captcha(),
        'msg'     => $msg,
        'ofertas' => $ofertas,
        'view'    => "ofertas/index");  
        $this->load->view('includes/template',$data);
      }
      

    } 
    /*------------------------------------------------------
    FUNCION PARA ENVIAR LOS MENSAJES ADJUNTANDO HOJA DE VIDA*/     
    public function HV_email($data){
      $fecha=date("d-m-Y");
      $hora=date("H:m:s");            
      $correo=$data['correo_oferta']; 
      $remitente= "nelidaforero@reddegh.com";
      $asunto= "Aplicacion de oferta";
      $cuerpo= "El usuario <strong>".$data['nombre']." ".$data['apellido']."</strong> ha aplicado en tu oferta de empleo ".$data['nombre_oferta']."
       <br>Puede ver su hoja de vida en el siguiente link  ".base_url().$data['hoja_vida']."
       <br>Las razones por la cuales el candidato considera su hoja de vida para este perfil son:
       <br>".$data['razones']."";
      $sheader="From:".$remitente."\nReply-To:".$remitente."\n";
      $sheader=$sheader."Mime-Version: 1.0\n";
      $sheader=$sheader."Content-Type: text/html";
      mail($correo,$asunto,$cuerpo,$sheader);  
    } 
    /*---------------------------------------------------
    FUNCION PARA VER LAS HOJAS REFERIDAS DE LOS USUARIOS*/     
    public function referidas($msg=""){ 
      $hv_referidas=$this->model_ofertas->hoja_vida_referida();
        $data=array(
        'msg'     => $msg,
        'hojas_referidas' => $hv_referidas,
        'view'    => "ofertas/referidas");  
        $this->load->view('includes/template',$data);
    }       
}