<?php

class Upload extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('image_lib'); 
		$this->load->helper(array('form', 'url'));
	}

	function index()
	{
		$this->load->view('upload_form', array('error' => ' ' ));
	}

	function do_upload()
	{
		$config['upload_path'] = './images/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		$this->load->library('upload', $config);


		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			$this->load->view('upload_form', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$this->load->view('upload_success', $data);
		}
	}
	 function mirar(){
	 	       // $uploaddata = $this->upload->data();
		/*$config['image_library'] = 'gd2';
		$config['source_image']	= '../images/pauta.jpg';
		$config['new_image	'] = '../images/';
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = false;
		$config['width']	 = 100;
		$config['height']	= 150;
		$this->load->library('image_lib', $config); 
		$this->image_lib->resize();*/
			$config['image_library'] = 'GD2';
			$config['source_image']	= '/images/pauta.jpg';
			$config['new_image']	= '/images/mypic_small.jpg';
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['width']	 = 75;
			$config['height']	= 50;
			$this->load->library('image_lib', $config);
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			/*$config['new_image']	= '/images/mypic2_small.jpg';
			$config['width']	 = 150;
			$config['height']	= 100;
			$this->image_lib->initialize($config);
			$this->image_lib->resize();*/
        
        if (!$this->image_lib->resize())
			{
			    echo $this->image_lib->display_errors();
			}else{ echo "bien";}
	 }
}
?>