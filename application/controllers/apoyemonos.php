<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apoyemonos extends CI_Controller {

	public function __construct ()
	{   		
	    parent :: __construct (); 
	    $this->load->model('model_index');
	    $this->load->model('model_apoyemonos'); 
	    $this->_logeo_in();      
	}
    /*-----------------------------------
    FUNCION QUE INICIALIZARA POR DEFECTO*/
	public function index($msg=""){
      $img_pagina=$this->model_index->imagen_pagina();
      $img_pautas=$this->model_index->imagen_pauta();
      $solicitudes=$this->model_apoyemonos->mostrar_solicitudes();      
      $data= array(
        'img_pagina'=>$img_pagina,
      	'msg' => $msg,
      	'solicitudes'=>$solicitudes,   
        'view' => 'apoyemonos/index',
        'img_pauta'        => $img_pautas,
        'img_captcha' => $this->img_captcha(), 
      );
      $this->load->view('includes/template',$data);		
	}
    /*-----------------------------------------
    FUNCION PARA VALIDAR LA SESSION DEL USUARIO*/ 	
	function _logeo_in(){
		$login_in = $this->session->userdata('login_red');
		if ($login_in !=true){
		redirect ('index');
		}
	}
    /*----------------------------------------
    FUNCION PARA GENERAR UN NUEMERO ALEATORIO*/ 
    public function Captchar_String($length,$uc=TRUE,$n=TRUE,$sc=FALSE){
    $source = 'abcdefghijklmnopqrstuvwxyz';
    if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      if($n==1) $source .= '1234567890';
        // if($sc==1) $source .= '|@#~$%()=^*+[]{}-_';
        if($length>0){
        $rstr = "";
        $source = str_split($source,1);
        for($i=1; $i<=$length; $i++){
          mt_srand((double)microtime() * 1000000);
          $num = mt_rand(1,count($source));
          $rstr .= $source[$num-1];
        }
      }
      return $rstr;
    } 
    /*----------------------------------------
    FUNCION PARA IMAGEN CON CODIGO DE CAPTCHA*/     
    public function img_captcha(){
      $numero= $this->Captchar_String(7,TRUE,TRUE,TRUE); 
      $imagen_p =   imagecreate(100, 50);
      $fondo = imagecolorallocate($imagen_p, 250, 14, 100);
      $color_texto = imagecolorallocate($imagen_p, 255, 255, 255);
      imagestring($imagen_p, 20, 15, 15, $numero, $color_texto);
                ob_start (); 
            imagepng($imagen_p);
            $image_data = ob_get_contents (); 
            ob_end_clean (); 
            $image_data_base64 = base64_encode ($image_data);

             $captcha = array(
             'numero_captcha' => $numero
            );
            $this->session->set_userdata($captcha);
            return $image_data_base64;
    } 
    /*---------------------------
    FUNCION CREAR UNA SOLICITUD*/ 
    public function crear_solicitud(){
     if($_POST){
	     $this->form_validation->set_rules('titulo','Titulo','required');
	     $this->form_validation->set_rules('solicitud','solicitud','required');
	     $this->form_validation->set_rules('captcha_crear_solicitud','captcha','required|callback__validacion_captcha');
         $this->form_validation->set_message('required','El Campo %s es requerido');
	     $this->form_validation->set_message('_validacion_captcha','El Codigo ingresado es Invalido');
	     if ($this->form_validation->run()==FALSE) {
	       $msg = '<script type="text/javascript" language="text/javascript">setTimeout(function(){$.fancybox($("#crear_solicitud_box"));},100);</script>';
	       $this->index($msg);
	     }else{
	       $data=array(
	       	'id_users' => $this->session->userdata('id_user_red'),
	       	'titulo' => $this->input->post('titulo'),
	       	'solicitud'=> $this->input->post('solicitud'),           
	       );
	       $this->model_apoyemonos->insert_solicitud($data);
           echo "<script type='text/javascript'>
           alert('\"Su Solicitud estara en linea en menos de 12 horas, si cumple con los parametros legales permitidos\"');
           window.location='../apoyemonos';
           </script>";
	     }
	 }else{
	 	redirect('apoyemonos');
	 }
    } 
    /*---------------------------
    FUNCION CREAR UNA SOLICITUD*/ 
    public function responder_solicitud(){
     if($_POST){
	     $this->form_validation->set_rules('respoder_solicitud','Respuesta solicitud','required');
	     $this->form_validation->set_rules('id_solicitud','El id solicitud','required');
	     $this->form_validation->set_rules('captcha_responder_solicitud','captcha','required|callback__validacion_captcha');
         $this->form_validation->set_message('required','El Campo %s es requerido');
	     $this->form_validation->set_message('_validacion_captcha','El Codigo ingresado es Invalido');
	     if ($this->form_validation->run()==FALSE) {
	       $msg = '<script type="text/javascript" language="text/javascript">setTimeout(function(){$.fancybox($("#responder_solicitud_box"));},100);</script>';
	       $this->index($msg);
	     }else{
	       $data=array(
	       	'id_users' => $this->session->userdata('id_user_red'),
	       	'id_solicitud' => $this->input->post('id_solicitud'),
	       	'respuesta_solicitud'=> $this->input->post('respoder_solicitud'),           
	       );
	       $this->model_apoyemonos->insert_respuesta_solicitud($data);
           echo "<script type='text/javascript'>
           alert('\"Gracias por responder a esta solicitud. Su aporte esta en linea\"');
           window.location='../apoyemonos';
           </script>";
	     }
	 }else{
	 	redirect('apoyemonos');
	 }
    } 
    /*-----------------------------------------------
    FUNCION PARA MOSTAR RESPUESTAS DE UNA SOLICITUD*/ 
    public function mostrar_respuestas_solicitud($id){
      $respuestas =$this->model_apoyemonos->mostrar_respuesta_solicitud($id);
      return $respuestas;
    }       
    /*-----------------------------
    FUNCION PARA VALIDAR EL CAPTCHA*/ 
    public function _validacion_captcha($captcha){
     if ($this->session->userdata('numero_captcha') == $captcha)
      {
       return true;  
      }else{
        return false;
      }  
    } 
    /*-----------------------------
    FUNCION PARA VALIDAR EL CAPTCHA*/ 
    public static function formato_fecha($fecha){
    $mes=array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre',);
    $datos=explode("-", $fecha );
    $formato= $mes[$datos[1]]." ".$datos[2]." del ".$datos[0];
    return $formato;
    }           	
}// CIERRE DE LA CLASE