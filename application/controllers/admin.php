<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

  public function __construct ()  {       
      parent :: __construct (); 
      $this->load->model('model_admin');
      $this->load->model('model_ofertas');
      $this->load->model('model_nosotros');
      $this->load->model('model_index');
      $this->load->model('model_miperfil');
      $this->load->library('pagination');
      $this->load->library('table');
      $this->_logeo_in();
      $this->_logeo_admin();       
  }

  function _logeo_in(){
      $login_in = $this->session->userdata('login_red');
      if ($login_in !=true){
        redirect ('index');
      }
    }
  /******Valida que sea el administrador***/  
  function _logeo_admin(){
    $id=$this->session->userdata('id_user_red');
    $admin=$this->model_admin->usuarios_id($id);
    $admin[0]->idtipoUsuario;
    if ($id !='1' and $admin[0]->idtipoUsuario!=3){
       redirect ('index');
    }      
  }    
    /*-----------------------------------
    FUNCION QUE INICIALIZARA POR DEFECTO*/
  public function index(){   
    $data['msg']='';
    $data['view']='admin/index';
    $this->load->view('includes/template',$data);
  }

 public function estado_users($msg1=""){
      $pages=5; //Numero de registros mostrados por páginas
      $config['base_url'] = base_url().'admin/estado_users'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
      $config['total_rows'] = $this->model_admin->totalCustomers() ;  
      $config['per_page'] = $pages; 
      $config['num_links'] = 2; //Numero de links mostrados en la paginación
 
      $this->pagination->initialize($config);
      if ($msg1==1){
        $msg1='
              <div class="alert-box success">
                 Cambio de Estado Exitoso!!
                 <a href="" class="close">&times;</a>
               </div>';
      }else {
        $msg1="";
      }
 
      $data["usuario"] = $this->model_admin->getAllPaginated($config['per_page'],$this->uri->segment(3));
      $data['msg']='';
      $data['msg1']=$msg1;
      $data['view']='admin/usuarios';
      $this->load->view('includes/template',$data);
    }

  function cambiar_estado($id){
    $data['id']=$id;
    $data['msg']='';
    $data['view']='admin/confirmacion_cambioEstado';
    $this->load->view('includes/template',$data);

  }
 function confirmacion_cambiar_estado($id){
    $tipo=$this->model_admin->usuarios_id($id);
    

    $tipo_usuario=$tipo[0]->idtipoUsuario;
    if($tipo_usuario==1){
      $nuevo_tipo=2;
    }else{
      $nuevo_tipo=1;
    }
    $tipo=$this->model_admin->cambiar_estado($id,$nuevo_tipo);
    redirect('admin/estado_users/1');
  }
  function ver_noticias(){
    $noticias=$this->model_index->noticia();
    $data= array(
        'msg'     =>'', 
        'view'    => 'admin/ver_noticias', 
        'noticia' => $noticias
    );
    $this->load->view('includes/template',$data);


  }
  function noticias(){
    $data['view']='admin/crear_noticias';
    $data['msg']='';
    $this->load->view('includes/template',$data);


  }
  public function crear_noticias(){
     
         $this->form_validation->set_rules('titulo','titulo','required');
         $this->form_validation->set_rules('noticia','noticia','required');
         $this->form_validation->set_rules('img_noticia','Imagen','callback__validacion_img');
         $this->form_validation->set_message('required','%s es requerido');
         $this->form_validation->set_message('_validacion_img','El formato de Archivo no esta permitido');
         if ($this->form_validation->run()==FALSE) {
           $this->noticias();
         }else{
         // print_r($_FILES);
             $rutaServidor="images/noticias";
             $rutaTemporal= $_FILES["img_noticia"]["tmp_name"];
             $nombreimage= $_FILES["img_noticia"]["name"];
             $nombre= date("Y-m-d").$nombreimage;
             $rutaDestino= $rutaServidor.'/'.$nombre;
             move_uploaded_file($rutaTemporal, $rutaDestino) ;
             $datos=array(
              'titulo' => $this->input->post('titulo'),
              'noticia' => $this->input->post('noticia'),
              'img' => $rutaDestino);
             $this->model_admin->insertarnoticia($datos);
              $data['view']='admin/crear_noticias';
              $data['msg']='
              <div class="alert-box success">
                 Noticia creada con Exito!!
                 <a href="" class="close">&times;</a>
               </div>';
              $this->load->view('includes/template',$data);

         }
  }

  public function editar_noticia($id){
    $noticia=$this->model_index->mostrarNoticia($id);
    $data=array(
    'msg'=>'',  
    'noticia' => $noticia,
    'view'    => "admin/editar_noticia",  
    );
    $this->load->view('includes/template',$data);

  }

  public function update_noticias(){
     
         $this->form_validation->set_rules('titulo','titulo','required');
         $this->form_validation->set_rules('noticia','noticia','required');
         $this->form_validation->set_message('required','%s es requerido');
         if ($this->form_validation->run()==FALSE) {
           $this->noticias();
         }else{
          $img_noticia=$_FILES["img_noticia"]["tmp_name"];
           if ($img_noticia!=null){
              $this->form_validation->set_rules('img_noticia','Imagen','callback__validacion_img');
              $this->form_validation->set_message('_validacion_img','El formato de Archivo no esta permitido');

                 $rutaServidor="images/noticias";
                 $rutaTemporal= $_FILES["img_noticia"]["tmp_name"];
                 $nombreimage= $_FILES["img_noticia"]["name"];
                 $nombre= date("Y-m-d").$nombreimage;
                 $rutaDestino= $rutaServidor.'/'.$nombre;
                 move_uploaded_file($rutaTemporal, $rutaDestino) ;
                 $datos=array(
                  'id' => $this->input->post('id'),
                  'titulo' => $this->input->post('titulo'),
                  'noticia' => $this->input->post('noticia'),
                  'img' => $rutaDestino);
                 $this->model_admin->updatenoticiaImg($datos);
                 $noticias=$this->model_index->noticia();
                  $data= array(
                      'msg'     =>'
                        <div class="alert-box success">
                           Noticia Editada con Exito!!
                           <a href="" class="close">&times;</a>
                         </div>', 
                      'view'    => 'admin/ver_noticias', 
                      'noticia' => $noticias
                  );
                  $this->load->view('includes/template',$data);
            }else{
                 $datos=array(
                  'id' => $this->input->post('id'),
                  'titulo' => $this->input->post('titulo'),
                  'noticia' => $this->input->post('noticia'));
                 $this->model_admin->updatenoticia($datos);
                 $noticias=$this->model_index->noticia();
                  $data= array(
                      'msg'     =>'
                        <div class="alert-box success">
                           Noticia Editada con Exito!!
                           <a href="" class="close">&times;</a>
                         </div>', 
                      'view'    => 'admin/ver_noticias', 
                      'noticia' => $noticias
                  );
                  $this->load->view('includes/template',$data);              
              

            }
       }     
  }

  public function confirmacion_noticia($id){
    $data['id']=$id;
    $data['view']='admin/confirmacion_eliminacion_noticia';
    $this->load->view('includes/template',$data);
  }

  public function eliminar_noticia($id){
  $this->model_admin->eliminarNoticia($id);
  $noticias=$this->model_index->noticia();
    $data= array(
        'msg'     =>'
    <div class="alert-box success">
       Noticia Eliminada!!
       <a href="" class="close">&times;</a>
     </div>', 
        'view'    => 'admin/ver_noticias', 
        'noticia' => $noticias
    );
    $this->load->view('includes/template',$data);
  }  

  /*  FUNCION PARA VALIDAR EL TIPO DE IMAGEN*/ 
  public function _validacion_img($FILES){
     if ($_FILES["img_noticia"]["type"]=="image/jpeg" || $_FILES["img_noticia"]["type"]=="image/pjpeg" || $_FILES["img_noticia"]["type"]=="image/gif" || $_FILES["img_noticia"]["type"]=="image/bmp" || $_FILES["img_noticia"]["type"]=="image/png")
      {
       return true;  
      }else{
        return false;
      }  
    } 
  function password(){
    $data['view']='admin/password';
    $data['msg']='';
    $this->load->view('includes/template',$data);

  } 
  function cambiar_password(){
      $this->form_validation->set_rules('contrasena', 'contrasena', 'required');
      $this->form_validation->set_rules('repite_contrasena', 'Confirma contrasena', 'required|matches[contrasena]|md5');
      $this->form_validation->set_message('required', '<div class="ci_error">El Campo es Requerido</div>'); 
      $this->form_validation->set_message('matches', '<div class="ci_error">Las Contraseñas no coinciden</div>');       

        if($this->form_validation->run() == FALSE){
          $this->password();
        }
        else{
          $contrasena=md5($this->input->post('contrasena'));
           $id= $this->session->userdata('id_user_red');
            $this->model_admin->cambio_password($id,$contrasena);

            $data['msg']='
              <div class="alert-box success">
                 Se ha actualizado tu contraseña
                 <a href="" class="close">&times;</a>
               </div>';
            $data['view'] = 'admin/password';
            $this->load->view('includes/template',$data);
          }
  }
   public function autorizar_ofertas(){
     $ofertas=$this->model_admin->ofertas_sinAutorizar();
      $data=array(
        'img_captcha' => $this->img_captcha(),
        'msg'=>'',
        'ofertas' => $ofertas,
        'view'    => "admin/autotizar_ofertas",
      ); 
      $this->load->view('includes/template',$data);
  }

  

  public function publicar($id){
     $publciar=$this->model_admin->publicar($id);
     $ofertas=$this->model_admin->ofertas_sinAutorizar();
     $data=array(
        'img_captcha' => $this->img_captcha(),
        'msg'=>'
              <div class="alert-box success">
                 Publicacion exitosa
                 <a href="" class="close">&times;</a>
               </div>',
        'ofertas' => $ofertas,
        'view'    => 'admin/autotizar_ofertas',
      );  
      $this->load->view('includes/template',$data);
  }
    public function cancelar_oferta(){
     $ofertas=$this->model_ofertas->ofertas();
      $data=array(
        'img_captcha' => $this->img_captcha(),
        'msg'=>'',
        'ofertas' => $ofertas,
        'view'    => "admin/cancelar_ofertas",
      ); 
      $this->load->view('includes/template',$data);
  }
   public function cancelar($id){
     $publciar=$this->model_admin->cancelar_oferta($id);
     $ofertas=$this->model_ofertas->ofertas();
      $data=array(
        'img_captcha' => $this->img_captcha(),
        'msg'=>'
              <div class="alert-box success">
                 La oferta ya no se encuentra Publicada
                 <a href="" class="close">&times;</a>
               </div>',
        'ofertas' => $ofertas,
        'view'    => "admin/cancelar_ofertas",
      ); 
      $this->load->view('includes/template',$data);
  }
      public function img_captcha(){
      $numero= $this->Captchar_String(7,TRUE,TRUE,TRUE); 
      $imagen_p =   imagecreate(100, 50);
      $fondo = imagecolorallocate($imagen_p, 250, 14, 100);
      $color_texto = imagecolorallocate($imagen_p, 255, 255, 255);
      imagestring($imagen_p, 20, 15, 15, $numero, $color_texto);
                ob_start (); 
            imagepng($imagen_p);
            $image_data = ob_get_contents (); 
            ob_end_clean (); 
            $image_data_base64 = base64_encode ($image_data);

             $captcha = array(
             'numero_captcha' => $numero
            );
            $this->session->set_userdata($captcha);
            return $image_data_base64;
    } 
    public function Captchar_String($length,$uc=TRUE,$n=TRUE,$sc=FALSE){
    $source = 'abcdefghijklmnopqrstuvwxyz';
    if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      if($n==1) $source .= '1234567890';
        // if($sc==1) $source .= '|@#~$%()=^*+[]{}-_';
        if($length>0){
        $rstr = "";
        $source = str_split($source,1);
        for($i=1; $i<=$length; $i++){
          mt_srand((double)microtime() * 1000000);
          $num = mt_rand(1,count($source));
          $rstr .= $source[$num-1];
        }
      }
      return $rstr;
    }

    public function nosotros(){

      $data['view']='admin/nosotros';
      $data['msg']='';
      $this->load->view('includes/template',$data);

    }

    public function crear_nosotros(){
     
         $this->form_validation->set_rules('titulo','titulo','required');
         $this->form_validation->set_rules('contenido','contenido','required');
         $this->form_validation->set_rules('img_noticia','Imagen','callback__validacion_img');
         $this->form_validation->set_message('required','%s es requerido');
         $this->form_validation->set_message('_validacion_img','El formato de Archivo no esta permitido');
         if ($this->form_validation->run()==FALSE) {
           $this->noticias();
         }else{
         // print_r($_FILES);
             $rutaServidor="images/nosotros";
             $rutaTemporal= $_FILES["img_noticia"]["tmp_name"];
             $nombreimage= $_FILES["img_noticia"]["name"];
             $nombre= date("Y-m-d").$nombreimage;
             $rutaDestino= $rutaServidor.'/'.$nombre;
             move_uploaded_file($rutaTemporal, $rutaDestino) ;
             $datos=array(
              'titulo' => $this->input->post('titulo'),
              'contenido' => $this->input->post('contenido'),
              'img' => $rutaDestino);
             $this->model_admin->insertarNosotros($datos);
              $data['view']='admin/crear_noticias';
              $data['msg']='
              <div class="alert-box success">
                 Nosotros creado con Exito!!
                 <a href="" class="close">&times;</a>
               </div>';
              $this->load->view('includes/template',$data);

         }
  }

    function editar_oferta($id){
    $ofertas=$this->model_ofertas->mostrarOferta($id); 
    $data['ofertas']=$ofertas;
    $data['msg']='';
    $data['view']='admin/editar_oferta';
    $this->load->view('includes/template',$data);

  }

  public function guardar_oferta(){
    if ($_POST) {
         $this->form_validation->set_rules('titulo','titulo','required');
         $this->form_validation->set_rules('perfil_oferta','Perfil oferta','required');
         $this->form_validation->set_rules('salario','Salario','required');
         $this->form_validation->set_rules('clase_contrato','Tipo oferta','required');
         $this->form_validation->set_rules('pais','País','required');
         $this->form_validation->set_rules('cuidad','Ciudad','required');
         $this->form_validation->set_message('required','%s no Puede estar nulo');
         if ($this->form_validation->run()==FALSE) {
           $this->editar_oferta($this->input->post('id'));
         }else{
             //print_r($_FILES);
          $img_oferta=$_FILES["img_noticia"]["tmp_name"];
           if ($img_oferta!=null){
              $this->form_validation->set_rules('img_noticia','Imagen','callback__validacion_img');
              $this->form_validation->set_message('_validacion_img','El formato de Archivo no esta permitido');

               $rutaServidor="images/ofertas_img";
               $rutaTemporal= $_FILES["img_noticia"]["tmp_name"];
               $nombre= date("Y-m-d H:i:s").$usuario.".jpeg";
               $rutaDestino= $rutaServidor.'/'.$nombre;
               move_uploaded_file($rutaTemporal, $rutaDestino) ;
               echo $rutaDestino;
               $datos=array(
                'id'=>$this->input->post('id'),
                'titulo' => $this->input->post('titulo'),
                'logo' => $rutaDestino,
                'perfil' => $this->input->post('perfil_oferta'),
                'salario' => $this->input->post('salario'),
                'contrato' => $this->input->post('clase_contrato'),
                'pais' => $this->input->post('pais'),
                'ciudad' => $this->input->post('cuidad'),
                'fecha' => date("Y-m-d H:i:s"),
               );
               $this->model_admin->updateOfertasimg($datos);
               redirect('admin/autorizar_ofertas');
             }else{
              $datos=array(
                'id'=>$this->input->post('id'),
                'titulo' => $this->input->post('titulo'),
                'perfil' => $this->input->post('perfil_oferta'),
                'salario' => $this->input->post('salario'),
                'contrato' => $this->input->post('clase_contrato'),
                'pais' => $this->input->post('pais'),
                'ciudad' => $this->input->post('cuidad'),
                'fecha' => date("Y-m-d H:i:s"),
               );
               $this->model_admin->updateOfertas($datos);
               redirect('admin/autorizar_ofertas');
             }
         }
      }else{
        redirect('ofertas');
      }
  }

  public function confirmar_oferta($id){
    $data['id']=$id;
    $data['view']='admin/confirmar_eliminacion';
    $this->load->view('includes/template',$data);
  }

  public function eliminar_oferta($id){
  $this->model_admin->eliminarOferta($id);
  $ofertas=$this->model_admin->ofertas_sinAutorizar();
      $data=array(
        'img_captcha' => $this->img_captcha(),
        'msg'=>'
    <div class="alert-box success">
       Oferta Eliminada!!
       <a href="" class="close">&times;</a>
     </div>',
        'ofertas' => $ofertas,
        'view'    => "admin/autotizar_ofertas",
      ); 
      $this->load->view('includes/template',$data);

  }

  public function ver_nosotros(){
    $nosotros=$this->model_nosotros->nosotros();
      $data= array(
        'view'    => 'admin/vista_nosotros', 
        'msg'     =>'',
        'nosotros' => $nosotros
      );
      $this->load->view('includes/template',$data);
  }

  public function confirmar_nosotros($id){
    $data['id']=$id;
    $data['msg']='';
    $data['view']='admin/confirmar_eliminacion_nosotros';
    $this->load->view('includes/template',$data);
  }
  public function eliminar_nosotros($id){
    $this->model_admin->eliminarNosotros($id);
    $nosotros=$this->model_nosotros->nosotros();
      $data= array(
        'view'    => 'admin/vista_nosotros', 
        'msg'     =>'
    <div class="alert-box success">
       Contenido Eliminado!!
       <a href="" class="close">&times;</a>
     </div>',
        'nosotros' => $nosotros
      );
      $this->load->view('includes/template',$data);
  }
  public function editar_nosotros($id){
    $nosotros=$this->model_admin->nosotros($id);
      $data= array(
        'view'    => 'admin/editar_nosotros', 
        'msg'     =>'',
        'nosotros' => $nosotros);
      $this->load->view('includes/template',$data);
  }

  public function update_nosotros(){

      $this->form_validation->set_rules('titulo','titulo','required');
      $this->form_validation->set_rules('contenido','contenido','required');
      $this->form_validation->set_message('required','%s es requerido');
          if ($this->form_validation->run()==FALSE) {
             $this->editar_oferta($this->input->post('id'));
           }else{
            $img_oferta=$_FILES["img_noticia"]["tmp_name"];
            if ($img_oferta!=null){
                  $this->form_validation->set_rules('img_noticia','Imagen','callback__validacion_img');
                  $this->form_validation->set_message('_validacion_img','El formato de Archivo no esta permitido');

                 $rutaServidor="images/nosotros";
                 $rutaTemporal= $_FILES["img_noticia"]["tmp_name"];
                 $nombreimage= $_FILES["img_noticia"]["name"];
                 $nombre= date("Y-m-d").$nombreimage;
                 $rutaDestino= $rutaServidor.'/'.$nombre;
                 move_uploaded_file($rutaTemporal, $rutaDestino) ;
                 $datos=array(
                  'id' => $this->input->post('id'),
                  'titulo' => $this->input->post('titulo'),
                  'contenido' => $this->input->post('contenido'),
                  'img' => $rutaDestino);
                 $this->model_admin->updateNosotrosimg($datos);
                  $nosotros=$this->model_nosotros->nosotros();
                  $data= array(
                    'view'    => 'admin/vista_nosotros', 
                    'msg'     =>'
                  <div class="alert-box success">
                     Contenido Editado!!
                     <a href="" class="close">&times;</a>
                   </div>',
                    'nosotros' => $nosotros);
                 $this->load->view('includes/template',$data);
             }else{

                 $datos=array(
                  'id' => $this->input->post('id'),
                  'titulo' => $this->input->post('titulo'),
                  'contenido' => $this->input->post('contenido'));
                 $this->model_admin->updateNosotros($datos);
                 $nosotros=$this->model_nosotros->nosotros();
                  $data= array(
                    'view'    => 'admin/vista_nosotros', 
                    'msg'     =>'
                  <div class="alert-box success">
                     Contenido Editado!!
                     <a href="" class="close">&times;</a>
                   </div>',
                    'nosotros' => $nosotros);
                 $this->load->view('includes/template',$data);

             }     

         }

  }

   public function buscador(){

      $this->form_validation->set_rules('palabra','palabra','required');
      $this->form_validation->set_message('required','la %s es requerido');
      if ($this->form_validation->run()==FALSE) {
           $this->estado_users();
      }else{
        $palabra=$this->input->post('palabra');
        $usuarios=$this->model_admin->buscar($palabra);
        if ($usuarios==FALSE){
          $this->estado_users();
        }
        $data['usuario']=$usuarios;
        $data['msg']='';
        $data['msg1']='';
        $data['view']='admin/usuarios';
        $this->load->view('includes/template',$data);
      }
   }

   public function buscadorOfertas(){

      $this->form_validation->set_rules('palabra','palabra','required');
      $this->form_validation->set_message('required','la %s es requerido');
      if ($this->form_validation->run()==FALSE) {
           $this->autorizar_ofertas();
      }else{
        $palabra=$this->input->post('palabra');
        $ofertas=$this->model_ofertas->buscar($palabra);
        if ($ofertas==FALSE){
          $this->autorizar_ofertas();
        }
        $msg='';
        $data=array(
        'img_captcha' => $this->img_captcha(),
        'msg'     => $msg,
        'ofertas' => $ofertas,
        'view'    => "admin/autotizar_ofertas");  
        $this->load->view('includes/template',$data);
      }   
  }  

  public function filtro(){
    $oferta=$this->input->post('filtro');
    $ofertas=$this->model_admin->ofertas_filtro($oferta);
        $msg='';
        $data=array(
        'img_captcha' => $this->img_captcha(),
        'msg'     => $msg,
        'ofertas' => $ofertas,
        'view'    => "admin/autotizar_ofertas");  
        $this->load->view('includes/template',$data);
  } 

  public function editar_usuario($id){
    $usuarios=$this->model_miperfil->usuarios($id);
    $paises = $this->model_miperfil->Mostrar_paises();
    $sectores = $this->model_miperfil->Mostrar_sectores(); 
    $data=array(
      'msg1' => '',
      'msg' => '',
      'usuarios' => $usuarios,
      'view' => 'admin/editar_usuario',
      'paises' => $paises,
      'sectores' => $sectores,
    );
    $this->load->view('includes/template',$data);

  }

  public function actualizar_datos_usuarios(){
    $contrasena=$this->input->post('contrasena');
    $id=$this->input->post('id');
        if($contrasena!=null){
          $this->form_validation->set_rules('correo','correo','required|valid_email|callback__verificarcorreo');
          $this->form_validation->set_rules('telefono','teléfono','required');
          $this->form_validation->set_rules('pais','país','required');
          $this->form_validation->set_rules('ciudad','ciudad','required');
          $this->form_validation->set_rules('profesion','profesión','required');
          $this->form_validation->set_rules('contrasena', 'contrasena', 'required');
          $this->form_validation->set_rules('repite_contrasena', 'Confirma contrasena', 'required|matches[contrasena]|md5');
          $this->form_validation->set_message('required','%s es requerido');
          $this->form_validation->set_message('valid_email','Correo no valido');
          $this->form_validation->set_message('matches','Las contraseñas no coinciden');
          $this->form_validation->set_message('numeric','%s debe ser numerico');
          $this->form_validation->set_message('_verificarcorreo','El correo ya existe');
          $this->form_validation->set_message('required', '<div class="ci_error">El Campo es Requerido</div>'); 
          $this->form_validation->set_message('matches', '<div class="ci_error">Las Contraseñas no coinciden</div>');       
        }else{
            $this->form_validation->set_rules('correo','correo','required|valid_email|callback__verificarcorreo');
            $this->form_validation->set_rules('telefono','teléfono','required');
            $this->form_validation->set_rules('pais','país','required');
            $this->form_validation->set_rules('ciudad','ciudad','required');
            $this->form_validation->set_rules('profesion','profesión','required');
            $this->form_validation->set_message('required','%s es requerido');
            $this->form_validation->set_message('valid_email','Correo no valido');
            $this->form_validation->set_message('numeric','%s debe ser numerico');
            $this->form_validation->set_message('_verificarcorreo','El correo ya existe');
            $this->form_validation->set_message('required', '<div class="ci_error">El Campo es Requerido</div>'); 
        }
          if ($this->form_validation->run()==FALSE) {
            $this->editar_usuario($id);
        }else{
            if($contrasena!=null){
                  $datos=array(
                         'id' => $id,
                         'correo' => $this->input->post('correo'),
                         'telefono' => $this->input->post('telefono'),
                         'pais' => $this->input->post('pais'),
                         'ciudad' => $this->input->post('ciudad'),
                         'profesion' => $this->input->post('profesion'),
                         'empresa' => $this->input->post('empresa'),
                         'sector' => $this->input->post('sector'),
                         'cargo' => $this->input->post('cargo'),
                         'contrasena' => md5($this->input->post('contrasena')),
                );
              $this->model_miperfil->editarContrasena($datos);
            }else{
                $datos=array(
                       'id' => $id,
                       'correo' => $this->input->post('correo'),
                       'telefono' => $this->input->post('telefono'),
                       'pais' => $this->input->post('pais'),
                       'ciudad' => $this->input->post('ciudad'),
                       'profesion' => $this->input->post('profesion'),
                       'empresa' => $this->input->post('empresa'),
                       'sector' => $this->input->post('sector'),
                       'cargo' => $this->input->post('cargo'),
                       'contrasena' => md5($this->input->post('contrasena')),
                );
              $this->model_miperfil->editar($datos);
            }
            $this->model_miperfil->editar($datos);
            $usuarios=$this->model_miperfil->usuarios($id);
            $paises = $this->model_miperfil->Mostrar_paises();
            $sectores = $this->model_miperfil->Mostrar_sectores(); 
            $data['usuarios']=$usuarios;
            $data['paises']=$paises;
            $data['sectores']=$sectores;
            $data['msg']='';
            $data['msg1']='
            <div class="alert-box success">
               Actualizacion Exitosa!!
               <a href="" class="close">&times;</a>
             </div>';
            $data['view']="admin/editar_usuario";
            $this->load->view('includes/template',$data);
            }
  }

  public function encuesta(){
    $preguntas=$this->model_index->mostrarpreguntatodas();
    $respuestas=$this->model_index->mostrarrespuestas();
    $data=array(
      'msg1'     =>  '',
      'preguntas' => $preguntas,
      'respuestas' => $respuestas,
      'view'    => "admin/encuesta",
    );
    $this->load->view('includes/template',$data);

  }

  public function crear_encuesta(){
    $respuestas=$this->model_index->mostrarrespuestas();
    $data=array(
      'msg'     =>  '',
      'view'    => "admin/crear_encuesta",
    );
    $this->load->view('includes/template',$data);
  }
  public function insert_encuesta(){
      
      $this->form_validation->set_rules('pregunta','pregunta','required');
      $this->form_validation->set_rules('respuesta1','respuesta1','required');
      $this->form_validation->set_rules('respuesta2','respuesta2','required');
      $this->form_validation->set_rules('respuesta3','respuesta3','required');
      $this->form_validation->set_message('required', '<div class="ci_error">El Campo es Requerido</div>'); 
  
    if ($this->form_validation->run()==FALSE) {
            $this->crear_encuesta();
    }else{
       $datos=array(
                       'pregunta' => $this->input->post('pregunta'),
                );
        $this->model_admin->insert_pregunta($datos);
        $idpregunta=mysql_insert_id();
        $datos=array(
                       'idpregunta' => $idpregunta,
                       'respuesta' => $this->input->post('respuesta1'),
                );
        $this->model_admin->insert_respuesta($datos);
        $datos=array(
                       'idpregunta' => $idpregunta,
                       'respuesta' => $this->input->post('respuesta2'),
                );
        $this->model_admin->insert_respuesta($datos);
        $datos=array(
                       'idpregunta' => $idpregunta,
                       'respuesta' => $this->input->post('respuesta3'),
                );
        $this->model_admin->insert_respuesta($datos);
        $preguntas=$this->model_index->mostrarpregunta();
        $respuestas=$this->model_index->mostrarrespuestas();
        $data=array(
          'msg1'     =>  '
            <div class="alert-box success">
               Encuesta Creada con Exito!!
               <a href="" class="close">&times;</a>
             </div>',
          'preguntas' => $preguntas,
          'respuestas' => $respuestas,
          'view'    => "admin/encuesta",
        );
        $this->load->view('includes/template',$data);
        }        
  }

  public function confirmar_encuesta($id){
    $data['id']=$id;
    $data['msg']='';
    $data['view']='admin/confiramcion_encuesta';
    $this->load->view('includes/template',$data);
  }

  public function activar_encuesta($id){
    $this->model_admin->desactivar_encuesta();
    $this->model_admin->activar_encuesta($id);
    $this->model_admin->borrar_ip();
    $preguntas=$this->model_index->mostrarpreguntatodas();
    $respuestas=$this->model_index->mostrarrespuestas();
    $data=array(
      'msg'     =>  '',
      'msg1'     =>  '<div class="alert-box success">
               Encuesta Activa!!
               <a href="" class="close">&times;</a>
             </div>',
      'preguntas' => $preguntas,
      'respuestas' => $respuestas,
      'view'    => "admin/encuesta",
    );
    $this->load->view('includes/template',$data);

  }
  /*-------------------------------------------
  FUNCINO PARA VER LA VISTA DE LOS BANNER HOME*/
  public function vista($vist,$val=""){
    if ($vist!="") {
        if ($val==1){ $val='<div class="alert-box success">Imagen Subida Correctamente<a href="" class="close">&times;</a></div>';}
        if ($val==3){ $val='<div class="alert-box alert">Imagen Eliminada Correctamente<a href="" class="close">&times;</a></div>';}
        if ($vist=="banner") {              
              if ($val==2){ $val='<div class="alert-box success">Imagen de Home Activada<a href="" class="close">&times;</a></div>';}              
              $img_home=$this->model_admin->imagen_home();
              $data=array(
              'msg1'     => $val,  
              'msg'     => '',
              'view'    => "admin/banner",
              'imagenes'=>$img_home,
              );
              $this->load->view('includes/template',$data);
        }else{
          $img_pautas=$this->model_admin->imagen_putas();
          $data=array(
          'msg1' => $val,
          'msg' => '',
          'view'    => "admin/pautas",
          'imagenes'=>$img_pautas,
          );
          $this->load->view('includes/template',$data);      
        }
    }else{
      redirect('admin');
    }  
  }
  /*----------------------------------
  FUNCINO PARA INSERTAR IMG BANNER NUEVO*/
  public function banner(){
    $this->form_validation->set_rules('nombre','Campo','required');
    $this->form_validation->set_rules('adjuntar_banner','Campo','callback__validacionImg');
    $this->form_validation->set_message('_validacionImg','Debe ser formato de imagen y de dimensiones 940*700');
    $this->form_validation->set_message('required','El Campo es Requerido'); 
      if ($this->form_validation->run()==FALSE) {
      $this->vista("banner");
      }else{
        $url=$this->input->post('nombre');
        $rutaServidor= "images/banner_imagen";
        $rutaTemporal= $_FILES["adjuntar_banner"]["tmp_name"];
        if($_FILES["adjuntar_banner"]["type"]=="image/jpeg"){
           $nombre= date("Y-m-d")."_".date("H:i:s").".jpeg";
           $tipo_archivo=1;
        }
        if($_FILES["adjuntar_banner"]["type"]=="image/pjpeg"){
           $nombre= date("Y-m-d")."_".date("H:i:s").".pjpeg";
           $tipo_archivo=1;
        }
        if($_FILES["adjuntar_banner"]["type"]=="image/png"){
           $nombre= date("Y-m-d")."_".date("H:i:s").".png";
           $tipo_archivo=1;
        }
        if($_FILES["adjuntar_banner"]["type"]=="image/gif"){
           $nombre= date("Y-m-d")."_".date("H:i:s").".gif";
           $tipo_archivo=1;
        } 
        if($_FILES["adjuntar_banner"]["type"]=="application/x-shockwave-flash"){
           $nombre= date("Y-m-d")."_".date("H:i:s").".swf";
           $tipo_archivo=2;
        }                             
       $rutaDestino= $rutaServidor.'/'.$nombre;
       move_uploaded_file($rutaTemporal, $rutaDestino) ;
       $estado=$this->model_admin->img_home_estado();
        if ($estado!=false) {
         foreach ($estado as $mostrar) {
           $this->model_admin->update_estado_img($mostrar->id);
         }
        } 
       $data=array('ruta'=>$rutaDestino,'tipo'=>$tipo_archivo,'estado'=>1,'url'=>$url);
       $this->model_admin->inserta_imagen($data);
       redirect("admin/vista/banner/1");
      }
  }  
  /*--------------------------------------------------------
  FUNCION PARA SELECCIONAR LA IMG PREDETERMINADA EN EL HOME*/  
  public function imgHome(){
    if($_POST){
        if ($_POST['eliminar_home']!="") {
          $imagen=$this->model_admin->imagen_home_id($this->input->post('select_img'));
          unlink($imagen[0]->ruta);
          $this->model_admin->delete_img_home($this->input->post('select_img'));
            if ($_POST['eliminar_home']=="pauta_delete") {
              redirect("admin/vista/pauta/3");
            }else{
            redirect("admin/vista/banner/3");
            }
        }else{
            $estado=$this->model_admin->img_home_estado();
            foreach ($estado as $mostrar) {
              $this->model_admin->update_estado_img($mostrar->id);
            }
            $this->model_admin->img_home_estado_new($this->input->post('select_img'));
            redirect("admin/vista/banner/2");
        }
    }else{ redirect("admin/vista/banner/");}
  }
  /*-------------------------------------------------
  FUNCION PARA VALIDAR EL TIPO DE IMAGEN DEL BANNER*/ 
  public function _validacionImg($FILES){
   if ($_FILES["adjuntar_banner"]["type"]=="image/jpeg" || $_FILES["adjuntar_banner"]["type"]=="image/pjpeg" || $_FILES["adjuntar_banner"]["type"]=="image/gif" || $_FILES["adjuntar_banner"]["type"]=="image/png" || $_FILES["adjuntar_banner"]["type"]=="application/x-shockwave-flash")
    {   
        if ($_FILES["adjuntar_banner"]["type"]=="application/x-shockwave-flash") {
          return true;
        }else{
            $info=getimagesize($_FILES["adjuntar_banner"]["tmp_name"]);
            if ($info[0]==940 AND $info[1]==700)
             { return true;}else{ return false;}; 
        }
    }else{ return false; }  
   } 
  /*----------------------------------
  FUNCINO PARA INSERTAR IMG BANNER NUEVO*/
  public function pautas(){
    $this->form_validation->set_rules('nombre','Campo','required');
    $this->form_validation->set_rules('adjuntar_pautas','Campo','callback__Img_pautas');
    $this->form_validation->set_message('_Img_pautas','Debe ser formato de imagen y de dimensiones 300*320');
    $this->form_validation->set_message('required','El %s es Requerido');
      if ($this->form_validation->run()==FALSE) {
      $this->vista("pautas");
      }else{
        $ruta=$this->input->post('nombre');
        $rutaServidor= "images/banner_imagen";
        $rutaTemporal= $_FILES["adjuntar_pautas"]["tmp_name"];
        if($_FILES["adjuntar_pautas"]["type"]=="image/jpeg"){
           $nombre= date("Y-m-d")."_".date("H:i:s").".jpeg";
           $tipo_archivo=3;
        }
        if($_FILES["adjuntar_pautas"]["type"]=="image/pjpeg"){
           $nombre= date("Y-m-d")."_".date("H:i:s").".pjpeg";
           $tipo_archivo=3;
        }
        if($_FILES["adjuntar_pautas"]["type"]=="image/png"){
           $nombre= date("Y-m-d")."_".date("H:i:s").".png";
           $tipo_archivo=3;
        }
        if($_FILES["adjuntar_pautas"]["type"]=="image/gif"){
           $nombre= date("Y-m-d")."_".date("H:i:s").".gif";
           $tipo_archivo=3;
        }  
        if($_FILES["adjuntar_pautas"]["type"]=="application/x-shockwave-flash"){
           $nombre= date("Y-m-d")."_".date("H:i:s").".swf";
           $tipo_archivo=4;
        }                                    
       $rutaDestino= $rutaServidor.'/'.$nombre;
       move_uploaded_file($rutaTemporal, $rutaDestino) ;
       $data=array('ruta'=>$rutaDestino,'tipo'=>$tipo_archivo,'estado'=>0,'url'=>$ruta);
       $this->model_admin->inserta_imagen($data);
       redirect("admin/vista/pautas/1");
      }
  }  
  /*-------------------------------------------------
  FUNCION PARA VALIDAR EL TIPO DE IMAGEN DE LA PAUTAS*/ 
  public function _Img_pautas($FILES){
   if ($_FILES["adjuntar_pautas"]["type"]=="image/jpeg" || $_FILES["adjuntar_pautas"]["type"]=="image/pjpeg" || $_FILES["adjuntar_pautas"]["type"]=="image/gif" || $_FILES["adjuntar_pautas"]["type"]=="image/png" || $_FILES["adjuntar_pautas"]["type"]=="application/x-shockwave-flash")
    { 
        $info=getimagesize($_FILES["adjuntar_pautas"]["tmp_name"]);
        if ($_FILES["adjuntar_pautas"]["type"]=="application/x-shockwave-flash") {
          return true;
        }else{        
          if ($info[0]==300 AND $info[1]==320)
           { return true;}else{ return false;}; 
       }
    }else{ return false; }  
   }  
  /*---------------------------------------------
  FUNCION PARA MOSTAR LAS SOLICITUDES REALIZADAS*/ 
  public function solicitudes($msg1=""){
    $solicitudes= $this->model_admin->mostrar_solicitudes();
    if ($msg1==1){ $msg1='<div class="alert-box success">solicitud Actualizada<a href="" class="close">&times;</a></div>';}
    if ($msg1==2){ $msg1='<div class="alert-box alert">solicitud Eliminada<a href="" class="close">&times;</a></div>';}
    $data=array(
      'msg1' =>$msg1,
      'solicitudes'    => $solicitudes,
      'view'    => "admin/solicitudes",
    );
    $this->load->view('includes/template',$data); 
  }
  /*------------------------------------------------
  FUNCION PARA ACTUALIZAR EL ESTADO DE UNA SOLICITUD*/ 
  public function estado_solicitud($id){
    $solicitud= $this->model_admin->solicitud_id($id);
    $estado="";
    if ($solicitud[0]->estado==1){$estado=0;}
    if ($solicitud[0]->estado==0){$estado=1;}
    $data=array(
      'estado'=> $estado,
      'id'    => $solicitud[0]->id,
    );
    $this->model_admin->update_solicitud($data);
    redirect("admin/solicitudes/1"); 
  }  
  /*--------------------------------
  FUNCION PARA BUSCAR UNA SOLICITUD*/
  public function  buscar_solicitudes(){
    if (isset($_POST) AND $_POST['palabra']!="" ) {
      $buscador = $this->model_admin->buscar_solicitud($this->input->post('palabra'));
            $data=array(
            'msg1' =>"",
            'solicitudes'    => $buscador,
            'view'    => "admin/solicitudes",
            );
            $this->load->view('includes/template',$data); 
    }else{
      redirect('admin/solicitudes');
    }
  }
  /*---------------------------------
  FUNCION PARA FIILTRO DE SOLICITUDES*/
  public function  filtro_solicitud(){
    if (isset($_POST) AND $_POST['filtro_solicitud']!="" ) { 
      $filtro = $this->model_admin->filtro_solicitud($this->input->post('filtro_solicitud'));
            $data=array(
            'msg1' =>"",
            'solicitudes'    => $filtro,
            'view'    => "admin/solicitudes",
            );
            $this->load->view('includes/template',$data);       
    }else{
      redirect('admin/solicitudes');
    } 
  } 
  /*--------------------------------------------------
  FUNCION PARA  IR A LA VISTA DE ACTUALIZAR SOLICITUD*/ 
  public function editar_solicitud($id){
    if (isset($id) AND $id!="") {
      $solicitud = $this->model_admin->select_solicitud($id);
      $data=array(
      'msg1' =>"",
      'solicitudes'    => $solicitud,
      'view'    => "admin/update_solicitud",
      );
      $this->load->view('includes/template',$data);       
    }else{
      redirect('admin/solicitudes');
    }
  } 
  /*--------------------------------------
  FUNCION PARA  ACTUALIZAR UNA SOLICITUD*/ 
  public function update_solicitud(){
    if ($_POST) {
       $this->form_validation->set_rules('titulo','Titulo','required');
       $this->form_validation->set_rules('solicitud','solicitud','required');     
       $this->form_validation->set_message('required','El Campo %s es requerido');
        if ($this->form_validation->run()==FALSE) {
        $this->editar_solicitud($this->input->post('id_solicitud'));
        }else{
          $data=array(
           'id' => $this->input->post('id_solicitud'),
           'titulo' => $this->input->post('titulo'),
           'solicitud' =>$this->input->post('solicitud'),
          );
          $this->model_admin->update_solicitud_id($data);
          redirect("admin/solicitudes/1");
        }
    }else{
      redirect('admin/solicitudes');
    }
  }     
  /*-----------------------------------
  FUNCION PARA ELIMINAR  UNA SOLICITUD*/ 
  public function delete_solicitud($id){
    $this->model_admin->delete_solicitud($id);
    redirect("admin/solicitudes/2"); 
  } 
  /*-----------------------------------
  FUNCION PARA MOSTRAR LOS CLASIFICADOS*/ 
  public function clasificados($msg=""){
      if ($msg==1){ $msg='<div class="alert-box success">Clasificado Activado<a href="" class="close">&times;</a></div>';}    
      if ($msg==2){ $msg='<div class="alert-box alert">Clasificado Eliminada<a href="" class="close">&times;</a></div>';}
      if ($msg==3){ $msg='<div class="alert-box success">Clasificado Actualizado<a href="" class="close">&times;</a></div>';}
      $clasificado_pagado=$this->model_admin->clasificados();
      $clasificado_gratis=$this->model_admin->clasificados1();
      $data=array(
        'msg1'     => $msg,
        'clasificado_gratis' =>$clasificado_gratis,
        'clasificado_pagado' => $clasificado_pagado,
        'view'    => "admin/clasificados",
      );  
      $this->load->view('includes/template',$data);
  }  
  /*------------------------------------------------
  FUNCION PARA ACTUALIZAR EL ESTADO DE UNA SOLICITUD*/ 
  public function estado_clasificado($id){
    $this->model_admin->activar_clasificado($id);
    redirect("admin/clasificados/1"); 
  }
    /*------------------------------------------------
  FUNCION PARA ACTUALIZAR EL ESTADO DE UNA SOLICITUD*/ 
  public function estado_clasificadoP($id){
    $this->model_admin->activar_clasificadoP($id);
    redirect("admin/clasificados/1"); 
  } 
  /*-----------------------------------
  FUNCION PARA ELIMINAR  UNA SOLICITUD*/ 
  public function delete_clasificado($id){
    $clasificado=$this->model_admin->clasificado_id($id);
    unlink($clasificado[0]['ruta_img']);
    $this->model_admin->delete_clasificado($id);
    redirect("admin/clasificados/2");
  } 
  /*-----------------------------------
  FUNCION PARA ELIMINAR  UNA SOLICITUD*/ 
  public function delete_clasificadoP($id){
    $clasificado=$this->model_admin->clasificado_idP($id);
    unlink($clasificado[0]['ruta_img']);
    $this->model_admin->delete_clasificadoP($id);
    redirect("admin/clasificados/2");
  }   
  /*--------------------------------
  FUNCION PARA BUSCAR CLASIFICADOS*/
  public function  buscar_clasificado(){
    if (isset($_POST) AND $_POST['palabra']!="" ) {
      $buscador = $this->model_admin->buscador_clasificados($this->input->post('palabra'));
      $buscador1 = $this->model_admin->buscador_clasificados1($this->input->post('palabra'));
      $data=array(
        'msg1'     => '',
        'clasificado_gratis' =>$buscador1,
        'clasificado_pagado' => $buscador,
        'view'    => "admin/clasificados",
      ); 
      $this->load->view('includes/template',$data); 
    }else{
      redirect('admin/clasificados');
    }
  }   
  /*---------------------------------
  FUNCION PARA FIILTRO DE SOLICITUDES*/
  public function  filtro_clasificados(){
    if (isset($_POST) AND $_POST['filtro_solicitud']!="" ) { 
      $filtro1 = $this->model_admin->filtro_clasificados($this->input->post('filtro_solicitud'));
      $filtro = $this->model_admin->filtro_clasificados1($this->input->post('filtro_solicitud'));
      $data=array(
        'msg1'     => '',
        'clasificado_gratis' =>$filtro,
        'clasificado_pagado' => $filtro1,
        'view'    => "admin/clasificados",
      ); 
            $this->load->view('includes/template',$data);       
    }else{
      redirect('admin/clasificados');
    } 
  }  
  /*-----------------------------------------------
  FUNCION PARA IR A LA VISTA DE EDITAR CLASIFICADO*/
  public function editar_clasificado($id,$datos=""){
     if ($datos!="") {
          $data=array(
          'msg1'     => '',
          'view'    => "admin/clasificado",
          'clasificado'     => $datos,
          ); 
          $this->load->view('includes/template',$data);       
     }else{
         $clasificado = $this->model_admin->clasificado_id($id);
         $data=array(
         'msg1'     => '',
         'view'    => "admin/clasificado",
         'clasificado'     => $clasificado,
         ); 
         $this->load->view('includes/template',$data);
    }
  }  

 /*-------------------------------------
  FUNCION PARA ACTUALIZAR EL CLASIFICADO*/
  public function update_clasificado(){
        $this->form_validation->set_rules('titulo_clasificado','Titulo','required');
        if (isset($_FILES['img_oferta']) AND $_FILES['img_oferta']['name']!="") {
           $this->form_validation->set_rules('img_oferta','Imagen','callback__validacion_img_1');
           $this->form_validation->set_message('_validacion_img_1','El formato debe ser jpeg,jpg,png,gif con dimenciones 300*300');           
        }
        if (isset($_FILES['img_oferta1']) AND $_FILES['img_oferta1']['name']!="") {
           $this->form_validation->set_rules('img_oferta1','Imagen','callback__validacion_img_2');
           $this->form_validation->set_message('_validacion_img_2','El formato debe ser jpeg,jpg,png,gif con dimenciones 300*300');           
        }
        if (isset($_FILES['img_oferta2']) AND $_FILES['img_oferta2']['name']!="") {
           $this->form_validation->set_rules('img_oferta2','Imagen','callback__validacion_img_3');
           $this->form_validation->set_message('_validacion_img_3','El formato debe ser jpeg,jpg,png,gif con dimenciones 300*300');           
        }
        if (isset($_FILES['img_oferta3']) AND $_FILES['img_oferta3']['name']!="") {
           $this->form_validation->set_rules('img_oferta3','Imagen','callback__validacion_img_4');
           $this->form_validation->set_message('_validacion_img_4','El formato debe ser jpeg,jpg,png,gif con dimenciones 300*300');           
        }
        if (isset($_FILES['img_oferta4']) AND $_FILES['img_oferta4']['name']!="") {
           $this->form_validation->set_rules('img_oferta4','Imagen','callback__validacion_img_5');
           $this->form_validation->set_message('_validacion_img_5','El formato debe ser jpeg,jpg,png,gif con dimenciones 300*300');           
        }
        $this->form_validation->set_message('required','%s es requerido');
     if ($this->form_validation->run()==FALSE) {
         $datos=array(0=>array(
          'titulo'=>$this->input->post('titulo_clasificado'),
          'id'=>$this->input->post('id_clasificado'),
          'descripcion'=>$this->input->post('perfil_clasificado'),
          'telefono'=>$this->input->post('telefono'),
          'correo'=>$this->input->post('correo'),
          'costo'=>$this->input->post('costo'),
          'ruta_img'=>$this->input->post('ruta_img'),
          'ruta_img1'=>$this->input->post('ruta_img1'),
          'ruta_img2'=>$this->input->post('ruta_img2'),
          'ruta_img3'=>$this->input->post('ruta_img3'),
          'ruta_img4'=>$this->input->post('ruta_img4'),
          'forma_pago'=>$this->input->post('forma_pago'),
          'tiempo'=>$this->input->post('tiempo'),)
         );    
         $this->editar_clasificado($this->input->post('id_clasificado'),$datos);
     }else{
           $usuario=$this->session->userdata('nombre_user_red');
           $rutaDestino=  $this->input->post('ruta_img');
           $rutaDestino1= $this->input->post('ruta_img1');
           $rutaDestino2= $this->input->post('ruta_img2');
           $rutaDestino3= $this->input->post('ruta_img3');
           $rutaDestino4= $this->input->post('ruta_img4');
           $rutaServidor= "images/clasificado_imagen";
           /* Parte para validar si vienen imagenes e insertarlas*/
           if (isset($_FILES['img_oferta']) AND $_FILES['img_oferta']['name']!="") {
               $rutaTemporal= $_FILES["img_oferta"]["tmp_name"];
               $nombre= date("Y-m-d H:i:s").$usuario.".jpeg";
               $rutaDestino= $rutaServidor.'/'.$nombre;
               move_uploaded_file($rutaTemporal, $rutaDestino);                
           }
           /* Parte para validar si vienen imagenes e insertarlas*/
           if (isset($_FILES['img_oferta1']) AND $_FILES['img_oferta1']['name']!="") {
               $rutaTemporal1= $_FILES["img_oferta1"]["tmp_name"];
               $nombre1= date("Y-m-d H:i:s").$usuario."1.jpeg";
               $rutaDestino1= $rutaServidor.'/'.$nombre1;
               move_uploaded_file($rutaTemporal1, $rutaDestino1);                
           }
           if (isset($_FILES['img_oferta2']) AND $_FILES['img_oferta2']['name']!="") {
               $rutaTemporal2= $_FILES["img_oferta2"]["tmp_name"];
               $nombre2= date("Y-m-d H:i:s").$usuario."2.jpeg";
               $rutaDestino2= $rutaServidor.'/'.$nombre2;
               move_uploaded_file($rutaTemporal2, $rutaDestino2);
           }
           if (isset($_FILES['img_oferta3']) AND $_FILES['img_oferta3']['name']!="") {
               $rutaTemporal3= $_FILES["img_oferta3"]["tmp_name"];
               $nombre3= date("Y-m-d H:i:s").$usuario."3.jpeg";
               $rutaDestino3= $rutaServidor.'/'.$nombre3;
               move_uploaded_file($rutaTemporal3, $rutaDestino3);
           }
           if (isset($_FILES['img_oferta4']) AND $_FILES['img_oferta4']['name']!="") {
               $rutaTemporal4= $_FILES["img_oferta4"]["tmp_name"];
               $nombre4= date("Y-m-d H:i:s").$usuario."4.jpeg";
               $rutaDestino4= $rutaServidor.'/'.$nombre4;
               move_uploaded_file($rutaTemporal4, $rutaDestino4);
           }    
          $datos=array( 
          'id' => $this->input->post('id_clasificado'),  
          'titulo' => $this->input->post('titulo_clasificado'),
          'perfil' => $this->input->post('perfil_clasificado'),
          'telefono' => $this->input->post('telefono'),
          'correo' => $this->input->post('correo'),
          'costo' => $this->input->post('costo'),
          'forma_pago' => $this->input->post('forma_pago'),
          'tiempo'=>$this->input->post('tiempo'),
          'ruta_img' => $rutaDestino,
          'ruta_img1' => $rutaDestino1,
          'ruta_img2' => $rutaDestino2,
          'ruta_img3' => $rutaDestino3,
          'ruta_img4' => $rutaDestino4,
          );
          $idC=$this->input->post('id_clasificado');
          $this->model_admin->update_clasificado($datos);
          redirect('admin/clasificados/3');
      } 
  }
  /*-------------------------------------
  FUNCION PARA IR A LA VISTA DE BAKCUP*/  
  public function bakcup($msg=""){
      if ($msg==1){$msg='<div class="alert-box alert">No se Encontraron Usuarios<a href="" class="close">&times;</a></div>';}
      $data=array(
      'msg1'     => $msg,
      'view'    => "admin/bakcup",
      ); 
      $this->load->view('includes/template',$data);
  }
  /*-------------------------------------
  FUNCION PARA DESCARGAR LA BASE DE DATOS*/    
  public function base_datos(){
    if ($_POST) {
      // Nombre del archivo de con el cual queremos que se guarde la base de datos 
      $filename = "red_GH_new.sql";  
      // Cabeceras para forzar al navegador a guardar el archivo 
      header("Pragma: no-cache"); 
      header("Expires: 0"); 
      header("Content-Transfer-Encoding: binary"); 
      header("Content-type: application/force-download"); 
      header("Content-Disposition: attachment; filename=$filename"); 
      // Funciones para exportar la base de datos
      $executa = "mysqldump --user=dbo447019110  --password=@red9109 -h db447019110.db.1and1.com --opt db447019110"; 
      system($executa, $resultado); 
      // Comprobar si se ha realizado bien, si no es así, mostrará un mensaje de error 
      if ($resultado) { echo "<H1>Error ejecutando comando: $executa</H1>\n"; } 
    }else{
      redirect('admin');
    }
  }
  /*---------------------------------------
  FUNCION PARA DESCARGAR USUARIOS EN EXCEL*/    
  public function usuario_descarga(){
    if ($_POST) {
        $usuario=$this->model_admin->usuario_tipo($this->input->post('tipo_usuario'));
        if($usuario!=false){
        header("Content-Type: application/vnd.ms-excel");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("content-disposition: attachment;filename=Usuarios_REDEGH_tipo_".$this->input->post('tipo_usuario').".xls");
        echo'<html>';
        echo'<head><title>Usuarios</title></head>';
        echo'<body><table>
        <tr>
        <td><strong>Nombre</strong></td>
        <td><strong>Apellido</strong></td>
        <td><strong>Cedula</strong></td>
        <td><strong>Correo</strong></td>
        <td><strong>Telefono</strong></td>
        <td><strong>Pais</strong></td>
        <td><strong>Ciudad</strong></td>
        <td><strong>Profesion</strong></td>
        <td><strong>Empresa</strong></td>
        <td><strong>Sector</strong></td>
        <td><strong>Cargo</strong></td>
        </tr>';
        foreach ($usuario as $mostrar) {
        echo "<tr><td>".$mostrar->nombre."</td>";
        echo "<td>".$mostrar->apellido."</td>";
        echo "<td>".$mostrar->cedula."</td>";
        echo "<td>".$mostrar->email."</td>";
        echo "<td>".$mostrar->telefono."</td>";
        echo "<td>".$mostrar->pais."</td>";
        echo "<td>".$mostrar->ciudad."</td>";
        echo "<td>".$mostrar->profesion."</td>";
        echo "<td>".$mostrar->empresa."</td>";
        echo "<td>".$mostrar->sector."</td>";
        echo "<td>".$mostrar->cargo."</td></tr>";
        }        
        echo'</table></body>';
        echo'</html>';
        }else{
          redirect('admin/bakcup/1');
        }
      }else{
        redirect('admin');
      }  
  }
    /*--------------------------------------------------------
    FUNCION PARA VALIDAR EL TIPO DE IMAGEN CLASIFICADO GRANDE*/ 
    public function _validacion_img2($FILES){
     if ($_FILES["img_oferta"]["type"]=="image/jpeg" || $_FILES["img_oferta"]["type"]=="image/pjpeg" || $_FILES["img_oferta"]["type"]=="image/gif" || $_FILES["img_oferta"]["type"]=="image/bmp" || $_FILES["img_oferta"]["type"]=="image/png")
      {
          $info=getimagesize($_FILES["img_oferta"]["tmp_name"]);       
          if ($info[0]==300 AND $info[1]==300)
          { return true;}else{ return false;}; 
      }else{return false;}  
    } 
    /*--------------------------------------------------------
    FUNCION PARA VALIDAR EL TIPO DE IMAGEN CLASIFICADO PEQUEÑA*/ 
    public function _validacion_img1($FILES){
     if ($_FILES["img_oferta"]["type"]=="image/jpeg" || $_FILES["img_oferta"]["type"]=="image/pjpeg" || $_FILES["img_oferta"]["type"]=="image/gif" || $_FILES["img_oferta"]["type"]=="image/bmp" || $_FILES["img_oferta"]["type"]=="image/png")
      {
          $info=getimagesize($_FILES["img_oferta"]["tmp_name"]);       
          if ($info[0]==200 AND $info[1]==170)
          { return true;}else{ return false;}; 
      }else{return false;}  
    } 
    /*---------------------------------------------------
    FUNCION PARA VER LAS HOJAS REFERIDAS DE LOS USUARIOS*/     
    public function referidas($msg=""){ 
      $hv_referidas=$this->model_ofertas->hoja_vida_referida();
        if ($msg==1){ $msg='<div class="alert-box alert">Hoja de vida Eliminada<a href="" class="close">&times;</a></div>';}
        $data=array(
        'msg1'     => $msg,
        'hojas_referidas' => $hv_referidas,
        'view'    => "admin/referidas");  
        $this->load->view('includes/template',$data);
    }
    /*------------------------------------
    FUNCION PARA BORAR LAS HOJA REFERIDA*/     
    public function deleteHV($id){
      if (isset($id) AND $id!='') {
          $mostrar=$this->model_ofertas->hojas_referidas_id($id);      
          unlink($mostrar[0]->hv_url);
          $this->model_ofertas->delete_hv_referida($id); 
          redirect('admin/referidas/1');  
      }else{
          redirect('admin/referidas');
      }
    }  
    /*------------------------------------
    FUNCION PARA BORAR LAS HOJA REFERIDA*/     
    public function pagina_img($msg=""){
        if ($msg==1){ $msg='<div class="alert-box success">Imagen Subida Correctamente<a href="" class="close">&times;</a></div>';}
        if ($msg==2){ $msg='<div class="alert-box alert">Imagen Eliminada<a href="" class="close">&times;</a></div>';}
        $img_pagina=$this->model_admin->imagen_pagina();
        $data=array(
        'img_pagina'=>$img_pagina,
        'msg1'     => $msg,
        'view'    => "admin/pagina_img");  
        $this->load->view('includes/template',$data);
    }
 /*------------------------------------------
  FUNCION PARA SUBIR IMAGENES PARA LA PAGINA*/
  public function insert_pagina_img(){
    if (isset($_FILES) AND $_FILES["img_oferta"]["name"]!=""){
       $this->form_validation->set_rules('nombre','Campo','numeric');
       $this->form_validation->set_rules('img_oferta','Imagen','callback__validacion_img_pagina');
       $this->form_validation->set_message('_validacion_img_pagina','El formato debe ser jpeg,jpg,png con un Ancho de 940');
       if ($this->form_validation->run()==FALSE){
         $this->pagina_img($msg="");
       }else{
        $ruta="";
        $rutaServidor= "images/sliders";
        $rutaTemporal= $_FILES["img_oferta"]["tmp_name"];
        $nombre= date("Y-m-d")."_".date("H:i:s").".jpeg";
        $tipo_archivo=5;                                 
        $rutaDestino= $rutaServidor.'/'.$nombre;
        move_uploaded_file($rutaTemporal, $rutaDestino) ;
        $data=array('ruta'=>$rutaDestino,'tipo'=>$tipo_archivo,'estado'=>0,'url'=>$ruta);
        $this->model_admin->inserta_imagen($data);
        redirect("admin/pagina_img/1");
       }
    }
  }    
  /*--------------------------------------------------------
  FUNCION PARA VALIDAR EL TIPO DE IMAGEN CLASIFICADO PEQUEÑA*/ 
  public function _validacion_img_pagina($FILES){
   if ($_FILES["img_oferta"]["type"]=="image/jpeg" || $_FILES["img_oferta"]["type"]=="image/pjpeg" || $_FILES["img_oferta"]["type"]=="image/gif" || $_FILES["img_oferta"]["type"]=="image/jpg" || $_FILES["img_oferta"]["type"]=="image/png")
    {
        $info=getimagesize($_FILES["img_oferta"]["tmp_name"]);       
        if ($info[0]==940)
        { return true;}else{ return false;}; 
    }else{return false;}  
  } 
  /*-----------------------------------------------
  FUNCION PARA ELIMINAR LAS IMAGENES DE LA PAGINA*/ 
  public function elmina_imgP($id){
     if (isset($id) AND $id!="") {
          $imagen=$this->model_admin->imagen_home_id($id);
          unlink($imagen[0]->ruta);
          $this->model_admin->delete_img_home($id);
          redirect("admin/pagina_img/2");
     }else{
      redirect('admin/pagina_img');
     }
  }
  /******Confirmacion elimiancion usuarios***/////// 
  public function eliminarUsuario($id){
    $data=array(
      'msg1' => '',
      'msg' => '',
      'id' => $id,
      'view' => 'admin/confirmacion_eliminacion_usuarios',
    );
    $this->load->view('includes/template',$data);

  } 

  public function eliminar_user($id){
    
    $ofertas=$this->model_admin->ofertasUser($id);
    if($ofertas){
     $data['msg']='<div class="alert-box success">
               El usuarios no se puede Eliminar ya que a crado Ofertas
               <a href="" class="close">&times;</a>
             </div>';
    $data['view']='admin/index';
    $this->load->view('includes/template',$data);
    }else{
      $usuarios=$this->model_admin->eliminar_user($id);
      $data['msg']='<div class="alert-box success">
               Eliminacion Exito!!
               <a href="" class="close">&times;</a>
             </div>';
      $data['view']='admin/index';
      $this->load->view('includes/template',$data);
    }
  }
    
    /*--------------------------------------------------------
    FUNCION PARA VALIDAR EL TIPO DE IMAGEN CLASIFICADO GRANDE 2*/ 
    public function _validacion_img_2($FILES){
     if ($_FILES["img_oferta1"]["type"]=="image/jpeg" || $_FILES["img_oferta1"]["type"]=="image/pjpeg" || $_FILES["img_oferta1"]["type"]=="image/gif" || $_FILES["img_oferta1"]["type"]=="image/bmp" || $_FILES["img_oferta1"]["type"]=="image/png")
      {
          $info=getimagesize($_FILES["img_oferta1"]["tmp_name"]);       
          if ($info[0]==300 AND $info[1]==300)
          { return true;}else{ return false;}; 
      }else{return false;}  
    }

      public function _validacion_logotipo($FILES){
     if ($_FILES["logotipo"]["type"]=="image/jpeg" || $_FILES["logotipo"]["type"]=="image/pjpeg" || $_FILES["logotipo"]["type"]=="image/gif" || $_FILES["logotipo"]["type"]=="image/bmp" || $_FILES["logotipo"]["type"]=="image/png")
      {
          /*$info=getimagesize($_FILES["logotipo"]["tmp_name"]);       
          if ($info[0]==300 AND $info[1]==300)
          {*/ return true;/*}else{ return false;}; */
      }else{return false;}  
    }

    public function _validacion_banner($FILES){
     if ($_FILES["banner"]["type"]=="image/jpeg" || $_FILES["banner"]["type"]=="image/pjpeg" || $_FILES["banner"]["type"]=="image/gif" || $_FILES["banner"]["type"]=="image/bmp" || $_FILES["banner"]["type"]=="image/png")
      {
          /*$info=getimagesize($_FILES["banner"]["tmp_name"]);       
          if ($info[0]==300 AND $info[1]==300)
          {*/ return true;/*}else{ return false;}; */
      }else{return false;}  
    }
    /*--------------------------------------------------------
    FUNCION PARA VALIDAR EL TIPO DE IMAGEN CLASIFICADO GRANDE 3*/ 
    public function _validacion_img_3($FILES){
     if ($_FILES["img_oferta2"]["type"]=="image/jpeg" || $_FILES["img_oferta2"]["type"]=="image/pjpeg" || $_FILES["img_oferta2"]["type"]=="image/gif" || $_FILES["img_oferta2"]["type"]=="image/bmp" || $_FILES["img_oferta2"]["type"]=="image/png")
      {
          $info=getimagesize($_FILES["img_oferta2"]["tmp_name"]);       
          if ($info[0]==300 AND $info[1]==300)
          { return true;}else{ return false;}; 
      }else{return false;}  
    }
    /*--------------------------------------------------------
    FUNCION PARA VALIDAR EL TIPO DE IMAGEN CLASIFICADO GRANDE 4*/ 
    public function _validacion_img_4($FILES){
     if ($_FILES["img_oferta3"]["type"]=="image/jpeg" || $_FILES["img_oferta3"]["type"]=="image/pjpeg" || $_FILES["img_oferta3"]["type"]=="image/gif" || $_FILES["img_oferta3"]["type"]=="image/bmp" || $_FILES["img_oferta3"]["type"]=="image/png")
      {
          $info=getimagesize($_FILES["img_oferta3"]["tmp_name"]);       
          if ($info[0]==300 AND $info[1]==300)
          { return true;}else{ return false;}; 
      }else{return false;}  
    }
    /*--------------------------------------------------------
    FUNCION PARA VALIDAR EL TIPO DE IMAGEN CLASIFICADO GRANDE 5*/ 
    public function _validacion_img_5($FILES){
     if ($_FILES["img_oferta4"]["type"]=="image/jpeg" || $_FILES["img_oferta4"]["type"]=="image/pjpeg" || $_FILES["img_oferta4"]["type"]=="image/gif" || $_FILES["img_oferta4"]["type"]=="image/bmp" || $_FILES["img_oferta4"]["type"]=="image/png")
      {
          $info=getimagesize($_FILES["img_oferta4"]["tmp_name"]);       
          if ($info[0]==300 AND $info[1]==300)
          { return true;}else{ return false;}; 
      }else{return false;}  
    }
    /*--------------------------------------------------------
    FUNCION PARA VALIDAR EL TIPO DE IMAGEN CLASIFICADO GRANDE 5*/ 
    public function _validacionMiniBanner($FILES){
     if ($_FILES["mini_banner"]["type"]=="image/jpeg" || $_FILES["mini_banner"]["type"]=="image/pjpeg" || $_FILES["mini_banner"]["type"]=="image/gif" || $_FILES["mini_banner"]["type"]=="image/bmp" || $_FILES["mini_banner"]["type"]=="image/png")
      {
          $info=getimagesize($_FILES["mini_banner"]["tmp_name"]);       
          if ($info[0]==100 AND $info[1]==100)
          { return true;}else{ return false;}; 
      }else{return false;}  
    }    
    /*--------------------------------------------
    FUNCION PARA IR A LA VISTA DE LOS MINI-BANNER*/
    public function miniBanner($msm=''){
      if ($msm==1){ $msm='<div class="alert-box success">Imagen Subida Correctamente<a href="" class="close">&times;</a></div>';}
      if ($msm==2){ $msm='<div class="alert-box alert">Imagen Eliminada Correctamente<a href="" class="close">&times;</a></div>';}
      $order='order by id DESC';
      $miniBanner=$this->model_admin->miniBanner($order);
      $data=array(
            'imagenes'=>$miniBanner,
            'msg1'=>$msm,
            'view'=>'admin/miniBanner'
            );
      $this->load->view('includes/template',$data);
    }
    /*-----------------------------------
    FUNCION PARA INSERTAR UN MINI-BANNER*/
    public function insert_miniBanner($msm=''){
      if ($_POST) {
        $this->form_validation->set_rules('nombre_img','Nombre','required');
        $this->form_validation->set_rules('mini_banner','Imagen','callback__validacionMiniBanner');
        $this->form_validation->set_message('nombre_img','El %s es Requerido');
        $this->form_validation->set_message('_validacionMiniBanner','El Archivo debe ser jpg,png,jpeg<br>El Archivo debe tener dimensiones de 1000x100');
        if ($this->form_validation->run()==FALSE){
          $this->miniBanner($msm="");
        }else{          
          $rutaServidor="images/media";
          $rutaTemporal=$_FILES["mini_banner"]["tmp_name"];
          $nombre=$this->input->post('nombre_img').date("Y-m-d")."_".date("H:i:s").".jpeg";
          $rutaDestino= $rutaServidor.'/'.$nombre;
          move_uploaded_file($rutaTemporal,$rutaDestino);
          $data=array(
            'nombre_img'=>$this->input->post('nombre_img'),
            'url_img'=>$rutaDestino,
           );
          $this->model_admin->insert_miniBanner($data);
          redirect('admin/miniBanner/1');
        }
      }else{ redirect('admin/miniBanner');}
    }
    /*-----------------------------------
    FUNCION PARA ELIMINAR UN MINI-BANNER*/    
    public function delete_miniBanner(){
      if ($_POST){
      $order='WHERE id ='.$_POST['delete'];
      $miniBanner=$this->model_index->miniBanner($order);
      unlink($miniBanner[0]['url_img']);
      $this->model_admin->delete_miniBanner($miniBanner[0]['id']);
      redirect('admin/miniBanner/2');
      }else{ redirect('admin/miniBanner');}
    }    
    /*------------------------------------------
    FUNCION PARA IR A LA VISTA DE LAS REVISTAS*/
    public function revistas($msm=''){
      if ($msm==1){ $msm='<div class="alert-box success">La Revista fue Insertada Correctamente<a href="" class="close">&times;</a></div>';}
      if ($msm==2){ $msm='<div class="alert-box alert">La Revista fue Eliminada Correctamente<a href="" class="close">&times;</a></div>';}
      if ($msm==3){ $msm='<div class="alert-box">La Revista fue Actualizada Correctamente<a href="" class="close">&times;</a></div>';}
      $consulta='order by id DESC';
      $revistas=$this->model_index->revistas($consulta);
      $data=array(
            'revistas'=>$revistas,
            'msg1'=>$msm,
            'view'=>'admin/revistas'
            );
      $this->load->view('includes/template',$data);
    }
    /*---------------------------------
    FUNCION PARA INSERTAR UNA REVISTA*/    
    public function insert_revista()
    {
      if ($_POST) {
        $this->form_validation->set_rules('revista','Nombre','required');
        $this->form_validation->set_rules('ubicacion','ubicacion','required');
        $this->form_validation->set_message('required','El Campo es Requerido');
        if ($this->form_validation->run()==FALSE){
          $this->revistas($msm="");
        }else{
         $data=array(
           'nombre'=>$this->input->post('revista'),
           'ubicacion'=>$this->input->post('ubicacion'),
          );
         $this->model_admin->insert_revista($data);
         redirect('admin/revistas/1');
        }
      }else{redirect('admin/revistas');}
    }
    /*---------------------------------
    FUNCION PARA ELIMINAR UNA REVISTA*/
    public function revista_delete($value)
    {
      if(isset($value) AND $value!=""){
        $this->model_admin->revista_delete($value);
        redirect('admin/revistas/2');
      }else{redirect('admin/revistas');}
    }
    /*----------------------------------
    FUNCION PARA ACTUALIZAR UNA REVISTA*/
    public function updateRevista($id){
      if(isset($id) AND $id!=""){
      $consulta='WHERE id ='.$id;
      $revista=$this->model_index->revistas($consulta);
      $data=array(
            'revista'=>$revista,
            'view'=>'admin/revista'
            );
      $this->load->view('includes/template',$data);
      }else{redirect('admin/revistas');}
    }    
    public function update_revista(){
      if ($_POST) {
        $data=array(
         'id'=>$this->input->post('idRevista'),
         'nombre'=>$this->input->post('revista'),
         'ubicacion'=>$this->input->post('ubicacion'),
        );
        $this->model_admin->update_revista($data);
        redirect('admin/revistas/3');
      }else{redirect('admin/revistas');}
    }

    //Funciones directorio//
    public function directorio(){
    $directorio=$this->model_admin->directorio();
    $clasificacion=$this->model_admin->clasificacion();
      $data=array(
        'directorio' => $directorio,
        'view'    => "admin/directorio",
        'clasificacion' => $clasificacion,
      ); 
      $this->load->view('includes/template',$data);
  }

  /*Activar publicacion de directorio*/
  function activate_directorio($id){
    $directorio=$this->model_admin->directorio_id($id);
    $this->model_admin->activate_directorio($id);
    //mail($email, "Eliminacion", "Su anuncio de directorio ha sido eliminado");
    redirect("admin/directorio/");
  }

  /*-----------------------------------------------
  FUNCION PARA IR A LA VISTA DE EDITAR directorio*/
  public function editar_directorio($id,$datos=""){
    $paises = $this->model_miperfil->Mostrar_paises();
     if ($datos!="") {
          $data=array(
          'msg1'     => '',
          'view'    => "admin/directorio_edit",
          'directorio'     => $datos,
          'paises' => $paises,
          //'clasificacion' => $clasificacion,
          ); 
          $this->load->view('includes/template',$data);       
     }else{
         $directorio = $this->model_admin->directorio_id($id);
         $clasificacion = $this->model_admin->clasificacion_id($id);
         $data=array(
         'msg1'     => '',
         'view'    => "admin/directorio_edit",
         'directorio'     => $directorio,
         'clasificacion'     => $clasificacion,
         'paises' => $paises,
         ); 
         $this->load->view('includes/template',$data);
    }
  } 
  /*Desactivar publicacion de directorio*/
  function desactivate_directorio($id){
    $directorio=$this->model_admin->directorio_id($id);
    $this->model_admin->desactivate_directorio($id);
    //mail($email, "Activacion", "Su anuncio de directorio ha sido activado");
    redirect("admin/directorio/");
  }
  /*Funcion para editar anuncio de directorio*/
  function edit_directorio(){
        
        $this->form_validation->set_rules('nombre_empresa','Empresa','required');

        if (isset($_FILES['logotipo']) AND $_FILES['logotipo']['name']!="") {
           $this->form_validation->set_rules('logotipo','logotipo','callback__validacion_logotipo');
           $this->form_validation->set_message('_validacion_logotipo','El formato debe ser jpeg,jpg,png,gif');           
        }
        if (isset($_FILES['banner']) AND $_FILES['banner']['name']!="") {
           $this->form_validation->set_rules('banner','banner','callback__validacion_banner');
           $this->form_validation->set_message('_validacion_banner','El formato debe ser jpeg,jpg,png,gif');           
        }
          /*
           $this->form_validation->set_rules('logotipo','logotipo','callback__validacion_logotipo');
           $this->form_validation->set_message('logotipo','El formato debe ser jpeg,jpg,png,gif con dimenciones 300*300');           
           $this->form_validation->set_rules('banner','banner','callback__validacion_banner');
           $this->form_validation->set_message('banner','El formato debe ser jpeg,jpg,png,gif');           
           $this->form_validation->set_rules('email','Email','required|valid_email');
           $this->form_validation->set_rules('ciudad','Ciudad','requiered');
           $this->form_validation->set_rules('pais','Pais','requiered');
           $this->form_validation->set_rules('ciudad','Ciudad','requiered');
           $this->form_validation->set_rules('pagina_web','pagina_web','requiered');
           $this->form_validation->set_rules('clasificacion','Clasificacion','requiered');
           $this->form_validation->set_message('callback__validacion_logotipo','El formato debe ser jpeg,jpg,png,gif con dimenciones 200*170');           
           $this->form_validation->set_message('valid_email','Correo no valido');*/


          if ($this->form_validation->run()==FALSE) {
          $datos=array(0=>array(
          'id'=>$this->input->post('id'),
          'nombre_empresa'=>$this->input->post('nombre_empresa'),
          'descripcion'=>$this->input->post('descripcion'),
          'persona_contacto'=>$this->input->post('persona_contacto'),
          'telefono'=>$this->input->post('telefono'),
          'pais'=>$this->input->post('pais'), 
          'email'=>$this->input->post('email'),
          'ciudad'=>$this->input->post('ciudad'),
          'logotipo'=>$this->input->post('logotipo'),
          'pagina_web'=>$this->input->post('pagina_web'),
          //'clasificacion'=>$this->input->post('clasificacion'),
          'banner'=>$this->input->post('banner'),)
         );    
         $this->editar_directorio($this->input->post('id'),$datos);
     }else{
           $usuario=$this->session->userdata('nombre_user_red');
           $rutaDestino=  $this->input->post('logotipo');
           $rutaDestino2=  $this->input->post('banner');
           $rutaServidor= "images/directorio";
           $rutaServidor2= "images/directorio/banners";
           /* Parte para validar si vienen imagenes e insertarlas*/
           if (isset($_FILES['logotipo']) AND $_FILES['logotipo']['name']!="") {
               $rutaTemporal= $_FILES["logotipo"]["tmp_name"];
               $nombre= date("Y-m-d H:i:s").$usuario.".jpeg";
               $rutaDestino= $rutaServidor.'/'.$nombre;
               move_uploaded_file($rutaTemporal, $rutaDestino);                
           }

           if (isset($_FILES['banner']) AND $_FILES['banner']['name']!="") {
               $rutaTemporal2= $_FILES["banner"]["tmp_name"];
               $nombre2= date("Y-m-d H:i:s").$usuario.".jpeg";
               $rutaDestino2= $rutaServidor2.'/'.$nombre2;
               move_uploaded_file($rutaTemporal2, $rutaDestino2);                
           }

          $clasificacion = $this->input->post('clasificacion');
          $this->model_admin->borrado_clasificacion($this->input->post('id'));
          foreach ( $clasificacion as $value) {
                $this->model_admin->insert_clasificacion($value, $this->input->post('id'));
          }

          $datos=array(
          'id'=>$this->input->post('id'), 
          'nombre_empresa'=>$this->input->post('nombre_empresa'),
          'descripcion'=>$this->input->post('descripcion'),
          'persona_contacto'=>$this->input->post('persona_contacto'),
          'telefono'=>$this->input->post('telefono'),
          'pais'=>$this->input->post('pais'),
          'email'=>$this->input->post('email'),
          'ciudad'=>$this->input->post('ciudad'),
          'logotipo'=>$rutaDestino,
          'pagina_web'=>$this->input->post('pagina_web'),
          //'clasificacion'=>$this->input->post('clasificacion'),
          'banner'=>$rutaDestino2,
          );
          $id=$this->input->post('id');
          $this->model_admin->edit_directorio($datos);
          redirect('admin/directorio/3');
      }
      } 


  /*Eliminar publicacion de directorio*/
  function delete_directorio($id){
    $directorio=$this->model_admin->directorio_id($id);
    unlink($directorio[0]['logotipo']);
    $this->model_admin->delete_directorio($id);
    $this->model_admin->delete_directorio_clasificacion($id);
    //mail($email, "Desactivacion", "Su anuncio de directorio ha sido desactivado");
    redirect("admin/directorio/");
  }


}//cierre de la clase      
