;(function ($, window, undefined) {
  'use strict';

  var $doc = $(document),
      Modernizr = window.Modernizr;

      //

/* JQuery Searchable DropDown Plugin | Copyright (c) 2012 xhaggi */
  //

  $(document).ready(function() {
    $.fn.foundationAlerts           ? $doc.foundationAlerts() : null;
    $.fn.foundationButtons          ? $doc.foundationButtons() : null;
    $.fn.foundationAccordion        ? $doc.foundationAccordion() : null;
    $.fn.foundationNavigation       ? $doc.foundationNavigation() : null;
    $.fn.foundationTopBar           ? $doc.foundationTopBar() : null;
    $.fn.foundationCustomForms      ? $doc.foundationCustomForms() : null;
    $.fn.foundationMediaQueryViewer ? $doc.foundationMediaQueryViewer() : null;
    $.fn.foundationTabs             ? $doc.foundationTabs({callback : $.foundation.customForms.appendCustomMarkup}) : null;
    $.fn.foundationTooltips         ? $doc.foundationTooltips() : null;
    $.fn.foundationMagellan         ? $doc.foundationMagellan() : null;
    $.fn.foundationClearing         ? $doc.foundationClearing() : null;

    $.fn.placeholder                ? $('input, textarea').placeholder() : null;
  });

  // UNCOMMENT THE LINE YOU WANT BELOW IF YOU WANT IE8 SUPPORT AND ARE USING .block-grids
  // $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
  // $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
  // $('.block-grid.four-up>li:nth-child(4n+1)').css({clear: 'both'});
  // $('.block-grid.five-up>li:nth-child(5n+1)').css({clear: 'both'});

  // Hide address bar on mobile devices (except if #hash present, so we don't mess up deep linking).
 
///////////////////////////////////////registro
/**
 * JQuery Searchable DropDown Plugin
 *
 * @required jQuery 1.3.x or above
 * @author Sascha Woo <xhaggi@users.sourceforge.net>
 * $Id: jquery.searchabledropdown.js 53 2012-11-22 08:48:14Z xhaggi $
 *
 * Copyright (c) 2012 xhaggi
 * https://sourceforge.net/projects/jsearchdropdown/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */




//////////////////////////////////////////////////////////////
  if (Modernizr.touch && !window.location.hash) {
    $(window).load(function () {
      setTimeout(function () {
        window.scrollTo(0, 1);
      }, 0);
    });
  }
  $(document).ready(function(){
    /****************************************/
      $('.carousel').carousel({
      interval: 10000
      });
      $('#carousel_home').carousel({
      interval: 2000
      });
    /*------------------------------------------
    FORMULARIOS QUE SE MUESTRAN CON EL FANCYBOX*/
    $('.fancybox').fancybox();
    $('#msm_oferta_box').fancybox();
    /*--------------------------------------------*/    
    $("#crear_solicitud_form").validate();
    $("#contact_form").validate();
    $("#login_aplicar_form  ").validate();
    $("#oferta_empleo_form").validate();
    $('#aplicar_form').validate();
    $('#register_form').validate();    
    $(".replies_content").height(0);
    $(".link_toogle_replies").toggle(function (){
        var $tog = $(this).prev();
        $tog.animate({height: "500"}, 'slow')
        $(this).text("Cerrar Respuestas")
        .stop();
    }, function(){
        $(".replies_content").animate({height: '0'}, 'slow')
        $(this).text("Ver Respuestas")
        .stop();
    });
/*$(document).ready(function(){setTimeout(function(){$.fancybox($("#crear_oferta_box"));},100);});*/


  });
})(jQuery, this);
/*------------------------------------------
validacion de correo por medio de javascript*/
function valida_correo(correo) {
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(correo)){      
   return (true)
  } else {
   
   return (false);
  }
}
/*----------------------------
Funcion para logeo de usuario  */
$('#ingresar_login').click(function(){
  var correo= $('#correo_login').val();
  var pass  = $('#pass_login').val();
  datos= {correo_login: correo, pass_login: pass}
  $.ajax({
     type: "POST",
     url:  "registro/login",
     data: datos,
     dataType: "json",
     success: function(data){
      if (data.validacion=="error") 
        {
          $('#login_form label.error').html('').append(' <span>*Usuario o contraseña Invalida</span> <a href="javascript:void(0)" onclick="Cambio_contrasena_formulario();">Recuperar Contraseña</a>');
          $('#correo_login').val('');
          $('#pass_login').val('');
        }
      if (data.validacion=="bien") {location.reload();};  
     }
  });
}); 
/*---------------------------*/
/*------------------------------------------
Funcion para logeo cuando aplica una oferta*/
$('#login_oferta').click(function(){
  var correo= $('#correo_oferta').val();
  var pass  = $('#correo_pass').val();
  var id_oferta  = $('#id_oferta_login').val();
  datos= {correo_login: correo, pass_login: pass,id_oferta: id_oferta}
  $.ajax({
     type: "POST",
     url:  "../registro/login_oferta",
     data: datos,
     dataType: "json",
     success: function(data){
      if (data.validacion=="error") 
        {
          $('.error_oferta').html('').append(' *Usuario o contraseña Invalida');
          $('#correo_login').val('');
          $('#pass_login').val('');
        }
      if (data.validacion=="bien") {
        $.fancybox(data.formulario);
       };  
     }
  });
});
/*---------------------------*/
/*---------------------------------------------
Funcion para abrir el fancybox pasandole el id
de la oferta y asi poder guardarla en la bd */
function aplicar_oferta(id){
  $("#idd_oferta").attr('value', id);
  $.fancybox($("#aplicar_oferta_box"));
}
/*----------------------------------------------------
Funcion para abrir el fancybox de responde solicitud y 
pasarle  el id de la oferta para abiri la otra ventana */
function responde_solicitud(id){
  $("#id_solicitud").attr('value', id);
  $.fancybox($("#responder_solicitud_box"));
}
/*----------------------------------------------
Funcion para abrir el fancybox de login y pasarle
  el id de la oferta para abiri la otra ventana */
function login_oferta(id){
  $("#id_oferta_login").attr('value', id);
  $.fancybox($("#login_form_box"));
}
/*----------------------------------------------
Funcion para abrir el fancybox de recuperacion 
de la contraseña del usuario*/
function Cambio_contrasena_formulario(){
$.fancybox($("#recuperar_contrasena"));
}
/*-----------------------------------------------------
Funcion para recuperar la contraseña por medio de ajax*/
function Cambio_contrasena(){
  var correo = $('#correo_recuperar').val();
  if (correo==""){
     $('.error_recuperar').html('').append('EL campo es requerido');
  }
  if (valida_correo(correo)==false)
  {
    $('.error_recuperar').html('').append('EL Correo no es valido');
  }else{
      datos= {correo_contrasena: correo}
      $.ajax({
        type: "POST",
        url:  "../registro/recuperar_contrasena",
        data: datos,
        dataType: "json",
        success: function(data){
          if(data.confirmacion==false){
            $('.error_recuperar').html('').append('EL Correo no Existe'); 
          }else{
            $.fancybox(data.confirmacion);
          }
        }
      });
  }    
}
/*----------------------------------------------------------
Funcion para mostrar los departamentos dependiendo del pais*/
$('#country_user').change(function(){
  var pais = $('#country_user').val();
  if (pais=="Colombia") {
    $('#C2').fadeIn(500);
  }else{$('#C2').fadeOut();};
      /*datos= {pais: pais}
      $.ajax({
        type: "POST",
        url:  "../../red/registro/departamentos",
        data: datos,
        dataType: "json",
        success: function(data){
          $('select#city_user').html(data);
        }
      });*/
});
$("#delete_home").click(function(){
  var valor = $("input[name='select_img']:checked").val(); 
  if (valor!=null){
    $("#eliminar_home").attr('value',valor);
    $("#img_home_delete").submit();
  }else{
    alert('Debe seleccionar una Imagen');
  }  
});

$("#delete_pauta").click(function(){
  var valor = $("input[name='select_img']:checked").val(); 
  if (valor!=null){
    $("#eliminar_home").attr('value','pauta_delete');
    $("#img_pauta_delete").submit();
  }else{
    alert('Debe seleccionar una Imagen');
  }  
});
/*----------------------------------------
Funcion para filtro de buscar solicitudes*/
$('#filtro_solicitud').change(function(){
  $("#filtro_solicitud_from").submit();
});
/*------------------------------------
Funcion para filtro de buscar ofertas*/
$('#filtro_ofertas').change(function(){
  $("#filtro_ofertas_from").submit();
});
/*------------------------------------
Funcion para mostrar el tipo de clasificado*/
$('#tiempo').change(function(){
  if($(this).val() == 15){
   $('.valor').html('').append('$1000');
  }
  if($(this).val() == 30){
   $('.valor').html('').append('$2000');
  }
  if($(this).val() == 45){
   $('.valor').html('').append('$3000');
  }
});


///////////funcion para cargar los minibanner cada 5 segundos/////////////////

var int=self.setInterval(function(){mini_banner()},5000);
function mini_banner(){
  var minibanner_final='';
  $.ajax({
  type: "POST",
  url: "../index/miniBanner",
  dataType: "json",
      success:function(data){
          $.each(data.minibanner, function(i,minibanner){
             minibanner_final = minibanner_final+'<li><img src="'+minibanner.url_img+'" alt=""></li>';
          });
          $(".columns.four.clients_home ul").html(minibanner_final);
      }
   });    
}


function ver_mas_directorio(banner){
  $("#id_solicitud").attr('value', banner);
  $.fancybox($("#ver_mas_box"));
}